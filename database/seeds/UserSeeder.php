<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'wawan', 
            'email' => 'wawan@gmail.com',
            'jabatan' => 'admin',
            'email_verified_at' => '2019-03-04 18:01:44',
            'password' => '$2y$10$yvL.RUCDsRn2oUhPwQkZoOC59TG9i9lOvqGOOcsFcCbM4UYJFFKby',
            'remember_token' => 'WggT9KZCd3txQsCTExoonQWqWP1g5v2rLoXZ7V2T04n0oxCg40sBKr34FCWH',
        ]);
    }
}
