<?php

use Illuminate\Database\Seeder;
use App\Mail;
class MailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            Mail::create([
                'nomor' => '10101010', 
                'tujuan' => 'Coba Tujuan',
                'nim' => 'coba nim',
                'nama' => 'coba nama',
                'email' => 'coba email',
                'hp' => 'coba hp'
            ]);
    }
}
