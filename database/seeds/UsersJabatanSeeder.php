<?php

use Illuminate\Database\Seeder;
use App\Jabatan;

class UsersJabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jabatan::create([
            'jabatan' => 'Admin'
        ]);
        Jabatan::create([
            'jabatan' => 'Karyawan'
        ]);
        Jabatan::create([
            'jabatan' => 'Mahasiswa'
        ]);
    }
}
