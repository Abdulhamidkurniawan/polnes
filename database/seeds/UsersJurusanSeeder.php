<?php

use Illuminate\Database\Seeder;
use App\Jurusan;

class UsersJurusanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jurusan::create([
            'jurusan' => 'Teknik Listrik - DIII'
        ]);
        Jurusan::create([
            'jurusan' => 'Teknik Listrik - DIV'
        ]);
    }
}
