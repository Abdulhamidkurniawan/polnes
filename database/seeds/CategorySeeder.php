<?php

use Illuminate\Database\Seeder;
use App\Category;
class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Profil',
            'slug' => str_slug('profil')
        ]);
        
        Category::create([
            'name' => 'Informasi Akademik',
            'slug' => str_slug('informasi_akademik')
        ]);

        Category::create([
            'name' => 'E-Alumni',
            'slug' => str_slug('e-alumni')
        ]);

        Category::create([
            'name' => 'Kalender Akademik',
            'slug' => str_slug('kalender_akademik')
        ]);
    }
}
