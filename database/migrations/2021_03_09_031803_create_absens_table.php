<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbsensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nim')->nullable();
            $table->string('nama')->nullable();
            $table->string('nama_mk')->nullable();
            $table->string('dosen')->nullable();
            $table->string('jurusan')->nullable();
            $table->string('prodi')->nullable();
            $table->string('tahun_ajaran')->nullable();
            $table->string('semester')->nullable();
            $table->string('kelas')->nullable();
            $table->string('jumlah_jam')->nullable();
            $table->string('jam_pertemuan')->nullable();
            $table->string('tanggal')->nullable();
            $table->string('materi')->nullable();
            $table->string('catatan')->nullable();
            $table->string('persetujuan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absens');
    }
}
