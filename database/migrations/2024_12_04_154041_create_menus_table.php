<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id', 2);
            $table->string('urutan', 2)->nullable();
            $table->string('tipe', 20)->nullable();
            $table->string('sub_menu', 20)->nullable();
            $table->string('judul')->nullable();
            $table->string('link')->nullable();
            $table->string('status', 9)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
