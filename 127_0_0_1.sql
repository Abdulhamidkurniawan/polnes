-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2019 at 06:05 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `polnes`
--
CREATE DATABASE IF NOT EXISTS `polnes` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `polnes`;

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `article_tag`
--

CREATE TABLE `article_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `borangs`
--

CREATE TABLE `borangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `butir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `borangs`
--

INSERT INTO `borangs` (`id`, `jenis`, `butir`, `judul`, `link`, `created_at`, `updated_at`) VALUES
(1, '3A', '3.1.1', 'DATA MAHASISWA REGULER', 'https://docs.google.com/spreadsheets/d/1XYBFdL7HbYXcqZblcO-DwOKXAWQl4gL6G0xLFqIdQO4/edit#gid=1439355385', '2019-03-12 17:43:42', '2019-03-19 09:52:49'),
(2, '3A', '3.1.3', 'JUMLAH MAHASISWA REGULER', 'https://docs.google.com/spreadsheets/d/1XYBFdL7HbYXcqZblcO-DwOKXAWQl4gL6G0xLFqIdQO4/edit#gid=407254256', '2019-03-19 09:58:54', '2019-03-19 09:58:54');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Profil', 'profil', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(2, 'Informasi Akademik', 'informasi-akademik', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(3, 'E-Alumni', 'e-alumni', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(4, 'Kalender Akademik', 'kalender-akademik', '2019-03-12 17:43:42', '2019-03-12 17:43:42');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_file_handlers`
--

CREATE TABLE `df_file_handlers` (
  `id` mediumint(9) NOT NULL,
  `uid` mediumint(9) DEFAULT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ext` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `handler` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `df_file_handlers`
--

INSERT INTO `df_file_handlers` (`id`, `uid`, `type`, `ext`, `handler`) VALUES
(1, NULL, 'txt', NULL, 'code_editor'),
(2, NULL, 'img', NULL, 'image_viewer'),
(3, NULL, 'wvideo', NULL, 'video_player'),
(4, NULL, 'mp3', NULL, 'audio_player'),
(5, NULL, 'office', NULL, 'office_web_viewer'),
(6, NULL, 'ooffice', NULL, 'office_web_viewer'),
(7, NULL, 'arch', NULL, 'arch'),
(8, NULL, NULL, 'odt', 'webodf'),
(9, NULL, NULL, 'ods', 'webodf'),
(10, NULL, NULL, 'odp', 'webodf'),
(11, NULL, NULL, 'pdf', 'open_in_browser'),
(12, NULL, NULL, 'url', 'handle_url'),
(13, NULL, NULL, 'html', 'html_editor'),
(14, NULL, NULL, 'kml', 'kml_viewer'),
(15, NULL, NULL, 'kmz', 'kml_viewer'),
(16, NULL, NULL, 'gpx', 'bing_kml_viewer'),
(17, NULL, NULL, 'md', 'markdown_viewer'),
(18, NULL, NULL, 'epub', 'epub_reader'),
(19, NULL, NULL, 'gpx', 'bing_kml_viewer');

-- --------------------------------------------------------

--
-- Table structure for table `df_file_logs`
--

CREATE TABLE `df_file_logs` (
  `id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `uid` mediumint(9) NOT NULL,
  `action` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_logs`
--

CREATE TABLE `df_logs` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT '2002-02-02 00:00:00',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` mediumint(9) NOT NULL DEFAULT '0',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `df_logs`
--

INSERT INTO `df_logs` (`id`, `date`, `action`, `data`, `uid`, `path`) VALUES
(1, '2019-03-14 00:04:26', 'user_edited', 'a:3:{s:3:\"uid\";s:1:\"1\";s:9:\"user_info\";a:16:{s:8:\"username\";s:9:\"superuser\";s:8:\"password\";s:60:\"$2y$10$RzcnNdSvneVtBDDzAHCiAuGn7/ctjl1Hg3e98p906jTgkzMq61Ve.\";s:23:\"require_password_change\";i:0;s:16:\"last_pass_change\";s:5:\"NOW()\";s:16:\"two_step_enabled\";i:0;s:15:\"expiration_date\";s:4:\"NULL\";s:9:\"activated\";i:1;s:4:\"name\";s:9:\"Superuser\";s:5:\"name2\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"website\";s:0:\"\";s:8:\"logo_url\";s:0:\"\";s:11:\"description\";s:0:\"\";s:21:\"receive_notifications\";i:0;}s:11:\"permissions\";a:29:{s:10:\"admin_type\";s:0:\"\";s:4:\"role\";s:4:\"NULL\";s:10:\"admin_over\";s:5:\"-ALL-\";s:10:\"homefolder\";s:0:\"\";s:15:\"space_quota_max\";N;s:15:\"admin_max_users\";i:0;s:25:\"admin_homefolder_template\";s:0:\"\";s:11:\"admin_users\";i:0;s:11:\"admin_roles\";i:0;s:19:\"admin_notifications\";i:0;s:10:\"admin_logs\";i:0;s:14:\"admin_metadata\";i:0;s:8:\"readonly\";i:0;s:6:\"upload\";i:1;s:15:\"upload_max_size\";N;s:18:\"upload_limit_types\";s:0:\"\";s:8:\"download\";i:1;s:16:\"download_folders\";i:1;s:13:\"read_comments\";i:1;s:14:\"write_comments\";i:1;s:5:\"email\";i:1;s:7:\"weblink\";i:1;s:5:\"share\";i:1;s:12:\"share_guests\";i:1;s:8:\"metadata\";i:1;s:12:\"file_history\";i:1;s:11:\"change_pass\";i:1;s:12:\"edit_profile\";i:1;s:13:\"users_may_see\";s:5:\"-ALL-\";}}', 1, ''),
(2, '2019-03-14 00:05:50', 'user_edited', 'a:3:{s:3:\"uid\";s:1:\"1\";s:9:\"user_info\";a:16:{s:8:\"username\";s:9:\"superuser\";s:8:\"password\";s:60:\"$2y$10$xKkp.jl4d4gF.b5RcQs5OeZEgZzMKCosRuOluL9ANGQr2EVir83XS\";s:23:\"require_password_change\";i:0;s:16:\"last_pass_change\";s:5:\"NOW()\";s:16:\"two_step_enabled\";i:0;s:15:\"expiration_date\";s:4:\"NULL\";s:9:\"activated\";i:1;s:4:\"name\";s:9:\"Superuser\";s:5:\"name2\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"website\";s:0:\"\";s:8:\"logo_url\";s:0:\"\";s:11:\"description\";s:0:\"\";s:21:\"receive_notifications\";i:0;}s:11:\"permissions\";a:29:{s:10:\"admin_type\";s:0:\"\";s:4:\"role\";s:4:\"NULL\";s:10:\"admin_over\";s:5:\"-ALL-\";s:10:\"homefolder\";s:49:\"C:/xampp/htdocs/polnes_lara/public/ff/file_upload\";s:15:\"space_quota_max\";N;s:15:\"admin_max_users\";i:0;s:25:\"admin_homefolder_template\";s:0:\"\";s:11:\"admin_users\";i:0;s:11:\"admin_roles\";i:0;s:19:\"admin_notifications\";i:0;s:10:\"admin_logs\";i:0;s:14:\"admin_metadata\";i:0;s:8:\"readonly\";i:0;s:6:\"upload\";i:1;s:15:\"upload_max_size\";N;s:18:\"upload_limit_types\";s:0:\"\";s:8:\"download\";i:1;s:16:\"download_folders\";i:1;s:13:\"read_comments\";i:1;s:14:\"write_comments\";i:1;s:5:\"email\";i:1;s:7:\"weblink\";i:1;s:5:\"share\";i:1;s:12:\"share_guests\";i:1;s:8:\"metadata\";i:1;s:12:\"file_history\";i:1;s:11:\"change_pass\";i:1;s:12:\"edit_profile\";i:1;s:13:\"users_may_see\";s:5:\"-ALL-\";}}', 1, ''),
(3, '2019-03-14 00:13:00', 'login', 'a:1:{s:2:\"IP\";s:9:\"127.0.0.1\";}', 1, ''),
(4, '2019-03-14 00:13:10', 'new_folder', 'a:2:{s:9:\"full_path\";s:57:\"C:/xampp/htdocs/polnes_lara/public/ff/file_upload/E-Modul\";s:13:\"relative_path\";s:18:\"/ROOT/HOME/E-Modul\";}', 1, ''),
(5, '2019-03-14 00:13:23', 'new_folder', 'a:2:{s:9:\"full_path\";s:71:\"C:/xampp/htdocs/polnes_lara/public/ff/file_upload/E-Modul/Elektronika 1\";s:13:\"relative_path\";s:32:\"/ROOT/HOME/E-Modul/Elektronika 1\";}', 1, ''),
(6, '2019-03-14 00:15:03', 'user_added', 'a:3:{s:3:\"uid\";s:1:\"2\";s:9:\"user_info\";a:19:{s:8:\"username\";s:9:\"mahasiswa\";s:8:\"password\";s:60:\"$2y$10$rFqlNUsuDevVDqgJyWQTWusWx2FWTTbILvCjfDJjcC3FNEYBiLK6y\";s:16:\"last_pass_change\";s:5:\"NOW()\";s:9:\"activated\";i:1;s:23:\"require_password_change\";i:0;s:16:\"two_step_enabled\";i:0;s:15:\"two_step_secret\";s:0:\"\";s:8:\"last_otp\";s:0:\"\";s:15:\"expiration_date\";s:4:\"NULL\";s:17:\"registration_date\";s:5:\"NOW()\";s:4:\"name\";s:9:\"mahasiswa\";s:5:\"name2\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"website\";s:0:\"\";s:8:\"logo_url\";s:0:\"\";s:11:\"description\";s:0:\"\";s:21:\"receive_notifications\";i:0;}s:11:\"permissions\";a:29:{s:10:\"admin_type\";s:0:\"\";s:4:\"role\";s:4:\"NULL\";s:10:\"admin_over\";s:0:\"\";s:10:\"homefolder\";s:57:\"C:/xampp/htdocs/polnes_lara/public/ff/file_upload/E-Modul\";s:15:\"space_quota_max\";N;s:15:\"admin_max_users\";i:0;s:25:\"admin_homefolder_template\";s:0:\"\";s:11:\"admin_users\";i:0;s:11:\"admin_roles\";i:0;s:19:\"admin_notifications\";i:0;s:10:\"admin_logs\";i:0;s:14:\"admin_metadata\";i:0;s:8:\"readonly\";i:0;s:6:\"upload\";i:0;s:15:\"upload_max_size\";N;s:18:\"upload_limit_types\";s:0:\"\";s:8:\"download\";i:1;s:16:\"download_folders\";i:0;s:13:\"read_comments\";i:0;s:14:\"write_comments\";i:0;s:5:\"email\";i:0;s:7:\"weblink\";i:0;s:5:\"share\";i:0;s:12:\"share_guests\";i:0;s:8:\"metadata\";i:0;s:12:\"file_history\";i:0;s:11:\"change_pass\";i:0;s:12:\"edit_profile\";i:0;s:13:\"users_may_see\";s:6:\"a:0:{}\";}}', 1, ''),
(7, '2019-03-14 00:15:07', 'user_edited', 'a:3:{s:3:\"uid\";s:1:\"1\";s:9:\"user_info\";a:15:{s:8:\"username\";s:9:\"superuser\";s:8:\"password\";s:60:\"$2y$10$xKkp.jl4d4gF.b5RcQs5OeZEgZzMKCosRuOluL9ANGQr2EVir83XS\";s:23:\"require_password_change\";i:0;s:16:\"two_step_enabled\";i:0;s:15:\"expiration_date\";s:4:\"NULL\";s:9:\"activated\";i:1;s:4:\"name\";s:9:\"Superuser\";s:5:\"name2\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"website\";s:0:\"\";s:8:\"logo_url\";s:0:\"\";s:11:\"description\";s:0:\"\";s:21:\"receive_notifications\";i:0;}s:11:\"permissions\";a:29:{s:10:\"admin_type\";s:0:\"\";s:4:\"role\";s:4:\"NULL\";s:10:\"admin_over\";s:5:\"-ALL-\";s:10:\"homefolder\";s:49:\"C:/xampp/htdocs/polnes_lara/public/ff/file_upload\";s:15:\"space_quota_max\";N;s:15:\"admin_max_users\";i:0;s:25:\"admin_homefolder_template\";s:0:\"\";s:11:\"admin_users\";i:0;s:11:\"admin_roles\";i:0;s:19:\"admin_notifications\";i:0;s:10:\"admin_logs\";i:0;s:14:\"admin_metadata\";i:0;s:8:\"readonly\";i:0;s:6:\"upload\";i:1;s:15:\"upload_max_size\";N;s:18:\"upload_limit_types\";s:0:\"\";s:8:\"download\";i:1;s:16:\"download_folders\";i:1;s:13:\"read_comments\";i:1;s:14:\"write_comments\";i:1;s:5:\"email\";i:1;s:7:\"weblink\";i:1;s:5:\"share\";i:1;s:12:\"share_guests\";i:1;s:8:\"metadata\";i:1;s:12:\"file_history\";i:1;s:11:\"change_pass\";i:1;s:12:\"edit_profile\";i:1;s:13:\"users_may_see\";s:5:\"-ALL-\";}}', 1, ''),
(8, '2019-03-14 00:15:48', 'logout', '', 1, ''),
(9, '2019-03-14 00:16:44', 'login', 'a:1:{s:2:\"IP\";s:9:\"127.0.0.1\";}', 2, ''),
(10, '2019-03-14 00:16:53', 'logout', '', 2, ''),
(11, '2019-03-14 00:17:56', 'login', 'a:1:{s:2:\"IP\";s:9:\"127.0.0.1\";}', 2, ''),
(12, '2019-03-14 00:18:19', 'logout', '', 2, ''),
(13, '2019-03-14 00:18:36', 'login_failed', 'a:3:{s:2:\"IP\";s:9:\"127.0.0.1\";s:5:\"error\";s:17:\"Invalid password.\";s:9:\"errorCode\";s:10:\"WRONG_PASS\";}', 1, ''),
(14, '2019-03-14 00:18:42', 'login', 'a:1:{s:2:\"IP\";s:9:\"127.0.0.1\";}', 1, ''),
(15, '2019-03-14 00:19:23', 'user_edited', 'a:3:{s:3:\"uid\";s:1:\"2\";s:9:\"user_info\";a:15:{s:8:\"username\";s:9:\"mahasiswa\";s:8:\"password\";s:60:\"$2y$10$rFqlNUsuDevVDqgJyWQTWusWx2FWTTbILvCjfDJjcC3FNEYBiLK6y\";s:23:\"require_password_change\";i:0;s:16:\"two_step_enabled\";i:0;s:15:\"expiration_date\";s:4:\"NULL\";s:9:\"activated\";i:1;s:4:\"name\";s:9:\"mahasiswa\";s:5:\"name2\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"website\";s:0:\"\";s:8:\"logo_url\";s:0:\"\";s:11:\"description\";s:0:\"\";s:21:\"receive_notifications\";i:0;}s:11:\"permissions\";a:29:{s:10:\"admin_type\";s:0:\"\";s:4:\"role\";s:4:\"NULL\";s:10:\"admin_over\";s:0:\"\";s:10:\"homefolder\";s:57:\"C:/xampp/htdocs/polnes_lara/public/ff/file_upload/E-Modul\";s:15:\"space_quota_max\";N;s:15:\"admin_max_users\";i:0;s:25:\"admin_homefolder_template\";s:0:\"\";s:11:\"admin_users\";i:0;s:11:\"admin_roles\";i:0;s:19:\"admin_notifications\";i:0;s:10:\"admin_logs\";i:0;s:14:\"admin_metadata\";i:0;s:8:\"readonly\";i:1;s:6:\"upload\";i:0;s:15:\"upload_max_size\";N;s:18:\"upload_limit_types\";s:0:\"\";s:8:\"download\";i:1;s:16:\"download_folders\";i:0;s:13:\"read_comments\";i:0;s:14:\"write_comments\";i:0;s:5:\"email\";i:0;s:7:\"weblink\";i:0;s:5:\"share\";i:0;s:12:\"share_guests\";i:0;s:8:\"metadata\";i:0;s:12:\"file_history\";i:0;s:11:\"change_pass\";i:0;s:12:\"edit_profile\";i:0;s:13:\"users_may_see\";s:6:\"a:0:{}\";}}', 1, ''),
(16, '2019-03-14 00:23:31', 'logout', '', 1, ''),
(17, '2019-03-14 00:24:53', 'login_failed', 'a:3:{s:2:\"IP\";s:3:\"::1\";s:5:\"error\";s:17:\"Invalid password.\";s:9:\"errorCode\";s:10:\"WRONG_PASS\";}', 1, ''),
(18, '2019-03-14 00:24:59', 'login', 'a:1:{s:2:\"IP\";s:3:\"::1\";}', 1, ''),
(19, '2019-03-14 00:28:48', 'user_edited', 'a:3:{s:3:\"uid\";s:1:\"1\";s:9:\"user_info\";a:15:{s:8:\"username\";s:9:\"superuser\";s:8:\"password\";s:60:\"$2y$10$xKkp.jl4d4gF.b5RcQs5OeZEgZzMKCosRuOluL9ANGQr2EVir83XS\";s:23:\"require_password_change\";i:0;s:16:\"two_step_enabled\";i:0;s:15:\"expiration_date\";s:4:\"NULL\";s:9:\"activated\";i:1;s:4:\"name\";s:9:\"Superuser\";s:5:\"name2\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"website\";s:0:\"\";s:8:\"logo_url\";s:0:\"\";s:11:\"description\";s:0:\"\";s:21:\"receive_notifications\";i:0;}s:11:\"permissions\";a:29:{s:10:\"admin_type\";s:0:\"\";s:4:\"role\";s:4:\"NULL\";s:10:\"admin_over\";s:5:\"-ALL-\";s:10:\"homefolder\";s:49:\"C:/xampp/htdocs/polnes_lara/public/ff/file_upload\";s:15:\"space_quota_max\";N;s:15:\"admin_max_users\";i:0;s:25:\"admin_homefolder_template\";s:0:\"\";s:11:\"admin_users\";i:0;s:11:\"admin_roles\";i:0;s:19:\"admin_notifications\";i:0;s:10:\"admin_logs\";i:0;s:14:\"admin_metadata\";i:0;s:8:\"readonly\";i:0;s:6:\"upload\";i:1;s:15:\"upload_max_size\";N;s:18:\"upload_limit_types\";s:0:\"\";s:8:\"download\";i:1;s:16:\"download_folders\";i:1;s:13:\"read_comments\";i:1;s:14:\"write_comments\";i:1;s:5:\"email\";i:1;s:7:\"weblink\";i:1;s:5:\"share\";i:1;s:12:\"share_guests\";i:1;s:8:\"metadata\";i:1;s:12:\"file_history\";i:1;s:11:\"change_pass\";i:1;s:12:\"edit_profile\";i:1;s:13:\"users_may_see\";s:5:\"-ALL-\";}}', 1, ''),
(20, '2019-03-14 00:29:17', 'logout', '', 1, ''),
(21, '2019-03-14 00:29:59', 'login', 'a:1:{s:2:\"IP\";s:9:\"127.0.0.1\";}', 1, ''),
(22, '2019-03-14 00:30:34', 'logout', '', 1, ''),
(23, '2019-03-14 00:30:52', 'login', 'a:1:{s:2:\"IP\";s:9:\"127.0.0.1\";}', 1, ''),
(24, '2019-03-14 00:31:13', 'logout', '', 1, ''),
(25, '2019-03-14 00:31:24', 'login', 'a:1:{s:2:\"IP\";s:9:\"127.0.0.1\";}', 2, ''),
(26, '2019-03-14 00:31:33', 'logout', '', 2, ''),
(27, '2019-03-14 09:55:24', 'login', 'a:1:{s:2:\"IP\";s:9:\"127.0.0.1\";}', 2, ''),
(28, '2019-03-14 09:55:44', 'logout', '', 2, ''),
(29, '2019-03-15 10:23:57', 'login', 'a:1:{s:2:\"IP\";s:9:\"127.0.0.1\";}', 2, ''),
(30, '2019-03-15 10:40:06', 'logout', '', 2, ''),
(31, '2019-03-15 10:40:21', 'login', 'a:1:{s:2:\"IP\";s:9:\"127.0.0.1\";}', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `df_modules_folders_notifications`
--

CREATE TABLE `df_modules_folders_notifications` (
  `id` mediumint(9) NOT NULL,
  `uid` int(9) NOT NULL,
  `pathid` int(10) NOT NULL,
  `shareid` int(8) DEFAULT NULL,
  `notify_write` int(1) NOT NULL DEFAULT '0',
  `notify_read` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_modules_metadata_dt`
--

CREATE TABLE `df_modules_metadata_dt` (
  `id` mediumint(9) NOT NULL,
  `owner` mediumint(9) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ext` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_types` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `system` smallint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `df_modules_metadata_dt`
--

INSERT INTO `df_modules_metadata_dt` (`id`, `owner`, `name`, `description`, `ext`, `file_types`, `system`) VALUES
(1, 0, 'Documents', 'System type', 'pdf,txt', 'office,ooffice', 1),
(2, 0, 'Photos', 'System type', 'psb,psd,tiff,tif,bmp', 'img,raw', 1),
(3, 0, 'Audio', 'System type', '', 'mp3,audio', 1),
(4, 0, 'Video', 'System type', '', 'wvideo,video', 1);

-- --------------------------------------------------------

--
-- Table structure for table `df_modules_metadata_fields`
--

CREATE TABLE `df_modules_metadata_fields` (
  `id` mediumint(9) NOT NULL,
  `fsid` mediumint(9) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hide_fieldset_name_in_column` smallint(1) NOT NULL DEFAULT '0',
  `show_column_by_default` smallint(1) NOT NULL DEFAULT '0',
  `system` smallint(1) NOT NULL DEFAULT '0',
  `source` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `df_modules_metadata_fields`
--

INSERT INTO `df_modules_metadata_fields` (`id`, `fsid`, `name`, `description`, `type`, `options`, `hide_fieldset_name_in_column`, `show_column_by_default`, `system`, `source`, `sort`) VALUES
(1, 0, 'comment', 'Comments (System)', '', '', 0, 0, 1, NULL, NULL),
(2, 0, 'filelog', 'File Logs (System)', '', '', 0, 0, 1, NULL, NULL),
(3, 0, 'label', 'Labels (System)', '', '', 0, 0, 1, NULL, NULL),
(4, 0, 'star', 'Stars (System)', '', '', 0, 0, 1, NULL, NULL),
(5, 0, 'filename', 'Search (System)', '', '', 0, 0, 1, NULL, NULL),
(6, 0, 'zoho_collab', 'Zoho Collaborative Editing (System)', '', '', 0, 0, 1, NULL, NULL),
(7, 0, 'Tags', '', 'multiple', '', 0, 0, 1, 'MWG->Keywords', NULL),
(8, 3, 'Width', '', 'small', '', 1, 0, 0, 'MWG->width', 1),
(9, 3, 'Height', '', 'small', '', 1, 0, 0, 'MWG->height', 2),
(10, 3, 'Date taken', '', 'date', '', 1, 0, 0, 'MWG->DateCreated', 3),
(11, 3, 'Author', '', 'multiple', '', 1, 0, 0, 'MWG->Creator', 4),
(12, 3, 'Description', '', 'large', '', 1, 0, 0, 'MWG->Description', 5),
(13, 3, 'Copyright', '', '', '', 1, 0, 0, 'MWG->Copyright', 6),
(14, 3, 'GPS latitude', '', 'small', '', 1, 0, 0, 'MWG->GPSlatitude', 7),
(15, 3, 'GPS longitude', '', 'small', '', 1, 0, 0, 'MWG->GPSlongitude', 8),
(16, 4, 'Movie title', '', '', '', 1, 0, 0, 'info->quicktime->moov->subatoms->3->subatoms->0->subatoms->1->subatoms->0->data', 1),
(17, 4, 'Width', '', 'small', '', 1, 0, 0, 'info->video->resolution_x', 2),
(18, 4, 'Height', '', 'small', '', 1, 0, 0, 'info->video->resolution_y', 3),
(19, 4, 'Codec', '', '', '', 1, 0, 0, 'info->video->dataformat', 4),
(20, 5, 'Artist', '', '', '', 1, 0, 0, 'info->comments->artist', 1),
(21, 5, 'Title', '', '', '', 1, 0, 0, 'info->comments->title', 2),
(22, 5, 'Album', '', '', '', 1, 0, 0, 'info->comments->album', 3),
(23, 5, 'Duration', '', 'small', '', 1, 0, 0, 'info->playtime_string', 4),
(24, 5, 'Codec', '', '', '', 1, 0, 0, 'info->audio->codec', 5),
(25, 6, 'Author', '', '', '', 1, 0, 0, NULL, 1),
(26, 6, 'Description', '', '', '', 1, 0, 0, NULL, 2),
(27, 0, 'Rating', '', 'stars', '', 1, 0, 1, 'Rating', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `df_modules_metadata_fieldsets`
--

CREATE TABLE `df_modules_metadata_fieldsets` (
  `id` mediumint(9) NOT NULL,
  `owner` mediumint(9) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `generic` smallint(1) NOT NULL DEFAULT '0',
  `system` smallint(1) NOT NULL DEFAULT '0',
  `visible` smallint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `df_modules_metadata_fieldsets`
--

INSERT INTO `df_modules_metadata_fieldsets` (`id`, `owner`, `name`, `description`, `generic`, `system`, `visible`) VALUES
(1, 0, 'Comments (System)', 'Required for the file commenting system', 1, 1, 0),
(2, 0, 'Label (System)', 'Required for the label system', 1, 1, 0),
(3, 0, 'Image properties', '', 0, 1, 1),
(4, 0, 'Video properties', '', 0, 1, 1),
(5, 0, 'Audio properties', '', 0, 1, 1),
(6, 0, 'Various information', '', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `df_modules_metadata_files`
--

CREATE TABLE `df_modules_metadata_files` (
  `id` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `type_id` mediumint(9) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_modules_metadata_values`
--

CREATE TABLE `df_modules_metadata_values` (
  `id` int(10) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `uid` mediumint(9) NOT NULL,
  `share_id` mediumint(9) DEFAULT NULL,
  `file_id` mediumint(9) NOT NULL,
  `field_id` mediumint(9) NOT NULL,
  `val` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_modules_notifications`
--

CREATE TABLE `df_modules_notifications` (
  `id` mediumint(9) NOT NULL,
  `owner` mediumint(9) DEFAULT NULL,
  `object_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `object_id` mediumint(9) NOT NULL DEFAULT '0',
  `action` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_modules_search_index_queue`
--

CREATE TABLE `df_modules_search_index_queue` (
  `id` mediumint(9) NOT NULL,
  `uid` mediumint(9) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_modules_shares`
--

CREATE TABLE `df_modules_shares` (
  `id` mediumint(9) NOT NULL,
  `uid` mediumint(9) NOT NULL,
  `created` datetime NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `with_gid` mediumint(9) DEFAULT NULL,
  `with_uid` mediumint(9) DEFAULT NULL,
  `anonymous` smallint(1) NOT NULL DEFAULT '0',
  `perms_upload` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `perms_download` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `perms_comment` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `perms_read_comments` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `perms_alter` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `perms_share` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_modules_trash`
--

CREATE TABLE `df_modules_trash` (
  `id` mediumint(9) NOT NULL,
  `uid` mediumint(9) NOT NULL,
  `relative_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_deleted` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_modules_user_groups`
--

CREATE TABLE `df_modules_user_groups` (
  `id` mediumint(9) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` mediumint(9) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_modules_user_roles`
--

CREATE TABLE `df_modules_user_roles` (
  `id` mediumint(9) NOT NULL,
  `system` smallint(1) NOT NULL DEFAULT '0',
  `owner` mediumint(9) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_users` smallint(1) NOT NULL DEFAULT '0',
  `admin_roles` smallint(1) NOT NULL DEFAULT '0',
  `admin_notifications` smallint(1) NOT NULL DEFAULT '0',
  `admin_logs` smallint(1) NOT NULL DEFAULT '0',
  `admin_metadata` smallint(1) NOT NULL DEFAULT '0',
  `admin_over` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_max_users` smallint(7) NOT NULL DEFAULT '0',
  `admin_homefolder_template` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `homefolder` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_folder` smallint(1) NOT NULL DEFAULT '1',
  `space_quota_max` int(20) DEFAULT NULL,
  `space_quota_current` int(20) NOT NULL DEFAULT '0',
  `readonly` smallint(1) DEFAULT NULL,
  `upload` smallint(1) DEFAULT NULL,
  `upload_max_size` bigint(20) DEFAULT NULL,
  `upload_limit_types` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `download` smallint(1) DEFAULT NULL,
  `download_folders` smallint(1) DEFAULT NULL,
  `read_comments` smallint(1) DEFAULT NULL,
  `write_comments` smallint(1) DEFAULT NULL,
  `email` smallint(1) NOT NULL DEFAULT '0',
  `weblink` smallint(1) NOT NULL DEFAULT '0',
  `share` smallint(1) NOT NULL DEFAULT '0',
  `share_guests` smallint(1) NOT NULL DEFAULT '0',
  `metadata` smallint(1) NOT NULL DEFAULT '0',
  `file_history` smallint(1) NOT NULL DEFAULT '0',
  `users_may_see` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '-ALL-',
  `change_pass` smallint(1) NOT NULL DEFAULT '1',
  `edit_profile` smallint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `df_modules_user_roles`
--

INSERT INTO `df_modules_user_roles` (`id`, `system`, `owner`, `name`, `description`, `admin_type`, `admin_users`, `admin_roles`, `admin_notifications`, `admin_logs`, `admin_metadata`, `admin_over`, `admin_max_users`, `admin_homefolder_template`, `homefolder`, `create_folder`, `space_quota_max`, `space_quota_current`, `readonly`, `upload`, `upload_max_size`, `upload_limit_types`, `download`, `download_folders`, `read_comments`, `write_comments`, `email`, `weblink`, `share`, `share_guests`, `metadata`, `file_history`, `users_may_see`, `change_pass`, `edit_profile`) VALUES
(1, 1, NULL, 'Guest', 'Automatically deleted when there are no files shared with.', '', 0, 0, 0, 0, 0, '', 0, '', '', 0, NULL, 0, 1, 0, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, '-ALL-', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `df_modules_weblinks`
--

CREATE TABLE `df_modules_weblinks` (
  `id` int(10) NOT NULL,
  `id_rnd` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `uid` mediumint(9) NOT NULL,
  `pathid` int(10) NOT NULL,
  `share_id` mediumint(9) DEFAULT NULL,
  `short_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `expiry` datetime DEFAULT NULL,
  `download_limit` mediumint(6) DEFAULT NULL,
  `allow_uploads` int(1) NOT NULL DEFAULT '0',
  `allow_downloads` int(1) NOT NULL DEFAULT '1',
  `force_save` tinyint(1) DEFAULT '0',
  `system` smallint(1) NOT NULL DEFAULT '0',
  `notify` mediumint(1) NOT NULL DEFAULT '0',
  `download_terms` text COLLATE utf8mb4_unicode_ci,
  `show_comments` tinyint(1) NOT NULL DEFAULT '0',
  `show_comments_names` tinyint(1) NOT NULL DEFAULT '0',
  `require_login` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_notifications_logs`
--

CREATE TABLE `df_notifications_logs` (
  `id` mediumint(9) NOT NULL,
  `date` datetime NOT NULL,
  `has_errors` smallint(1) NOT NULL DEFAULT '0',
  `data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_oauth_access_tokens`
--

CREATE TABLE `df_oauth_access_tokens` (
  `id` mediumint(8) NOT NULL,
  `access_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sid` mediumint(8) NOT NULL,
  `expiry` int(11) NOT NULL,
  `device_uuid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_oauth_access_token_scopes`
--

CREATE TABLE `df_oauth_access_token_scopes` (
  `id` mediumint(8) NOT NULL,
  `access_token_id` mediumint(8) NOT NULL,
  `scope` mediumint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_oauth_auth_codes`
--

CREATE TABLE `df_oauth_auth_codes` (
  `id` mediumint(8) NOT NULL,
  `auth_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sid` mediumint(8) NOT NULL,
  `redirect_uri` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiry` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_oauth_auth_code_scopes`
--

CREATE TABLE `df_oauth_auth_code_scopes` (
  `id` mediumint(9) NOT NULL,
  `auth_code_id` mediumint(9) NOT NULL,
  `scope` mediumint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_oauth_clients`
--

CREATE TABLE `df_oauth_clients` (
  `id` mediumint(9) NOT NULL,
  `enabled` smallint(1) NOT NULL DEFAULT '0',
  `cid` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_url` text COLLATE utf8mb4_unicode_ci,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publisher` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` text COLLATE utf8mb4_unicode_ci,
  `publisher_website` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `df_oauth_clients`
--

INSERT INTO `df_oauth_clients` (`id`, `enabled`, `cid`, `secret`, `logo_url`, `name`, `publisher`, `description`, `website`, `publisher_website`) VALUES
(1, 1, 'FileRun0000000000000000000Mobile', '0000000000000000NoSecret0000000000000000', 'https://www.filerun.com/images/logo-mobile-app.png', 'FileRun Mobile', 'Afian AB', 'Authentication for the mobile apps', 'http://www.filerun.com', 'http://www.afian.se');

-- --------------------------------------------------------

--
-- Table structure for table `df_oauth_client_redirect_uris`
--

CREATE TABLE `df_oauth_client_redirect_uris` (
  `id` mediumint(8) NOT NULL,
  `cid` mediumint(8) NOT NULL,
  `uri` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `df_oauth_client_redirect_uris`
--

INSERT INTO `df_oauth_client_redirect_uris` (`id`, `cid`, `uri`) VALUES
(1, 1, 'filerun://');

-- --------------------------------------------------------

--
-- Table structure for table `df_oauth_refresh_tokens`
--

CREATE TABLE `df_oauth_refresh_tokens` (
  `id` mediumint(8) NOT NULL,
  `refresh_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` mediumint(9) NOT NULL,
  `expiry` int(11) NOT NULL,
  `device_uuid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_oauth_scopes`
--

CREATE TABLE `df_oauth_scopes` (
  `id` mediumint(8) NOT NULL,
  `scope` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `df_oauth_scopes`
--

INSERT INTO `df_oauth_scopes` (`id`, `scope`, `description`) VALUES
(1, 'profile', ''),
(2, 'download', ''),
(3, 'upload', ''),
(4, 'modify', ''),
(5, 'delete', ''),
(6, 'list', ''),
(7, 'email', ''),
(8, 'weblink', ''),
(9, 'upload.sandbox', ''),
(10, 'weblink.sandbox', ''),
(11, 'delete.sandbox', ''),
(12, 'admin', ''),
(13, 'list.sandbox', ''),
(14, 'download.sandbox', ''),
(15, 'share', ''),
(16, 'share.sandbox', ''),
(17, 'metadata', '');

-- --------------------------------------------------------

--
-- Table structure for table `df_oauth_sessions`
--

CREATE TABLE `df_oauth_sessions` (
  `id` mediumint(8) NOT NULL,
  `owner_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid` mediumint(8) NOT NULL,
  `redirect_uri` text COLLATE utf8mb4_unicode_ci,
  `date_created` datetime DEFAULT NULL,
  `client_ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_oauth_session_scopes`
--

CREATE TABLE `df_oauth_session_scopes` (
  `id` mediumint(9) NOT NULL,
  `sid` mediumint(9) NOT NULL,
  `scope` mediumint(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `df_paths`
--

CREATE TABLE `df_paths` (
  `id` int(10) NOT NULL,
  `path` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `depth` smallint(3) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `uniq` char(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `df_paths`
--

INSERT INTO `df_paths` (`id`, `path`, `filename`, `depth`, `date_added`, `uniq`) VALUES
(1, 'C:/xampp/htdocs/polnes_lara/public/ff/file_upload/E-Modul', 'E-Modul', 7, '2019-03-14 00:13:10', 'e4faebe8a4d3a1804255fc769eb25ab5'),
(2, 'C:/xampp/htdocs/polnes_lara/public/ff/file_upload/E-Modul/Elektronika 1', 'Elektronika 1', 8, '2019-03-14 00:13:23', 'a475981029af6ca0885558821b112af2');

-- --------------------------------------------------------

--
-- Table structure for table `df_relationships`
--

CREATE TABLE `df_relationships` (
  `id` mediumint(9) NOT NULL,
  `related_to_id` mediumint(9) NOT NULL DEFAULT '0',
  `object_id` mediumint(9) NOT NULL DEFAULT '0',
  `relation_type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `df_relationships`
--

INSERT INTO `df_relationships` (`id`, `related_to_id`, `object_id`, `relation_type`) VALUES
(1, 2, 3, 'meta-fs2dt'),
(2, 4, 4, 'meta-fs2dt'),
(3, 3, 5, 'meta-fs2dt'),
(4, 4, 5, 'meta-fs2dt'),
(5, 1, 6, 'meta-fs2dt');

-- --------------------------------------------------------

--
-- Table structure for table `df_settings`
--

CREATE TABLE `df_settings` (
  `id` mediumint(9) NOT NULL,
  `var` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `val` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `df_settings`
--

INSERT INTO `df_settings` (`id`, `var`, `val`) VALUES
(1, 'currentVersion', '2018.11.11'),
(2, 'app_title', 'JTE'),
(3, 'smtp_enable', '0'),
(4, 'smtp_host', ''),
(5, 'smtp_port', ''),
(6, 'smtp_username', ''),
(7, 'smtp_password', ''),
(8, 'smtp_auth', '0'),
(9, 'smtp_security', 'ssl'),
(10, 'last_email_notification', '31'),
(11, 'instant_email_notifications', '1'),
(12, 'default_notification_address', 'configure-me@example.com'),
(13, 'user_registration_enable', ''),
(14, 'user_registration_generate_passwords', '1'),
(15, 'user_registration_email_verification', '0'),
(16, 'user_registration_approval', '0'),
(17, 'user_registration_reqfields_email', '1'),
(18, 'user_registration_reqfields_company', '0'),
(19, 'user_registration_reqfields_website', '0'),
(20, 'user_registration_reqfields_description', '0'),
(21, 'user_registration_default_groups', 'a:0:{}'),
(22, 'user_registration_default_role', '-'),
(23, 'user_registration_change_pass', '1'),
(24, 'captcha', '0'),
(25, 'allow_change_pass', '1'),
(26, 'max_login_attempts', '5'),
(27, 'allow_persistent_login', '1'),
(28, 'logout_redirect', '/'),
(29, 'password_recovery_enable', '1'),
(30, 'password_recovery_force_change', '1'),
(31, 'search_enable', '0'),
(32, 'search_elastic_host_url', 'http://localhost:9200'),
(33, 'search_default_mode', 'filename'),
(34, 'search_tika_path', '/path/to/tika-app-1.12.jar'),
(35, 'thumbnails_enable', '1'),
(36, 'thumbnails_imagemagick', '0'),
(37, 'thumbnails_imagemagick_path', '/usr/bin/convert'),
(38, 'thumbnails_size', '140'),
(39, 'thumbnails_imagemagick_ext', 'psd,psb,eps,tst,plt,ai,pdf,jpg,jpeg,gif,png,jpe,erf,dng,cr2,crw,3fr,fff,ppm,raw,kdc,dcr,nef,mef,mos,nrw,orf,raf,mrw,mdc,rw2,pef,x3f,srw,arw,iiq,svg'),
(40, 'thumbnails_ffmpeg', '0'),
(41, 'thumbnails_ffmpeg_path', '/usr/bin/ffmpeg'),
(42, 'thumbnails_ffmpeg_ext', 'mpg,mpeg,mp4,mov,avi,divx,mkv,wmv,rm,flv,asx,asf,swf,3gp,3g2,m4v,m2ts,mts,m2v,ogv,webm'),
(43, 'versioning_max', '2'),
(44, 'quota_warning_level', '90'),
(45, 'ui_default_language', 'english'),
(46, 'ui_display_language_menu', '1'),
(47, 'ui_double_click', 'preview'),
(48, 'ui_login_logo', ''),
(49, 'ui_login_text', ''),
(50, 'ui_login_title', 'JTE'),
(51, 'ui_title_logo', '1'),
(52, 'ui_logo_url', 'C:/xampp/htdocs/polnes_lara/public/img/polnes.png'),
(53, 'ui_default_view', 'thumbnails'),
(54, 'gravatar_enabled', '1'),
(55, 'upload_blocked_types', ''),
(56, 'allow_folder_notifications', '0'),
(57, 'disable_file_history', '0'),
(58, 'pushercom_app_id', ''),
(59, 'pushercom_app_key', ''),
(60, 'pushercom_app_secret', ''),
(61, 'pushercom_enable', '0'),
(62, 'file_history_entry_lifetime', '7'),
(63, 'user_activity_logs_entry_lifetime', '7'),
(64, 'maintenance', '0'),
(65, 'maintenance_message_public', 'Our FileRun website is currently undergoing scheduled maintenance. Will be back online shortly.\nThank you for your patience!'),
(66, 'maintenance_message_users', 'This application is currently undergoing scheduled maintenance. Will be back online shortly.\nThank you for your patience!'),
(67, 'send_from_custom_email', '0'),
(68, 'passwords_min_length', '4'),
(69, 'passwords_letters_and_digits', '0'),
(70, 'passwords_requires_uppercase', '0'),
(71, 'passwords_requires_special', '0'),
(72, 'passwords_prevent_seq', '0'),
(73, 'passwords_prevent_common', '0'),
(74, 'passwords_life_time', '365'),
(75, 'logout_inactivity', '30'),
(76, 'auth_plugin', ''),
(77, 'auth_allow_local', '1'),
(78, 'notifications_template', '<div style=\"font-family:tahoma,arial,verdana,sans-serif;font-size:13px;\">\n		Hi {$info.userInfo.name},<br>\n		<br>\n\n		{foreach from=$info.actions item=action}\n			{$action.message}\n		{/foreach}\n\n		<br>\n		Best regards,<br>\n		<br>\n		<a href=\"{$config.url.root}\">{$config.url.root}</a>\n</div>'),
(79, 'notifications_subject_template', '{$settings.app_title|safeHTML} notifications ({$info.actions[0].info.userInfo.name}: {$info.actions[0].info.actionDescription})'),
(80, 'ui_media_folders_music_enable', '0'),
(81, 'ui_media_folders_photos_enable', '0'),
(82, 'guest_users', '1'),
(83, 'guest_users_delete', '1m'),
(84, 'ui_thumbs_in_detailed', '1'),
(85, 'ui_enable_rating', '0'),
(86, 'ui_photos_thumbnail_size', '200'),
(87, 'ui_theme', 'green'),
(88, 'search_result_limit', '200'),
(89, 'search_mode', 'broad'),
(90, 'ui_login_bg', ''),
(91, 'ui_logo_link_url', ''),
(92, 'ui_help_url', ''),
(93, 'ui_welcome_message', 'Selamat Datang di E-Modul Jurusan Teknik Elektro'),
(94, 'auth_sync_passwords', '0'),
(95, 'auth_plugin_ip_mask', ''),
(96, 'user_registration_reqfields_phone', '0'),
(97, 'logout_url', ''),
(98, 'logout_hide', '0');

-- --------------------------------------------------------

--
-- Table structure for table `df_users`
--

CREATE TABLE `df_users` (
  `id` mediumint(9) NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_step_enabled` int(1) NOT NULL DEFAULT '0',
  `two_step_secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_otp` varchar(35) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_pass_change` datetime DEFAULT NULL,
  `cookie` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` mediumint(9) DEFAULT NULL,
  `registration_date` datetime DEFAULT NULL,
  `activated` smallint(1) NOT NULL DEFAULT '0',
  `expiration_date` datetime DEFAULT NULL,
  `require_password_change` int(1) NOT NULL DEFAULT '0',
  `failed_login_attempts` smallint(1) NOT NULL DEFAULT '0',
  `last_access_date` timestamp NULL DEFAULT NULL,
  `last_notif_delivery_date` timestamp NULL DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `last_logout_date` datetime DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receive_notifications` smallint(1) NOT NULL DEFAULT '0',
  `new_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `df_users`
--

INSERT INTO `df_users` (`id`, `username`, `password`, `two_step_enabled`, `two_step_secret`, `last_otp`, `last_pass_change`, `cookie`, `owner`, `registration_date`, `activated`, `expiration_date`, `require_password_change`, `failed_login_attempts`, `last_access_date`, `last_notif_delivery_date`, `last_login_date`, `last_logout_date`, `email`, `receive_notifications`, `new_email`, `name`, `name2`, `phone`, `company`, `website`, `description`, `logo_url`, `avatar`) VALUES
(1, 'superuser', '$2y$10$xKkp.jl4d4gF.b5RcQs5OeZEgZzMKCosRuOluL9ANGQr2EVir83XS', 0, NULL, NULL, '2019-03-14 00:05:50', NULL, NULL, '2019-03-14 00:03:51', 1, NULL, 0, 0, '2019-03-15 02:40:21', '2019-03-13 16:13:00', '2019-03-15 10:40:21', '2019-03-14 00:31:14', '', 0, NULL, 'Superuser', '', '', '', '', '', '', NULL),
(2, 'mahasiswa', '$2y$10$rFqlNUsuDevVDqgJyWQTWusWx2FWTTbILvCjfDJjcC3FNEYBiLK6y', 0, '', '', '2019-03-14 00:15:03', NULL, NULL, '2019-03-14 00:15:03', 1, NULL, 0, 0, '2019-03-15 02:32:21', '2019-03-13 16:16:44', '2019-03-15 10:23:57', '2019-03-15 10:40:06', '', 0, NULL, 'mahasiswa', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `df_users_permissions`
--

CREATE TABLE `df_users_permissions` (
  `id` mediumint(9) NOT NULL,
  `uid` mediumint(9) NOT NULL,
  `role` mediumint(9) DEFAULT NULL,
  `admin_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_users` smallint(1) NOT NULL DEFAULT '0',
  `admin_roles` smallint(1) NOT NULL DEFAULT '0',
  `admin_notifications` smallint(1) NOT NULL DEFAULT '0',
  `admin_logs` smallint(1) NOT NULL DEFAULT '0',
  `admin_metadata` smallint(1) NOT NULL DEFAULT '0',
  `admin_over` text COLLATE utf8mb4_unicode_ci,
  `admin_max_users` smallint(7) NOT NULL DEFAULT '0',
  `admin_homefolder_template` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `homefolder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `space_quota_max` int(20) DEFAULT NULL,
  `space_quota_current` int(20) NOT NULL DEFAULT '0',
  `readonly` smallint(1) DEFAULT NULL,
  `upload` smallint(1) DEFAULT NULL,
  `upload_max_size` bigint(20) DEFAULT NULL,
  `upload_limit_types` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `download` smallint(1) DEFAULT NULL,
  `download_folders` smallint(1) DEFAULT NULL,
  `read_comments` smallint(1) DEFAULT NULL,
  `write_comments` smallint(1) DEFAULT NULL,
  `email` smallint(1) NOT NULL DEFAULT '0',
  `weblink` smallint(1) NOT NULL DEFAULT '0',
  `share` smallint(1) NOT NULL DEFAULT '0',
  `share_guests` smallint(1) NOT NULL DEFAULT '0',
  `metadata` smallint(1) NOT NULL DEFAULT '0',
  `file_history` smallint(1) NOT NULL DEFAULT '0',
  `users_may_see` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-ALL-',
  `change_pass` smallint(1) NOT NULL DEFAULT '1',
  `edit_profile` smallint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `df_users_permissions`
--

INSERT INTO `df_users_permissions` (`id`, `uid`, `role`, `admin_type`, `admin_users`, `admin_roles`, `admin_notifications`, `admin_logs`, `admin_metadata`, `admin_over`, `admin_max_users`, `admin_homefolder_template`, `homefolder`, `space_quota_max`, `space_quota_current`, `readonly`, `upload`, `upload_max_size`, `upload_limit_types`, `download`, `download_folders`, `read_comments`, `write_comments`, `email`, `weblink`, `share`, `share_guests`, `metadata`, `file_history`, `users_may_see`, `change_pass`, `edit_profile`) VALUES
(1, 1, NULL, '', 0, 0, 0, 0, 0, '-ALL-', 0, '', 'C:/xampp/htdocs/polnes_lara/public/ff/file_upload', NULL, 0, 0, 1, NULL, '', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '-ALL-', 1, 1),
(2, 2, NULL, '', 0, 0, 0, 0, 0, '', 0, '', 'C:/xampp/htdocs/polnes_lara/public/ff/file_upload/E-Modul', NULL, 0, 1, 0, NULL, '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'a:0:{}', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `df_users_sessions`
--

CREATE TABLE `df_users_sessions` (
  `id` mediumint(9) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `csrf_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mails`
--

CREATE TABLE `mails` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tujuan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mails`
--

INSERT INTO `mails` (`id`, `nomor`, `tujuan`, `nim`, `nama`, `email`, `hp`, `created_at`, `updated_at`) VALUES
(1, '10101010', 'Coba Tujuan', 'coba nim', 'coba nama', 'coba email', 'coba hp', '2019-03-12 17:43:42', '2019-03-12 17:43:42');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(100, '2014_10_12_000000_create_users_table', 1),
(101, '2014_10_12_100000_create_password_resets_table', 1),
(102, '2019_02_25_112049_create_articles_table', 1),
(103, '2019_02_25_114415_create_tags_table', 1),
(104, '2019_02_25_114937_create_article_tag_table', 1),
(105, '2019_03_01_130823_create_categories_table', 1),
(106, '2019_03_01_131911_create_posts_table', 1),
(107, '2019_03_01_132215_create_comments_table', 1),
(108, '2019_03_07_145458_create_mails_table', 1),
(109, '2019_03_19_165935_create_borangs_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `title`, `slug`, `content`, `created_at`, `updated_at`) VALUES
(1, 2, 'judul cob', 'judul-cob', 'postspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostsposts', '2019-03-12 19:05:36', '2019-03-12 19:23:59'),
(2, 1, 'cat1', 'cat1', 'cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1', '2019-03-12 19:35:26', '2019-03-12 19:35:26'),
(3, 3, 'cat3', 'cat3', 'cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3', '2019-03-12 19:35:42', '2019-03-12 19:35:42'),
(4, 4, 'cat4', 'cat4', 'cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4', '2019-03-12 19:35:52', '2019-03-12 19:35:52'),
(5, 2, 'cat2', 'wkkwkwkw', 'ddddddddddddddddddddd', '2019-03-12 20:26:45', '2019-03-12 20:52:30'),
(6, 2, 'cat2', 'wkkwkwkw', 'ddddddddddddddddddddd', '2019-03-12 20:26:45', '2019-03-12 20:52:30'),
(7, 2, 'cat2', 'wkkwkwkw', 'ddddddddddddddddddddd', '2019-03-12 20:26:45', '2019-03-12 20:52:30'),
(8, 4, 'Kalender Akademik 2018 - 2019', 'cat4', '<p><img src=\"/photos/1/kalender akademik.jpg\" alt=\"\" width=\"1050\" height=\"525\" /></p>', '2019-03-12 21:35:52', '2019-03-13 20:24:20'),
(9, 1, 'Profil Jurusan Teknik Elektro', 'profil-jurusan-teknik-elektro', '<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/photos/1/d3.jpg\" alt=\"\" width=\"800\" height=\"1000\" /></p>', '2019-03-13 03:45:10', '2019-03-13 18:36:39'),
(11, 2, 'judul cobaaa', 'judul-cobaaa', '<p style=\"text-align: left;\"><strong>sdasdasd</strong></p>', '2019-03-13 05:51:47', '2019-03-13 20:49:57');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_berita`
--

CREATE TABLE `tabel_berita` (
  `id` int(6) NOT NULL,
  `isi` varchar(5) NOT NULL,
  `gambar` varchar(5) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `jabatan`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'wawan', 'wawan@gmail.com', 'admin', '2019-03-04 10:01:44', '$2y$10$yvL.RUCDsRn2oUhPwQkZoOC59TG9i9lOvqGOOcsFcCbM4UYJFFKby', 'XcVBmTV9USqoaj1Z0RfzVlT3KE8KxwCBKg96L4TLB0Hp3Au5dsnZCZNf2nVQ', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(3, 'jte', 'jte@gmail.com', 'karyawan', '2019-03-04 10:01:44', '$2y$10$yvL.RUCDsRn2oUhPwQkZoOC59TG9i9lOvqGOOcsFcCbM4UYJFFKby', 'JZXUAGtP6DVwSvy8QbEPizniN3BGS9cVeD5qmLdO0KZ2UoygZ0QPBzsEQxa0', '2019-03-12 17:43:42', '2019-03-12 17:43:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_slug_unique` (`slug`);

--
-- Indexes for table `article_tag`
--
ALTER TABLE `article_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `borangs`
--
ALTER TABLE `borangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_post_id_foreign` (`post_id`),
  ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Indexes for table `df_file_handlers`
--
ALTER TABLE `df_file_handlers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_file_logs`
--
ALTER TABLE `df_file_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pid` (`pid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `action` (`action`),
  ADD KEY `date` (`date`);

--
-- Indexes for table `df_logs`
--
ALTER TABLE `df_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_modules_folders_notifications`
--
ALTER TABLE `df_modules_folders_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`,`pathid`);

--
-- Indexes for table `df_modules_metadata_dt`
--
ALTER TABLE `df_modules_metadata_dt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_modules_metadata_fields`
--
ALTER TABLE `df_modules_metadata_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `system` (`system`);

--
-- Indexes for table `df_modules_metadata_fieldsets`
--
ALTER TABLE `df_modules_metadata_fieldsets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_modules_metadata_files`
--
ALTER TABLE `df_modules_metadata_files`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pid` (`pid`),
  ADD KEY `type_id` (`type_id`);

--
-- Indexes for table `df_modules_metadata_values`
--
ALTER TABLE `df_modules_metadata_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date_added` (`date_added`),
  ADD KEY `uid` (`uid`,`file_id`,`field_id`),
  ADD KEY `file_id` (`file_id`,`field_id`),
  ADD KEY `uid_2` (`uid`,`field_id`),
  ADD KEY `val` (`val`(100)),
  ADD KEY `field_id` (`field_id`);

--
-- Indexes for table `df_modules_notifications`
--
ALTER TABLE `df_modules_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_modules_search_index_queue`
--
ALTER TABLE `df_modules_search_index_queue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_modules_shares`
--
ALTER TABLE `df_modules_shares`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`,`path`(248));

--
-- Indexes for table `df_modules_trash`
--
ALTER TABLE `df_modules_trash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_modules_user_groups`
--
ALTER TABLE `df_modules_user_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_modules_user_roles`
--
ALTER TABLE `df_modules_user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_modules_weblinks`
--
ALTER TABLE `df_modules_weblinks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_rnd` (`id_rnd`),
  ADD KEY `uid` (`uid`,`pathid`),
  ADD KEY `expiry` (`expiry`);

--
-- Indexes for table `df_notifications_logs`
--
ALTER TABLE `df_notifications_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_oauth_access_tokens`
--
ALTER TABLE `df_oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `access_token` (`access_token`),
  ADD KEY `sid` (`sid`);

--
-- Indexes for table `df_oauth_access_token_scopes`
--
ALTER TABLE `df_oauth_access_token_scopes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `access_token_id` (`access_token_id`),
  ADD KEY `scope` (`scope`);

--
-- Indexes for table `df_oauth_auth_codes`
--
ALTER TABLE `df_oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sid` (`sid`);

--
-- Indexes for table `df_oauth_auth_code_scopes`
--
ALTER TABLE `df_oauth_auth_code_scopes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auth_code_id` (`auth_code_id`),
  ADD KEY `scope` (`scope`);

--
-- Indexes for table `df_oauth_clients`
--
ALTER TABLE `df_oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cid` (`cid`);

--
-- Indexes for table `df_oauth_client_redirect_uris`
--
ALTER TABLE `df_oauth_client_redirect_uris`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cid` (`cid`);

--
-- Indexes for table `df_oauth_refresh_tokens`
--
ALTER TABLE `df_oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `refresh_token` (`refresh_token`),
  ADD KEY `access_token_id` (`access_token_id`);

--
-- Indexes for table `df_oauth_scopes`
--
ALTER TABLE `df_oauth_scopes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `df_oauth_sessions`
--
ALTER TABLE `df_oauth_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cid` (`cid`);

--
-- Indexes for table `df_oauth_session_scopes`
--
ALTER TABLE `df_oauth_session_scopes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sid` (`sid`),
  ADD KEY `scope` (`scope`);

--
-- Indexes for table `df_paths`
--
ALTER TABLE `df_paths`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uniq` (`uniq`),
  ADD KEY `filename` (`filename`(248),`depth`),
  ADD KEY `path` (`path`(250));

--
-- Indexes for table `df_relationships`
--
ALTER TABLE `df_relationships`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `object_id` (`object_id`,`relation_type`);

--
-- Indexes for table `df_settings`
--
ALTER TABLE `df_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `var` (`var`);

--
-- Indexes for table `df_users`
--
ALTER TABLE `df_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `expiration_date` (`expiration_date`);

--
-- Indexes for table `df_users_permissions`
--
ALTER TABLE `df_users_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uid` (`uid`);

--
-- Indexes for table `df_users_sessions`
--
ALTER TABLE `df_users_sessions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token` (`token`);

--
-- Indexes for table `mails`
--
ALTER TABLE `mails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Indexes for table `tabel_berita`
--
ALTER TABLE `tabel_berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_slug_unique` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `article_tag`
--
ALTER TABLE `article_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `borangs`
--
ALTER TABLE `borangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_file_handlers`
--
ALTER TABLE `df_file_handlers`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `df_file_logs`
--
ALTER TABLE `df_file_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_logs`
--
ALTER TABLE `df_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `df_modules_folders_notifications`
--
ALTER TABLE `df_modules_folders_notifications`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_modules_metadata_dt`
--
ALTER TABLE `df_modules_metadata_dt`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `df_modules_metadata_fields`
--
ALTER TABLE `df_modules_metadata_fields`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `df_modules_metadata_fieldsets`
--
ALTER TABLE `df_modules_metadata_fieldsets`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `df_modules_metadata_files`
--
ALTER TABLE `df_modules_metadata_files`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_modules_metadata_values`
--
ALTER TABLE `df_modules_metadata_values`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_modules_notifications`
--
ALTER TABLE `df_modules_notifications`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_modules_search_index_queue`
--
ALTER TABLE `df_modules_search_index_queue`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_modules_shares`
--
ALTER TABLE `df_modules_shares`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_modules_trash`
--
ALTER TABLE `df_modules_trash`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_modules_user_groups`
--
ALTER TABLE `df_modules_user_groups`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_modules_user_roles`
--
ALTER TABLE `df_modules_user_roles`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `df_modules_weblinks`
--
ALTER TABLE `df_modules_weblinks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_notifications_logs`
--
ALTER TABLE `df_notifications_logs`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_oauth_access_tokens`
--
ALTER TABLE `df_oauth_access_tokens`
  MODIFY `id` mediumint(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_oauth_access_token_scopes`
--
ALTER TABLE `df_oauth_access_token_scopes`
  MODIFY `id` mediumint(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_oauth_auth_codes`
--
ALTER TABLE `df_oauth_auth_codes`
  MODIFY `id` mediumint(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_oauth_auth_code_scopes`
--
ALTER TABLE `df_oauth_auth_code_scopes`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_oauth_clients`
--
ALTER TABLE `df_oauth_clients`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `df_oauth_client_redirect_uris`
--
ALTER TABLE `df_oauth_client_redirect_uris`
  MODIFY `id` mediumint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `df_oauth_refresh_tokens`
--
ALTER TABLE `df_oauth_refresh_tokens`
  MODIFY `id` mediumint(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_oauth_scopes`
--
ALTER TABLE `df_oauth_scopes`
  MODIFY `id` mediumint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `df_oauth_sessions`
--
ALTER TABLE `df_oauth_sessions`
  MODIFY `id` mediumint(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_oauth_session_scopes`
--
ALTER TABLE `df_oauth_session_scopes`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `df_paths`
--
ALTER TABLE `df_paths`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `df_relationships`
--
ALTER TABLE `df_relationships`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `df_settings`
--
ALTER TABLE `df_settings`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `df_users`
--
ALTER TABLE `df_users`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `df_users_permissions`
--
ALTER TABLE `df_users_permissions`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `df_users_sessions`
--
ALTER TABLE `df_users_sessions`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mails`
--
ALTER TABLE `mails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tabel_berita`
--
ALTER TABLE `tabel_berita`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `df_oauth_access_tokens`
--
ALTER TABLE `df_oauth_access_tokens`
  ADD CONSTRAINT `df_oauth_access_tokens_ibfk_1` FOREIGN KEY (`sid`) REFERENCES `df_oauth_sessions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `df_oauth_access_token_scopes`
--
ALTER TABLE `df_oauth_access_token_scopes`
  ADD CONSTRAINT `df_oauth_access_token_scopes_ibfk_1` FOREIGN KEY (`access_token_id`) REFERENCES `df_oauth_access_tokens` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `df_oauth_access_token_scopes_ibfk_2` FOREIGN KEY (`scope`) REFERENCES `df_oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `df_oauth_auth_codes`
--
ALTER TABLE `df_oauth_auth_codes`
  ADD CONSTRAINT `df_oauth_auth_codes_ibfk_1` FOREIGN KEY (`sid`) REFERENCES `df_oauth_sessions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `df_oauth_auth_code_scopes`
--
ALTER TABLE `df_oauth_auth_code_scopes`
  ADD CONSTRAINT `df_oauth_auth_code_scopes_ibfk_1` FOREIGN KEY (`auth_code_id`) REFERENCES `df_oauth_auth_codes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `df_oauth_auth_code_scopes_ibfk_2` FOREIGN KEY (`scope`) REFERENCES `df_oauth_scopes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `df_oauth_client_redirect_uris`
--
ALTER TABLE `df_oauth_client_redirect_uris`
  ADD CONSTRAINT `df_oauth_client_redirect_uris_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `df_oauth_clients` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `df_oauth_refresh_tokens`
--
ALTER TABLE `df_oauth_refresh_tokens`
  ADD CONSTRAINT `df_oauth_refresh_tokens_ibfk_1` FOREIGN KEY (`access_token_id`) REFERENCES `df_oauth_access_tokens` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `df_oauth_sessions`
--
ALTER TABLE `df_oauth_sessions`
  ADD CONSTRAINT `df_oauth_sessions_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `df_oauth_clients` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `df_oauth_session_scopes`
--
ALTER TABLE `df_oauth_session_scopes`
  ADD CONSTRAINT `df_oauth_session_scopes_ibfk_1` FOREIGN KEY (`scope`) REFERENCES `df_oauth_scopes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `df_oauth_session_scopes_ibfk_2` FOREIGN KEY (`sid`) REFERENCES `df_oauth_sessions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
