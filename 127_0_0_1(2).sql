-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2019 at 06:39 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `polnes`
--
CREATE DATABASE IF NOT EXISTS `polnes` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `polnes`;

-- --------------------------------------------------------

--
-- Table structure for table `alumnis`
--

CREATE TABLE `alumnis` (
  `id` int(10) UNSIGNED NOT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenjang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_angkatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pembimbing_1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pembimbing_2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alumnis`
--

INSERT INTO `alumnis` (`id`, `nim`, `nama`, `jenis_kelamin`, `hp`, `jenjang`, `tahun_angkatan`, `judul`, `pembimbing_1`, `pembimbing_2`, `created_at`, `updated_at`) VALUES
(1, '2014.201.000012', 'Abdul Hamid Kurniawan2', 'Laki-laki', '0857528846482', 'D-IV2', '20142', 'Membangun blabla2', 'Nama112', 'Nama221', '2019-03-31 16:00:00', '2019-04-17 10:00:29');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `article_tag`
--

CREATE TABLE `article_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `borangs`
--

CREATE TABLE `borangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenjang` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `butir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `borangs`
--

INSERT INTO `borangs` (`id`, `jenjang`, `jenis`, `butir`, `judul`, `link`, `created_at`, `updated_at`) VALUES
(1, 'D3', '3A', '3.1.1', 'DATA MAHASISWA REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1754659708', '2019-03-12 17:43:42', '2019-04-14 20:09:18'),
(2, 'D3', '3A', '3.1.3', 'JUMLAH MAHASISWA REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=400023082', '2019-03-19 09:58:54', '2019-04-14 20:09:24'),
(3, 'D3', '3A', '3.4.1', 'EVALUASI KINERJA LULUSAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1159503922', '2019-03-20 04:53:20', '2019-03-20 04:53:20'),
(4, 'D3', '3A', '3.4.5', 'LEMBAGA YANG MEMESAN LULUSAN UNTUK BEKERJA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1234972110', '2019-03-20 04:54:48', '2019-03-20 04:54:48'),
(5, 'D3', '3A', '4.3.1', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI BIDANG PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2008253868', '2019-03-20 04:56:00', '2019-03-20 04:56:00'),
(6, 'D3', '3A', '4.3.2', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA DI LUAR PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=911612053', '2019-03-20 04:56:56', '2019-03-20 04:57:04'),
(7, 'D3', '3A', '4.3.3', 'AKTIVITAS DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI DENGAN PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=677928931', '2019-03-20 04:57:53', '2019-03-20 04:57:53'),
(8, 'D3', '3A', '4.3.4', 'AKTIVITAS MENGAJAR DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI DENGAN PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2009756471', '2019-03-20 04:58:46', '2019-03-20 04:58:46'),
(9, 'D3', '3A', '4.3.5', 'AKTIVITAS MENGAJAR DOSEN TETAP YANG BIDANG KEAHLIANNYA DI LUAR PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=704277241', '2019-03-20 04:59:32', '2019-03-20 04:59:32'),
(10, 'D3', '3A', '4.4.1', 'DATA DOSEN TIDAK TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1442443622', '2019-03-20 21:47:53', '2019-03-20 21:47:53'),
(11, 'D3', '3A', '4.4.2', 'AKTIVITAS MENGAJAR DATA DOSEN TIDAK TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1542069104', '2019-03-20 21:48:48', '2019-03-20 21:48:48'),
(12, 'D3', '3A', '4.5.1', 'KEGIATAN TENAGA AHLI/PAKAR (TIDAK TERMASUK DOSEN TETAP)', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1111130371', '2019-03-20 21:49:19', '2019-03-20 21:49:19'),
(13, 'D3', '3A', '4.5.2', 'PENINGKATAN KEMAMPUAN DOSEN TETAP MELALUI TUGAS BELAJAR', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=556312004', '2019-03-20 21:49:59', '2019-03-20 21:49:59'),
(14, 'D3', '3A', '4.5.3', 'KEGIATAN DOSEN TETAP DALAM SEMINAR DLL', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=574063147', '2019-03-20 21:50:26', '2019-03-20 21:50:26'),
(15, 'D3', '3A', '4.5.5', 'KEIKUTSERTAAN DOSEN TETAP DALAM ORGANISASI KEILMUAN/PROFESI', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=489362432', '2019-03-20 21:50:57', '2019-03-20 21:50:57'),
(16, 'D3', '3A', '4.6.1', 'TENAGA KEPENDIDIKAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=962865596', '2019-03-20 21:51:28', '2019-03-20 21:51:28'),
(17, 'D3', '3A', '5.1.2.1', 'STRUKTUR KURIKULUM BERDASARKAN URUTAN MK', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=526796234', '2019-03-20 21:52:12', '2019-03-20 21:52:12'),
(18, 'D3', '3A', '5.2.2', 'WAKTU PELAKSANAAN REAL PROSES BELAJAR MENGAJAR', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1352004936', '2019-03-20 21:53:01', '2019-03-20 21:53:01'),
(19, 'D3', '3A', '5.4.1', 'DOSEN PEMBIMBING AKADEMIK DAN JUMLAH MAHASISWA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=435475362', '2019-03-20 21:53:32', '2019-03-20 21:53:32'),
(20, 'D3', '3A', '5.5.2', 'PELAKSANAAN PEMBIMBINGAN TUGAS AKHIR / SKRIPSI', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1890696461', '2019-03-20 21:54:02', '2019-03-20 21:54:02'),
(21, 'D3', '3A', '6.2.1.1', 'PEROLEHAN DAN ALOKASI DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1627222120', '2019-03-20 21:54:24', '2019-03-20 21:54:24'),
(22, 'D3', '3A', '6.2.1.2', 'PENGGUNAAN DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=355764836', '2019-03-20 21:55:03', '2019-03-20 21:55:03'),
(23, 'D3', '3A', '6.2.2', 'DANA UNTUK KEGIATAN PENELITIAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=620757463', '2019-03-20 21:55:36', '2019-03-20 21:55:36'),
(24, 'D3', '3A', '6.2.3', 'DANA PELAYANAN/PENGABDIAN KEPADA MASYARAKAT', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1720320204', '2019-03-20 21:56:11', '2019-03-20 21:56:11'),
(25, 'D3', '3A', '6.3.1', 'DATA RUANG KERJA DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1679379112', '2019-03-20 21:56:49', '2019-03-20 21:56:49'),
(26, 'D3', '3A', '6.4.1', 'KETERSEDIAAN PUSTAKA YANG RELEVAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2100609172', '2019-03-20 21:57:17', '2019-03-20 21:57:17'),
(27, 'D3', '3A', '6.5.2', 'AKSESIBILITAS TIAP JENIS DATA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=691052859', '2019-03-20 21:57:47', '2019-03-20 21:57:47'),
(28, 'D3', '3A', '7.1.1', 'PENELITIAN DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=443141801', '2019-03-20 21:58:09', '2019-03-20 21:58:09'),
(29, 'D3', '3A', '7.1.2', 'JUDUL ARTIKEL ILMIAH/KARYA ILMIAH/KARYA SENI/BUKU', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=830523945', '2019-03-20 21:58:36', '2019-03-20 21:58:36'),
(30, 'D3', '3A', '7.2.1', 'KEGIATAN PELAYANAN/PENGABDIAN KEPADA MASYARAKAT (PKM)', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=126265948', '2019-03-20 21:59:11', '2019-03-20 21:59:11'),
(31, 'D3', '3B', '3.1.2', 'DATA MAHASISWA REGULER DAN NON REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=145011840', '2019-03-20 22:00:02', '2019-03-20 22:00:02'),
(32, 'D3', '3B', '3.2.1', 'RATA-RATA MASA STUDI DAN IPK', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=341466229', '2019-03-20 22:00:28', '2019-03-20 22:00:28'),
(33, 'D3', '3B', '4.1.1', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI BIDANG PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1122135574', '2019-03-20 22:01:09', '2019-03-20 22:01:09'),
(34, 'D3', '3B', '4.1.2', 'PENGGANTIAN DAN PENGEMBANGAN DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=120875157', '2019-03-20 22:01:35', '2019-03-20 22:01:35'),
(35, 'D3', '3B', '4.2', 'TENAGA KEPENDIDIKAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=410668040', '2019-03-20 22:02:00', '2019-03-20 22:02:00'),
(36, 'D3', '3B', '6.1.1.1', 'JUMLAH DANA YANG DITERIMA FAKULTAS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1571891997', '2019-03-20 22:02:20', '2019-03-20 22:02:20'),
(37, 'D3', '3B', '6.1.1.2', 'PENGGUNAAN DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1088807175', '2019-03-20 22:02:42', '2019-03-20 22:02:42'),
(38, 'D3', '3A', '6.1.1.3', 'PENGGUNAAN DANA KEGIATAN TRIDARMA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1089064400', '2019-03-20 22:03:08', '2019-03-20 22:03:08'),
(39, 'D3', '3B', '6.4.2', 'AKSESIBILITAS DATA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1104280315', '2019-03-20 22:03:34', '2019-03-20 22:03:34'),
(40, 'D3', '3B', '7.1.1', 'JUMLAH DAN DANA PENELITIAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=463904113', '2019-03-20 22:03:55', '2019-03-20 22:03:55'),
(41, 'D3', '3B', '7.2.1', 'JUMLAH DAN DANA KEGIATAN PELAYANAN / PENGABDIAN KEPADA MASYARAKAT', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=81297899', '2019-03-20 22:04:42', '2019-04-15 00:01:36'),
(51, 'D4', '3A', '3.1.1', 'DATA MAHASISWA REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1754659708', '2019-03-12 09:43:42', '2019-04-14 12:09:18'),
(52, 'D4', '3A', '3.1.3', 'JUMLAH MAHASISWA REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=400023082', '2019-03-19 01:58:54', '2019-04-14 12:09:24'),
(53, 'D4', '3A', '3.4.1', 'EVALUASI KINERJA LULUSAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1159503922', '2019-03-19 20:53:20', '2019-03-19 20:53:20'),
(54, 'D4', '3A', '3.4.5', 'LEMBAGA YANG MEMESAN LULUSAN UNTUK BEKERJA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1234972110', '2019-03-19 20:54:48', '2019-03-19 20:54:48'),
(55, 'D4', '3A', '4.3.1', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI BIDANG PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2008253868', '2019-03-19 20:56:00', '2019-03-19 20:56:00'),
(56, 'D4', '3A', '4.3.2', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA DI LUAR PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=911612053', '2019-03-19 20:56:56', '2019-03-19 20:57:04'),
(57, 'D4', '3A', '4.3.3', 'AKTIVITAS DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI DENGAN PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=677928931', '2019-03-19 20:57:53', '2019-03-19 20:57:53'),
(58, 'D4', '3A', '4.3.4', 'AKTIVITAS MENGAJAR DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI DENGAN PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2009756471', '2019-03-19 20:58:46', '2019-03-19 20:58:46'),
(59, 'D4', '3A', '4.3.5', 'AKTIVITAS MENGAJAR DOSEN TETAP YANG BIDANG KEAHLIANNYA DI LUAR PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=704277241', '2019-03-19 20:59:32', '2019-03-19 20:59:32'),
(60, 'D4', '3A', '4.4.1', 'DATA DOSEN TIDAK TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1442443622', '2019-03-20 13:47:53', '2019-03-20 13:47:53'),
(61, 'D4', '3A', '4.4.2', 'AKTIVITAS MENGAJAR DATA DOSEN TIDAK TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1542069104', '2019-03-20 13:48:48', '2019-03-20 13:48:48'),
(62, 'D4', '3A', '4.5.1', 'KEGIATAN TENAGA AHLI/PAKAR (TIDAK TERMASUK DOSEN TETAP)', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1111130371', '2019-03-20 13:49:19', '2019-03-20 13:49:19'),
(63, 'D4', '3A', '4.5.2', 'PENINGKATAN KEMAMPUAN DOSEN TETAP MELALUI TUGAS BELAJAR', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=556312004', '2019-03-20 13:49:59', '2019-03-20 13:49:59'),
(64, 'D4', '3A', '4.5.3', 'KEGIATAN DOSEN TETAP DALAM SEMINAR DLL', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=574063147', '2019-03-20 13:50:26', '2019-03-20 13:50:26'),
(65, 'D4', '3A', '4.5.5', 'KEIKUTSERTAAN DOSEN TETAP DALAM ORGANISASI KEILMUAN/PROFESI', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=489362432', '2019-03-20 13:50:57', '2019-03-20 13:50:57'),
(66, 'D4', '3A', '4.6.1', 'TENAGA KEPENDIDIKAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=962865596', '2019-03-20 13:51:28', '2019-03-20 13:51:28'),
(67, 'D4', '3A', '5.1.2.1', 'STRUKTUR KURIKULUM BERDASARKAN URUTAN MK', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=526796234', '2019-03-20 13:52:12', '2019-03-20 13:52:12'),
(68, 'D4', '3A', '5.2.2', 'WAKTU PELAKSANAAN REAL PROSES BELAJAR MENGAJAR', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1352004936', '2019-03-20 13:53:01', '2019-03-20 13:53:01'),
(69, 'D4', '3A', '5.4.1', 'DOSEN PEMBIMBING AKADEMIK DAN JUMLAH MAHASISWA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=435475362', '2019-03-20 13:53:32', '2019-03-20 13:53:32'),
(70, 'D4', '3A', '5.5.2', 'PELAKSANAAN PEMBIMBINGAN TUGAS AKHIR / SKRIPSI', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1890696461', '2019-03-20 13:54:02', '2019-03-20 13:54:02'),
(71, 'D4', '3A', '6.2.1.1', 'PEROLEHAN DAN ALOKASI DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1627222120', '2019-03-20 13:54:24', '2019-03-20 13:54:24'),
(72, 'D4', '3A', '6.2.1.2', 'PENGGUNAAN DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=355764836', '2019-03-20 13:55:03', '2019-03-20 13:55:03'),
(73, 'D4', '3A', '6.2.2', 'DANA UNTUK KEGIATAN PENELITIAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=620757463', '2019-03-20 13:55:36', '2019-03-20 13:55:36'),
(74, 'D4', '3A', '6.2.3', 'DANA PELAYANAN/PENGABDIAN KEPADA MASYARAKAT', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1720320204', '2019-03-20 13:56:11', '2019-03-20 13:56:11'),
(75, 'D4', '3A', '6.3.1', 'DATA RUANG KERJA DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1679379112', '2019-03-20 13:56:49', '2019-03-20 13:56:49'),
(76, 'D4', '3A', '6.4.1', 'KETERSEDIAAN PUSTAKA YANG RELEVAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2100609172', '2019-03-20 13:57:17', '2019-03-20 13:57:17'),
(77, 'D4', '3A', '6.5.2', 'AKSESIBILITAS TIAP JENIS DATA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=691052859', '2019-03-20 13:57:47', '2019-03-20 13:57:47'),
(78, 'D4', '3A', '7.1.1', 'PENELITIAN DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=443141801', '2019-03-20 13:58:09', '2019-03-20 13:58:09'),
(79, 'D4', '3A', '7.1.2', 'JUDUL ARTIKEL ILMIAH/KARYA ILMIAH/KARYA SENI/BUKU', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=830523945', '2019-03-20 13:58:36', '2019-03-20 13:58:36'),
(80, 'D4', '3A', '7.2.1', 'KEGIATAN PELAYANAN/PENGABDIAN KEPADA MASYARAKAT (PKM)', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=126265948', '2019-03-20 13:59:11', '2019-03-20 13:59:11'),
(81, 'D4', '3B', '3.1.2', 'DATA MAHASISWA REGULER DAN NON REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=145011840', '2019-03-20 14:00:02', '2019-03-20 14:00:02'),
(82, 'D4', '3B', '3.2.1', 'RATA-RATA MASA STUDI DAN IPK', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=341466229', '2019-03-20 14:00:28', '2019-03-20 14:00:28'),
(83, 'D4', '3B', '4.1.1', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI BIDANG PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1122135574', '2019-03-20 14:01:09', '2019-03-20 14:01:09'),
(84, 'D4', '3B', '4.1.2', 'PENGGANTIAN DAN PENGEMBANGAN DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=120875157', '2019-03-20 14:01:35', '2019-03-20 14:01:35'),
(85, 'D4', '3B', '4.2', 'TENAGA KEPENDIDIKAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=410668040', '2019-03-20 14:02:00', '2019-03-20 14:02:00'),
(86, 'D4', '3B', '6.1.1.1', 'JUMLAH DANA YANG DITERIMA FAKULTAS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1571891997', '2019-03-20 14:02:20', '2019-03-20 14:02:20'),
(87, 'D4', '3B', '6.1.1.2', 'PENGGUNAAN DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1088807175', '2019-03-20 14:02:42', '2019-03-20 14:02:42'),
(88, 'D4', '3A', '6.1.1.3', 'PENGGUNAAN DANA KEGIATAN TRIDARMA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1089064400', '2019-03-20 14:03:08', '2019-03-20 14:03:08'),
(89, 'D4', '3B', '6.4.2', 'AKSESIBILITAS DATA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1104280315', '2019-03-20 14:03:34', '2019-03-20 14:03:34'),
(90, 'D4', '3B', '7.1.1', 'JUMLAH DAN DANA PENELITIAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=463904113', '2019-03-20 14:03:55', '2019-03-20 14:03:55'),
(91, 'D4', '3B', '7.2.1', 'JUMLAH DAN DANA KEGIATAN PELAYANAN / PENGABDIAN KEPADA MASYARAKAT', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=81297899', '2019-03-20 14:04:42', '2019-04-14 16:01:36');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Profil', 'profil', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(2, 'Informasi Akademik', 'informasi-akademik', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(3, 'E-Alumni', 'e-alumni', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(4, 'Kalender Akademik', 'kalender-akademik', '2019-03-12 17:43:42', '2019-03-12 17:43:42');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jabatans`
--

CREATE TABLE `jabatans` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jabatans`
--

INSERT INTO `jabatans` (`id`, `jabatan`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-05-08 17:21:15', '2019-05-08 17:21:15'),
(2, 'karyawan', '2019-05-08 17:21:15', '2019-05-08 17:21:15'),
(3, 'mahasiswa', '2019-05-08 17:21:15', '2019-05-08 17:21:15');

-- --------------------------------------------------------

--
-- Table structure for table `jurusans`
--

CREATE TABLE `jurusans` (
  `id` int(10) UNSIGNED NOT NULL,
  `jurusan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jurusans`
--

INSERT INTO `jurusans` (`id`, `jurusan`, `created_at`, `updated_at`) VALUES
(1, 'Teknik Listrik - DIII', '2019-05-08 17:21:19', '2019-05-08 17:21:19'),
(2, 'Teknik Listrik - DIV', '2019-05-08 17:21:19', '2019-05-08 17:21:19');

-- --------------------------------------------------------

--
-- Table structure for table `mails`
--

CREATE TABLE `mails` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `judul` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prodi` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mails`
--

INSERT INTO `mails` (`id`, `nomor`, `judul`, `tujuan`, `alamat`, `nim`, `nama`, `email`, `hp`, `prodi`, `jenis`, `created_at`, `updated_at`) VALUES
(9, NULL, NULL, NULL, NULL, '2010.201.00011', 'Fakhor Abdurrahman', 'Fakhor2gmail.com', '085752884648', 'Teknik Listrik - DIII', 'Surat Aktif', '2019-05-09 21:30:04', '2019-05-09 21:30:04'),
(10, NULL, NULL, 'Sucofindo', NULL, '2010.204.00101', 'Albab Buchori', 'buchori@gmail.com', '08125827830', 'Teknik Listrik - DIV', 'Tugas Akhir', '2019-05-09 21:35:07', '2019-05-09 21:35:07'),
(22, NULL, 'Membangun D3', 'Politeknik Negeri Samarinda', 'Jl. DR. Ciptomangunkusumo, Kampus Gunung Lipan, Samarinda, Kalimantan Timur', '2018.201.00001', 'mhs', 'mhs@mhs.com', '085752884648', 'Teknik Listrik - DIII', 'Kerja Praktek - DIII', '2019-05-16 23:27:19', '2019-05-16 23:27:19'),
(23, NULL, 'Membangun D3', 'Politeknik Negeri Samarinda', 'Jl. DR. Ciptomangunkusumo, Kampus Gunung Lipan, Samarinda, Kalimantan Timur', '2018.201.00001', 'mhs', 'mhs@mhs.com', '085752884648', 'Teknik Listrik - DIII', 'Tugas Akhir', '2019-05-16 23:27:35', '2019-05-16 23:27:35'),
(24, NULL, 'Membangun D4', 'Politeknik Negeri Samarinda', 'Jl. DR. Ciptomangunkusumo, Kampus Gunung Lipan, Samarinda, Kalimantan Timur', '2018.201.00001', 'mhs', 'mhs@mhs.com', '085752884648', 'Teknik Listrik - DIV', 'Kerja Praktek - DIV', '2019-05-16 23:27:53', '2019-05-16 23:27:53'),
(25, NULL, 'Membangun D4', 'Politeknik Negeri Samarinda', 'Jl. DR. Ciptomangunkusumo, Kampus Gunung Lipan, Samarinda, Kalimantan Timur', '2018.201.00001', 'mhs', 'mhs@mhs.com', '085752884648', 'Teknik Listrik - DIV', 'Skripsi', '2019-05-16 23:28:10', '2019-05-16 23:28:10'),
(26, NULL, NULL, NULL, NULL, '2018.201.00001', 'mhs', 'mhs@mhs.com', '085752884648', 'Teknik Listrik - DIII', 'Surat Aktif', '2019-05-16 23:28:21', '2019-05-16 23:28:21'),
(27, NULL, NULL, NULL, NULL, '2018.201.00001', 'mhs', 'mhs@mhs.com', '085752884648', 'Teknik Listrik - DIV', 'Surat Aktif', '2019-05-16 23:28:36', '2019-05-16 23:28:36');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(100, '2014_10_12_000000_create_users_table', 1),
(101, '2014_10_12_100000_create_password_resets_table', 1),
(102, '2019_02_25_112049_create_articles_table', 1),
(103, '2019_02_25_114415_create_tags_table', 1),
(104, '2019_02_25_114937_create_article_tag_table', 1),
(105, '2019_03_01_130823_create_categories_table', 1),
(106, '2019_03_01_131911_create_posts_table', 1),
(107, '2019_03_01_132215_create_comments_table', 1),
(108, '2019_03_07_145458_create_mails_table', 1),
(109, '2019_03_19_165935_create_borangs_table', 2),
(110, '2019_04_17_144710_create_alumnis_table', 3),
(111, '2019_05_08_011813_create_surat_aktif_table', 4),
(114, '2019_05_08_080441_create_jabatan_table', 5),
(115, '2019_05_08_080639_create_jurusan_table', 5),
(116, '2019_05_08_080441_create_jabatans_table', 6),
(121, '2019_05_09_011844_create_jabatans_table', 7),
(122, '2019_05_09_011901_create_jurusans_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `title`, `slug`, `content`, `created_at`, `updated_at`) VALUES
(1, 2, 'judul cob', 'judul-cob', 'postspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostspostsposts', '2019-03-12 19:05:36', '2019-03-12 19:23:59'),
(2, 1, 'cat1', 'cat1', 'cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1cat1', '2019-03-12 19:35:26', '2019-03-12 19:35:26'),
(3, 3, 'cat3', 'cat3', 'cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3cat3', '2019-03-12 19:35:42', '2019-03-12 19:35:42'),
(4, 4, 'cat4', 'cat4', 'cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4cat4', '2019-03-12 19:35:52', '2019-03-12 19:35:52'),
(5, 2, 'cat2', 'wkkwkwkw', 'ddddddddddddddddddddd', '2019-03-12 20:26:45', '2019-03-12 20:52:30'),
(6, 2, 'cat2', 'wkkwkwkw', 'ddddddddddddddddddddd', '2019-03-12 20:26:45', '2019-03-12 20:52:30'),
(7, 2, 'cat2', 'wkkwkwkw', 'ddddddddddddddddddddd', '2019-03-12 20:26:45', '2019-03-12 20:52:30'),
(8, 4, 'Kalender Akademik 2018 - 2019', 'cat4', '<p><img src=\"/photos/1/kalender akademik.jpg\" alt=\"\" width=\"1050\" height=\"525\" /></p>', '2019-03-12 21:35:52', '2019-03-13 20:24:20'),
(9, 1, 'Profil Jurusan Teknik Elektro', 'profil-jurusan-teknik-elektro', '<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/photos/1/d3.jpg\" alt=\"\" width=\"800\" height=\"1000\" /></p>', '2019-03-13 03:45:10', '2019-03-13 18:36:39'),
(11, 2, 'judul cobaaa', 'judul-cobaaa', '<p style=\"text-align: left;\"><strong>sdasdasd</strong></p>', '2019-03-13 05:51:47', '2019-03-13 20:49:57');

-- --------------------------------------------------------

--
-- Table structure for table `surat_aktif`
--

CREATE TABLE `surat_aktif` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prodi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_berita`
--

CREATE TABLE `tabel_berita` (
  `id` int(6) NOT NULL,
  `isi` varchar(5) NOT NULL,
  `gambar` varchar(5) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `jabatan`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'wawan', 'wawan@gmail.com', 'admin', '2019-03-04 10:01:44', '$2y$10$yvL.RUCDsRn2oUhPwQkZoOC59TG9i9lOvqGOOcsFcCbM4UYJFFKby', 'Lr4dbjdFsGrwX9d4R5zk8mUv0ARASv6EotQPJ9s06Oj0J53mJpjcjFBM2WLm', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(5, 'Erry Yadie', 'erryyadie@polnes.ac.id', 'karyawan', '2019-04-05 00:41:26', '$2y$10$1lMGYZeMmiqbSJUpcj0b2.giS09RFsvhxm7orsastXBfu3yI7Xjm6', 'hpFjqWdAwg4lIDpktKoc8iN9Gg0jgIt2Qg0kaPMmS4GXG6HRPgRtECMpAKkB', '2019-04-05 00:31:14', '2019-05-08 17:26:48'),
(6, 'mhs', 'mhs@mhs.com', 'mahasiswa', '2019-04-05 00:41:26', '$2y$10$itC5HFR6xH/9I/IPzCPgwejmff7N8eHqU2bItGPQRbJfjYprxUeEG', NULL, '2019-05-07 21:38:08', '2019-05-07 21:38:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alumnis`
--
ALTER TABLE `alumnis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_slug_unique` (`slug`);

--
-- Indexes for table `article_tag`
--
ALTER TABLE `article_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `borangs`
--
ALTER TABLE `borangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_post_id_foreign` (`post_id`),
  ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Indexes for table `jabatans`
--
ALTER TABLE `jabatans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurusans`
--
ALTER TABLE `jurusans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mails`
--
ALTER TABLE `mails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Indexes for table `surat_aktif`
--
ALTER TABLE `surat_aktif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_berita`
--
ALTER TABLE `tabel_berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_slug_unique` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alumnis`
--
ALTER TABLE `alumnis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `article_tag`
--
ALTER TABLE `article_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `borangs`
--
ALTER TABLE `borangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jabatans`
--
ALTER TABLE `jabatans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jurusans`
--
ALTER TABLE `jurusans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mails`
--
ALTER TABLE `mails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `surat_aktif`
--
ALTER TABLE `surat_aktif`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_berita`
--
ALTER TABLE `tabel_berita`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
