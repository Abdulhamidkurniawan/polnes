-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 08, 2021 at 02:32 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `polnes`
--

-- --------------------------------------------------------

--
-- Table structure for table `absens`
--

CREATE TABLE `absens` (
  `id` int(10) UNSIGNED NOT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_mk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dosen` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jurusan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prodi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_ajaran` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `semester` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kelas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah_jam` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jam_pertemuan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `materi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persetujuan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `alumnis`
--

CREATE TABLE `alumnis` (
  `id` int(10) UNSIGNED NOT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenjang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_angkatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pembimbing_1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pembimbing_2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `article_tag`
--

CREATE TABLE `article_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `auth_sources`
--

CREATE TABLE `auth_sources` (
  `auth_id` bigint(20) NOT NULL,
  `title` varchar(60) NOT NULL,
  `plugin` varchar(32) NOT NULL,
  `auth_default` tinyint(4) NOT NULL DEFAULT '0',
  `settings` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `borangs`
--

CREATE TABLE `borangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenjang` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `butir` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `borangs`
--

INSERT INTO `borangs` (`id`, `jenjang`, `jenis`, `butir`, `judul`, `link`, `created_at`, `updated_at`) VALUES
(1, 'D3', '3A', '3.1.1', 'DATA MAHASISWA REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1754659708', '2019-03-12 17:43:42', '2019-04-14 20:09:18'),
(2, 'D3', '3A', '3.1.3', 'JUMLAH MAHASISWA REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=400023082', '2019-03-19 09:58:54', '2019-04-14 20:09:24'),
(3, 'D3', '3A', '3.4.1', 'EVALUASI KINERJA LULUSAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1159503922', '2019-03-20 04:53:20', '2019-03-20 04:53:20'),
(4, 'D3', '3A', '3.4.5', 'LEMBAGA YANG MEMESAN LULUSAN UNTUK BEKERJA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1234972110', '2019-03-20 04:54:48', '2019-03-20 04:54:48'),
(5, 'D3', '3A', '4.3.1', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI BIDANG PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2008253868', '2019-03-20 04:56:00', '2019-03-20 04:56:00'),
(6, 'D3', '3A', '4.3.2', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA DI LUAR PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=911612053', '2019-03-20 04:56:56', '2019-03-20 04:57:04'),
(7, 'D3', '3A', '4.3.3', 'AKTIVITAS DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI DENGAN PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=677928931', '2019-03-20 04:57:53', '2019-03-20 04:57:53'),
(8, 'D3', '3A', '4.3.4', 'AKTIVITAS MENGAJAR DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI DENGAN PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2009756471', '2019-03-20 04:58:46', '2019-03-20 04:58:46'),
(9, 'D3', '3A', '4.3.5', 'AKTIVITAS MENGAJAR DOSEN TETAP YANG BIDANG KEAHLIANNYA DI LUAR PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=704277241', '2019-03-20 04:59:32', '2019-03-20 04:59:32'),
(10, 'D3', '3A', '4.4.1', 'DATA DOSEN TIDAK TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1442443622', '2019-03-20 21:47:53', '2019-03-20 21:47:53'),
(11, 'D3', '3A', '4.4.2', 'AKTIVITAS MENGAJAR DATA DOSEN TIDAK TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1542069104', '2019-03-20 21:48:48', '2019-03-20 21:48:48'),
(12, 'D3', '3A', '4.5.1', 'KEGIATAN TENAGA AHLI/PAKAR (TIDAK TERMASUK DOSEN TETAP)', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1111130371', '2019-03-20 21:49:19', '2019-03-20 21:49:19'),
(13, 'D3', '3A', '4.5.2', 'PENINGKATAN KEMAMPUAN DOSEN TETAP MELALUI TUGAS BELAJAR', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=556312004', '2019-03-20 21:49:59', '2019-03-20 21:49:59'),
(14, 'D3', '3A', '4.5.3', 'KEGIATAN DOSEN TETAP DALAM SEMINAR DLL', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=574063147', '2019-03-20 21:50:26', '2019-03-20 21:50:26'),
(15, 'D3', '3A', '4.5.5', 'KEIKUTSERTAAN DOSEN TETAP DALAM ORGANISASI KEILMUAN/PROFESI', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=489362432', '2019-03-20 21:50:57', '2019-03-20 21:50:57'),
(16, 'D3', '3A', '4.6.1', 'TENAGA KEPENDIDIKAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=962865596', '2019-03-20 21:51:28', '2019-03-20 21:51:28'),
(17, 'D3', '3A', '5.1.2.1', 'STRUKTUR KURIKULUM BERDASARKAN URUTAN MK', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=526796234', '2019-03-20 21:52:12', '2019-03-20 21:52:12'),
(18, 'D3', '3A', '5.2.2', 'WAKTU PELAKSANAAN REAL PROSES BELAJAR MENGAJAR', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1352004936', '2019-03-20 21:53:01', '2019-03-20 21:53:01'),
(19, 'D3', '3A', '5.4.1', 'DOSEN PEMBIMBING AKADEMIK DAN JUMLAH MAHASISWA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=435475362', '2019-03-20 21:53:32', '2019-03-20 21:53:32'),
(20, 'D3', '3A', '5.5.2', 'PELAKSANAAN PEMBIMBINGAN TUGAS AKHIR / SKRIPSI', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1890696461', '2019-03-20 21:54:02', '2019-03-20 21:54:02'),
(21, 'D3', '3A', '6.2.1.1', 'PEROLEHAN DAN ALOKASI DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1627222120', '2019-03-20 21:54:24', '2019-03-20 21:54:24'),
(22, 'D3', '3A', '6.2.1.2', 'PENGGUNAAN DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=355764836', '2019-03-20 21:55:03', '2019-03-20 21:55:03'),
(23, 'D3', '3A', '6.2.2', 'DANA UNTUK KEGIATAN PENELITIAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=620757463', '2019-03-20 21:55:36', '2019-03-20 21:55:36'),
(24, 'D3', '3A', '6.2.3', 'DANA PELAYANAN/PENGABDIAN KEPADA MASYARAKAT', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1720320204', '2019-03-20 21:56:11', '2019-03-20 21:56:11'),
(25, 'D3', '3A', '6.3.1', 'DATA RUANG KERJA DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1679379112', '2019-03-20 21:56:49', '2019-03-20 21:56:49'),
(26, 'D3', '3A', '6.4.1', 'KETERSEDIAAN PUSTAKA YANG RELEVAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2100609172', '2019-03-20 21:57:17', '2019-03-20 21:57:17'),
(27, 'D3', '3A', '6.5.2', 'AKSESIBILITAS TIAP JENIS DATA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=691052859', '2019-03-20 21:57:47', '2019-03-20 21:57:47'),
(28, 'D3', '3A', '7.1.1', 'PENELITIAN DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=443141801', '2019-03-20 21:58:09', '2019-03-20 21:58:09'),
(29, 'D3', '3A', '7.1.2', 'JUDUL ARTIKEL ILMIAH/KARYA ILMIAH/KARYA SENI/BUKU', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=830523945', '2019-03-20 21:58:36', '2019-03-20 21:58:36'),
(30, 'D3', '3A', '7.2.1', 'KEGIATAN PELAYANAN/PENGABDIAN KEPADA MASYARAKAT (PKM)', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=126265948', '2019-03-20 21:59:11', '2019-03-20 21:59:11'),
(31, 'D3', '3B', '3.1.2', 'DATA MAHASISWA REGULER DAN NON REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=145011840', '2019-03-20 22:00:02', '2019-03-20 22:00:02'),
(32, 'D3', '3B', '3.2.1', 'RATA-RATA MASA STUDI DAN IPK', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=341466229', '2019-03-20 22:00:28', '2019-03-20 22:00:28'),
(33, 'D3', '3B', '4.1.1', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI BIDANG PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1122135574', '2019-03-20 22:01:09', '2019-03-20 22:01:09'),
(34, 'D3', '3B', '4.1.2', 'PENGGANTIAN DAN PENGEMBANGAN DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=120875157', '2019-03-20 22:01:35', '2019-03-20 22:01:35'),
(35, 'D3', '3B', '4.2', 'TENAGA KEPENDIDIKAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=410668040', '2019-03-20 22:02:00', '2019-03-20 22:02:00'),
(36, 'D3', '3B', '6.1.1.1', 'JUMLAH DANA YANG DITERIMA FAKULTAS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1571891997', '2019-03-20 22:02:20', '2019-03-20 22:02:20'),
(37, 'D3', '3B', '6.1.1.2', 'PENGGUNAAN DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1088807175', '2019-03-20 22:02:42', '2019-03-20 22:02:42'),
(38, 'D3', '3A', '6.1.1.3', 'PENGGUNAAN DANA KEGIATAN TRIDARMA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1089064400', '2019-03-20 22:03:08', '2019-03-20 22:03:08'),
(39, 'D3', '3B', '6.4.2', 'AKSESIBILITAS DATA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1104280315', '2019-03-20 22:03:34', '2019-03-20 22:03:34'),
(40, 'D3', '3B', '7.1.1', 'JUMLAH DAN DANA PENELITIAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=463904113', '2019-03-20 22:03:55', '2019-03-20 22:03:55'),
(41, 'D3', '3B', '7.2.1', 'JUMLAH DAN DANA KEGIATAN PELAYANAN / PENGABDIAN KEPADA MASYARAKAT', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=81297899', '2019-03-20 22:04:42', '2019-04-15 00:01:36'),
(51, 'D4', '3A', '3.1.1', 'DATA MAHASISWA REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1754659708', '2019-03-12 09:43:42', '2019-04-14 12:09:18'),
(52, 'D4', '3A', '3.1.3', 'JUMLAH MAHASISWA REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=400023082', '2019-03-19 01:58:54', '2019-04-14 12:09:24'),
(53, 'D4', '3A', '3.4.1', 'EVALUASI KINERJA LULUSAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1159503922', '2019-03-19 20:53:20', '2019-03-19 20:53:20'),
(54, 'D4', '3A', '3.4.5', 'LEMBAGA YANG MEMESAN LULUSAN UNTUK BEKERJA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1234972110', '2019-03-19 20:54:48', '2019-03-19 20:54:48'),
(55, 'D4', '3A', '4.3.1', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI BIDANG PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2008253868', '2019-03-19 20:56:00', '2019-03-19 20:56:00'),
(56, 'D4', '3A', '4.3.2', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA DI LUAR PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=911612053', '2019-03-19 20:56:56', '2019-03-19 20:57:04'),
(57, 'D4', '3A', '4.3.3', 'AKTIVITAS DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI DENGAN PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=677928931', '2019-03-19 20:57:53', '2019-03-19 20:57:53'),
(58, 'D4', '3A', '4.3.4', 'AKTIVITAS MENGAJAR DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI DENGAN PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2009756471', '2019-03-19 20:58:46', '2019-03-19 20:58:46'),
(59, 'D4', '3A', '4.3.5', 'AKTIVITAS MENGAJAR DOSEN TETAP YANG BIDANG KEAHLIANNYA DI LUAR PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=704277241', '2019-03-19 20:59:32', '2019-03-19 20:59:32'),
(60, 'D4', '3A', '4.4.1', 'DATA DOSEN TIDAK TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1442443622', '2019-03-20 13:47:53', '2019-03-20 13:47:53'),
(61, 'D4', '3A', '4.4.2', 'AKTIVITAS MENGAJAR DATA DOSEN TIDAK TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1542069104', '2019-03-20 13:48:48', '2019-03-20 13:48:48'),
(62, 'D4', '3A', '4.5.1', 'KEGIATAN TENAGA AHLI/PAKAR (TIDAK TERMASUK DOSEN TETAP)', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1111130371', '2019-03-20 13:49:19', '2019-03-20 13:49:19'),
(63, 'D4', '3A', '4.5.2', 'PENINGKATAN KEMAMPUAN DOSEN TETAP MELALUI TUGAS BELAJAR', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=556312004', '2019-03-20 13:49:59', '2019-03-20 13:49:59'),
(64, 'D4', '3A', '4.5.3', 'KEGIATAN DOSEN TETAP DALAM SEMINAR DLL', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=574063147', '2019-03-20 13:50:26', '2019-03-20 13:50:26'),
(65, 'D4', '3A', '4.5.5', 'KEIKUTSERTAAN DOSEN TETAP DALAM ORGANISASI KEILMUAN/PROFESI', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=489362432', '2019-03-20 13:50:57', '2019-03-20 13:50:57'),
(66, 'D4', '3A', '4.6.1', 'TENAGA KEPENDIDIKAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=962865596', '2019-03-20 13:51:28', '2019-03-20 13:51:28'),
(67, 'D4', '3A', '5.1.2.1', 'STRUKTUR KURIKULUM BERDASARKAN URUTAN MK', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=526796234', '2019-03-20 13:52:12', '2019-03-20 13:52:12'),
(68, 'D4', '3A', '5.2.2', 'WAKTU PELAKSANAAN REAL PROSES BELAJAR MENGAJAR', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1352004936', '2019-03-20 13:53:01', '2019-03-20 13:53:01'),
(69, 'D4', '3A', '5.4.1', 'DOSEN PEMBIMBING AKADEMIK DAN JUMLAH MAHASISWA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=435475362', '2019-03-20 13:53:32', '2019-03-20 13:53:32'),
(70, 'D4', '3A', '5.5.2', 'PELAKSANAAN PEMBIMBINGAN TUGAS AKHIR / SKRIPSI', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1890696461', '2019-03-20 13:54:02', '2019-03-20 13:54:02'),
(71, 'D4', '3A', '6.2.1.1', 'PEROLEHAN DAN ALOKASI DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1627222120', '2019-03-20 13:54:24', '2019-03-20 13:54:24'),
(72, 'D4', '3A', '6.2.1.2', 'PENGGUNAAN DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=355764836', '2019-03-20 13:55:03', '2019-03-20 13:55:03'),
(73, 'D4', '3A', '6.2.2', 'DANA UNTUK KEGIATAN PENELITIAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=620757463', '2019-03-20 13:55:36', '2019-03-20 13:55:36'),
(74, 'D4', '3A', '6.2.3', 'DANA PELAYANAN/PENGABDIAN KEPADA MASYARAKAT', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1720320204', '2019-03-20 13:56:11', '2019-03-20 13:56:11'),
(75, 'D4', '3A', '6.3.1', 'DATA RUANG KERJA DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1679379112', '2019-03-20 13:56:49', '2019-03-20 13:56:49'),
(76, 'D4', '3A', '6.4.1', 'KETERSEDIAAN PUSTAKA YANG RELEVAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=2100609172', '2019-03-20 13:57:17', '2019-03-20 13:57:17'),
(77, 'D4', '3A', '6.5.2', 'AKSESIBILITAS TIAP JENIS DATA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=691052859', '2019-03-20 13:57:47', '2019-03-20 13:57:47'),
(78, 'D4', '3A', '7.1.1', 'PENELITIAN DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=443141801', '2019-03-20 13:58:09', '2019-03-20 13:58:09'),
(79, 'D4', '3A', '7.1.2', 'JUDUL ARTIKEL ILMIAH/KARYA ILMIAH/KARYA SENI/BUKU', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=830523945', '2019-03-20 13:58:36', '2019-03-20 13:58:36'),
(80, 'D4', '3A', '7.2.1', 'KEGIATAN PELAYANAN/PENGABDIAN KEPADA MASYARAKAT (PKM)', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=126265948', '2019-03-20 13:59:11', '2019-03-20 13:59:11'),
(81, 'D4', '3B', '3.1.2', 'DATA MAHASISWA REGULER DAN NON REGULER', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=145011840', '2019-03-20 14:00:02', '2019-03-20 14:00:02'),
(82, 'D4', '3B', '3.2.1', 'RATA-RATA MASA STUDI DAN IPK', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=341466229', '2019-03-20 14:00:28', '2019-03-20 14:00:28'),
(83, 'D4', '3B', '4.1.1', 'DOSEN TETAP YANG BIDANG KEAHLIANNYA SESUAI BIDANG PS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1122135574', '2019-03-20 14:01:09', '2019-03-20 14:01:09'),
(84, 'D4', '3B', '4.1.2', 'PENGGANTIAN DAN PENGEMBANGAN DOSEN TETAP', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=120875157', '2019-03-20 14:01:35', '2019-03-20 14:01:35'),
(85, 'D4', '3B', '4.2', 'TENAGA KEPENDIDIKAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=410668040', '2019-03-20 14:02:00', '2019-03-20 14:02:00'),
(86, 'D4', '3B', '6.1.1.1', 'JUMLAH DANA YANG DITERIMA FAKULTAS', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1571891997', '2019-03-20 14:02:20', '2019-03-20 14:02:20'),
(87, 'D4', '3B', '6.1.1.2', 'PENGGUNAAN DANA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1088807175', '2019-03-20 14:02:42', '2019-03-20 14:02:42'),
(88, 'D4', '3A', '6.1.1.3', 'PENGGUNAAN DANA KEGIATAN TRIDARMA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1089064400', '2019-03-20 14:03:08', '2019-03-20 14:03:08'),
(89, 'D4', '3B', '6.4.2', 'AKSESIBILITAS DATA', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=1104280315', '2019-03-20 14:03:34', '2019-03-20 14:03:34'),
(90, 'D4', '3B', '7.1.1', 'JUMLAH DAN DANA PENELITIAN', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=463904113', '2019-03-20 14:03:55', '2019-03-20 14:03:55'),
(91, 'D4', '3B', '7.2.1', 'JUMLAH DAN DANA KEGIATAN PELAYANAN / PENGABDIAN KEPADA MASYARAKAT', 'https://docs.google.com/spreadsheets/d/12YY6lTMXoy3LSYR8KLZlHbEurm6m5aHs4pbG1W4b4PI/edit#gid=81297899', '2019-03-20 14:04:42', '2019-04-14 16:01:36');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Profil', 'profil', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(2, 'Informasi Akademik', 'informasi-akademik', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(3, 'E-Alumni', 'e-alumni', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(4, 'Kalender Akademik', 'kalender-akademik', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(5, 'Prodi-D3', 'prodi-d3', '2019-03-12 17:43:42', '2019-03-12 17:43:42'),
(6, 'Prodi-D4', 'prodi-d4', '2019-03-12 17:43:42', '2019-03-12 17:43:42');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `dosens`
--

CREATE TABLE `dosens` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nidn` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dosens`
--

INSERT INTO `dosens` (`id`, `nama`, `nip`, `nidn`, `created_at`, `updated_at`) VALUES
(1, 'Abdul Hamid, S.Kom., M.T.I', '199302282019031000', '1128029301', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(2, 'Abdul Rahman, LC., M.Fil.I', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(3, 'Dadang Suherman, S.S.T., M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(4, 'Dr. Ir. Prihadi Murdiyat, M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(5, 'Drs. Abdurrahim, M.Hum', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(6, 'Drs. La Bima, M. Si', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(7, 'Erry Yadie, S.T., M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(8, 'Hari Subagyo, S.S.T., M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(9, 'Ipniansyah, S.T., M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(10, 'Ir. Arbain P., M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(11, 'Ir. Bustani, M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(12, 'Ir. Cornelius Sarri, M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(13, 'Ir. Masing, M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(14, 'Ir. Muhammad Syahrir Djalil, M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(15, 'Ir. Muhammad Zainuddin, M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(16, 'Ir. Rusda, S.T., M.T.', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(17, 'Khairuddin Karim, S.T., M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(18, 'Lucianus Handri Gunanto, S.T., M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(19, 'Onglan Nainggolan, S.T., M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(20, 'Qomaruddin, S.T., M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(21, 'Rusdiansyah, S.T., M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(22, 'Subir, S.T., M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(23, 'Sunu Pradana, S.T., M.Eng', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(24, 'Suratno, S.T., M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(25, 'Toyib, S.T., M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11'),
(26, 'Verra Aullia, S.T., M. T', '0', '0', '2021-03-14 16:57:11', '2021-03-14 16:57:11');

-- --------------------------------------------------------

--
-- Table structure for table `jabatans`
--

CREATE TABLE `jabatans` (
  `id` int(10) UNSIGNED NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jabatans`
--

INSERT INTO `jabatans` (`id`, `jabatan`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-05-08 17:21:15', '2019-05-08 17:21:15'),
(2, 'karyawan', '2019-05-08 17:21:15', '2019-05-08 17:21:15'),
(3, 'mahasiswa', '2019-05-08 17:21:15', '2019-05-08 17:21:15');

-- --------------------------------------------------------

--
-- Table structure for table `jadwals`
--

CREATE TABLE `jadwals` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode_mk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_mk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dosen_mk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jam_awal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jam_akhir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jurusan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prodi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kelas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jams`
--

CREATE TABLE `jams` (
  `id` int(10) UNSIGNED NOT NULL,
  `hari` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jam_awal` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jam_akhir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jurusans`
--

CREATE TABLE `jurusans` (
  `id` int(10) UNSIGNED NOT NULL,
  `jurusan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jurusans`
--

INSERT INTO `jurusans` (`id`, `jurusan`, `created_at`, `updated_at`) VALUES
(1, 'Teknik Listrik - DIII', '2019-05-08 17:21:19', '2019-05-08 17:21:19'),
(2, 'Teknik Listrik - DIV', '2019-05-08 17:21:19', '2019-05-08 17:21:19');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswas`
--

CREATE TABLE `mahasiswas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jurusan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prodi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `angkatan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mahasiswas`
--

INSERT INTO `mahasiswas` (`id`, `nim`, `nama`, `jurusan`, `prodi`, `angkatan`, `status`, `created_at`, `updated_at`) VALUES
(1, '20642001', 'AKIF AL GHIFARI ROBBANI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(2, '20642002', 'DODI SAPUTRA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(3, '20642003', 'ALAN YUANDIKA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(4, '20642004', 'ARIA KHARISMA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(5, '20642005', 'GALANG NAZHRULLAH', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(6, '20642006', 'MUHAMMAD RAOULDHEAS AZHARY', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(7, '20642007', 'NAHARUDDIN SULIHIN', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(8, '20642008', 'BINTANG NUR JATI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(9, '20642009', 'RUSTINA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(10, '20642010', 'SATRYA DWI CAHYA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(11, '20642011', 'SYAHRUL FRATAMA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(12, '20642012', 'MUHAMMAD HAMIM', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(13, '20642013', 'AR\'SYAD', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(14, '20642014', 'M. SULTANI ALI HAFIDZ WARKANI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(15, '20642016', 'VASCO FRANSISKO WENGKANG', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(16, '20642017', 'EVAN KHOIRUL HUDA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(17, '20642018', 'FAIZAL RAHMANSYAH', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(18, '20642019', 'YORDAN ADI SAPUTRA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(19, '20642020', 'MARDIANTO PRATAMA PUTRA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(20, '20642021', 'MUHAMMAD DWIKI ABDILLAH', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(21, '20642022', 'FARISKY DIDEKTUS MODA SARE', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(22, '20642023', 'ANDI RAIHAN FITRA AHMAD', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(23, '20642024', 'SAMUEL HALOMOAN SITUMEANG', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(24, '20642025', 'DAFFA ALFARIZHI YUS', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(25, '20642026', 'NUR ABIDIN', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(26, '20642028', 'DIO RINALDI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(27, '20642029', 'PRIYONGGO HADI SULASONO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(28, '20642030', 'KUKUH ARRAHMAN', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(29, '20642031', 'AULIA HAMDANI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(30, '20642032', 'NUR AZIZAH HAYATI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(31, '20642033', 'MUHAMMAD RAHMAT RISKI MANURUNG', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(32, '20642034', 'LUMBAN RAJA WILSON SAMUEL', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(33, '20642035', 'MUHAMMAD KASMIR MAULANA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(34, '20642036', 'ARI RAMADHAN', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(35, '20642037', 'ACHMAD NUR JAELANI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(36, '20642038', 'MUHAMMAD HAFIZ ASHABULLAH', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(37, '20642039', 'MUCHAMMAD RIZKI AHLAN KAMAL', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(38, '20642040', 'RIZKY RACHMATULLAH', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(39, '20642041', 'ANDRE ANTONIUS', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(40, '20642042', 'IKHSAN AUREL RIYANTO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(41, '20642043', 'DAFFA REYSA PRAWIRA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(42, '20642045', 'DIVKI ARRIVAL ALFARIZI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(43, '20642046', 'RINA HARIANA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(44, '20642047', 'ABDUL GOFAR SAHIDDIN', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(45, '20642048', 'DECKYTRISARDI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(46, '20642049', 'MUHAMMAD RISKI HIDAYAT', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(47, '19642001', 'MUHAMMAD FAHREZA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(48, '19642002', 'MUHAMMAD ASKAR', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(49, '19642003', 'PATRIK HANDRIANO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(50, '19642004', 'DICKY CHANDRA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(51, '19642005', 'WAHYU ANANTYO AKBAR', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(52, '19642006', 'AUDI NUGRAHA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(53, '19642008', 'ZEIN AHMAD MALIKI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(54, '19642009', 'ALIFEVIOUS CHRISTIAN', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(55, '19642010', 'MUHAMMAD FAHRIZAL', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(56, '19642011', 'MUHAMMAD RIFKY FACHREZA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(57, '19642012', 'ABDUL RAHMAN W KAIMUDIN', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(58, '19642013', 'TITO SYUKUR PAMUNGKAS', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(59, '19642014', 'MUHAMMAD FARIZ PRASTHA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(60, '19642015', 'NIKOLAOS SULLE MANGAMPANG', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(61, '19642016', 'ANANG DWI PAMBUDI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(62, '19642017', 'DIHYA AHMAD RASYID RIDHO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(63, '19642018', 'KEVIN HABIB', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(64, '19642019', 'MUHAMMAD NAUFAL AKHBAR', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(65, '19642020', 'ZANWAR SUBEHI EKA PRATAMA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(66, '19642021', 'MUHAMMAD HAAZEM ANDRIONO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(67, '19642022', 'AHMAD ARIEF AHYANI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(68, '19642023', 'RICHO ANANDA SUPRIADI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(69, '19642024', 'RAHMADIVA AZZAHRA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(70, '19642025', 'HAFIDZ SATRIA WIBISONO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(71, '19642026', 'GAIL NOVTICHYENS PASAKA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(72, '19642031', 'RONALD MANGESA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(73, '19642032', 'DWI PUTRA RAMADHANY', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(74, '19642033', 'HARGREAVES MANOAH', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(75, '19642034', 'NUR AINI NABILAWATI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(76, '19642035', 'HENDRI HEZRON PATABANG', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(77, '19642036', 'FRANSISKUS SANDI T.', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(78, '19642037', 'RIZKY WAHYUDI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(79, '19642038', 'RICKY ISVANKA ', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(80, '19642039', 'ALFRED ROYNALDI. R', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(81, '19642040', 'ALMANDO TOGU PANGARIBUAN', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(82, '19642041', 'ERICO PRIYA PRATAMA UBRA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(83, '19642042', 'MUHAMMAD ERLIANSYAH', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(84, '19642043', 'MUHAMMAD ALI LUTHFI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(85, '19642044', 'YUSPAL LEBANG', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(86, '19642045', 'NOVRIEL DRIERY DANTE', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(87, '19642047', 'ACHMAD DHANI ANIL PUTRA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(88, '19642048', 'HENDRA NUR PRATOMO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(89, '19642049', 'IYANG MUKTI ALI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(90, '19642050', 'ARIF HIDAYAT', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(91, '19642051', 'VIRA AMALIA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(92, '19642052', 'MUHAMMAD SYUKUR ANDHIKA S', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(93, '19642053', 'LUIS MIGUEL', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(94, '19642055', 'MUHAMMAD ARYA SAPUTRA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(95, '18642001', 'INDRA SAPUTRA SURYA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(96, '18642002', 'TEGUH PRAMONO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(97, '18642003', 'FITRIA MAJIDAH', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(98, '18642004', 'MUHAMMAD SYAMZURI BAKRI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(99, '18642005', 'VOGIE PRAMES WARA LAKSONO PUTRA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(100, '18642006', 'FASCAL LUTHFI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(101, '18642007', 'BRILLIANTIE DAVIRA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(102, '18642008', 'DINDA AYU RAMADHANIYAH', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(103, '18642009', 'JAI YANSEN', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(104, '18642010', 'BAGAS DWI PRASETYO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(105, '18642011', 'DENI IRAWAN', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(106, '18642012', 'AGUNG WIJAYA PUTRA ', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(107, '18642013', 'APRILLIYAN MANUEL SUDIRO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(108, '18642014', 'TAUFIQ SAPUTRA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(109, '18642015', 'BAYU DWI CAHYONO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(110, '18642018', 'GOTMAN RENALDI SITUMORANG', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(111, '18642019', 'GRENYF CLAY SINAGA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(112, '18642020', 'RURY WILDAN ANGGORO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(113, '18642021', 'AHADIN AKBAR', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(114, '18642022', 'REYNER EDITRIAS T', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(115, '18642023', 'SEBULON BOYONG', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(116, '18642024', 'TRI ILHAM PRASETIYO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(117, '18642025', 'AHMAD IQBAL RUSDINI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(118, '18642026', 'MUNIRA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(119, '17642001', 'SAYUDI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(120, '17642002', 'RIZAL RENALDI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(121, '17642004', 'DAFFA SOHORKI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(122, '17642005', 'JURDIL HIDAYAT', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(123, '17642007', 'KAMAL HAKIM', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(124, '17642011', 'WAHYU PRATAMA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(125, '17642014', 'GITA BONITA ROFIKA PUTRI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(126, '17642015', 'MUHAMMAD MUGHNI', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(127, '17642016', 'HIKMAH ASSALAM', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(128, '17642018', 'MUHAMMAD AKBAR', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(129, '17642019', 'NOVA IKHSAN DARIS ', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(130, '17642021', 'ADAM WAHYU PRATAMA', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(131, '17642022', 'IGNASIUS REXY YULISTIAN', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(132, '17642024', 'BHEN ARIF WIJAKSONO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(133, '17642025', 'AFIF LUQMANUL HAKIM', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(134, '17642026', 'DONY NUGROHO', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(135, '17642027', 'YUHAL', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(136, '17642028', 'SEPTIYANA PUTRI ADININGTIAYAS', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29'),
(137, '17642029', 'TARUK ALLO, JHONSON', 'Teknik Elektro', 'D4', NULL, 'Aktif', '2021-03-14 19:44:29', '2021-03-14 19:44:29');

-- --------------------------------------------------------

--
-- Table structure for table `mails`
--

CREATE TABLE `mails` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `judul` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tujuan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prodi` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mails`
--

INSERT INTO `mails` (`id`, `nomor`, `judul`, `tujuan`, `alamat`, `nim`, `nama`, `email`, `hp`, `prodi`, `jenis`, `created_at`, `updated_at`) VALUES
(28, NULL, 'Analisis Efisiensi Pada Transformator CT Merk AAA', 'PT. Elektronik', 'Jl. Wiraswasta No.1B', '15642999', 'Choirul Huda', 'choirul.huda@gmail.com', '085752884648', 'Teknik Listrik - DIV', 'Surat Aktif', '2019-11-12 18:52:46', '2021-03-08 16:47:31');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(100, '2014_10_12_000000_create_users_table', 1),
(101, '2014_10_12_100000_create_password_resets_table', 1),
(102, '2019_02_25_112049_create_articles_table', 1),
(103, '2019_02_25_114415_create_tags_table', 1),
(104, '2019_02_25_114937_create_article_tag_table', 1),
(105, '2019_03_01_130823_create_categories_table', 1),
(106, '2019_03_01_131911_create_posts_table', 1),
(107, '2019_03_01_132215_create_comments_table', 1),
(108, '2019_03_07_145458_create_mails_table', 1),
(109, '2019_03_19_165935_create_borangs_table', 2),
(110, '2019_04_17_144710_create_alumnis_table', 3),
(111, '2019_05_08_011813_create_surat_aktif_table', 4),
(114, '2019_05_08_080441_create_jabatan_table', 5),
(115, '2019_05_08_080639_create_jurusan_table', 5),
(116, '2019_05_08_080441_create_jabatans_table', 6),
(121, '2019_05_09_011844_create_jabatans_table', 7),
(122, '2019_05_09_011901_create_jurusans_table', 7),
(139, '2021_03_09_031803_create_absens_table', 8),
(140, '2021_03_09_031810_create_tas_table', 8),
(141, '2021_03_09_031906_create_mks_table', 8),
(142, '2021_03_09_031951_create_jams_table', 8),
(143, '2021_03_09_032113_create_jadwals_table', 8),
(144, '2021_03_10_021505_create_dosens_table', 8),
(145, '2021_03_15_014505_create_mahasiswas_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `mks`
--

CREATE TABLE `mks` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode_mk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_mk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jurusan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prodi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `semester` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah_jam` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mks`
--

INSERT INTO `mks` (`id`, `kode_mk`, `nama_mk`, `jurusan`, `prodi`, `semester`, `sks`, `jumlah_jam`, `created_at`, `updated_at`) VALUES
(1, 'POL42102', 'Pendidikan Pancasila', 'Teknik Elektro', 'S1', '1', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(2, 'POL42103', 'Bahasa Indonesia', 'Teknik Elektro', 'S1', '1', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(3, 'PLT42134', 'Rancangan Listrik I', 'Teknik Elektro', 'S1', '1', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(4, 'PLT42122', 'Rangkaian Listrik I', 'Teknik Elektro', 'S1', '1', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(5, 'PLT42119', 'Matematika Terapan I', 'Teknik Elektro', 'S1', '1', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(6, 'PLT42109', 'Kimia Terapan', 'Teknik Elektro', 'S1', '1', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(7, 'PLT42111', 'Ilmu Bahan', 'Teknik Elektro', 'S1', '1', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(8, 'PLT42112', 'Gambar Teknik', 'Teknik Elektro', 'S1', '1', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(9, 'PLT42129', 'Instalasi Listrik I', 'Teknik Elektro', 'S1', '1', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(10, 'PLT42105', 'K3 dan Hukum Ketenagakerjaan', 'Teknik Elektro', 'S1', '1', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(11, 'PLT42158', 'Praktek Dasar Teknologi Mekanik', 'Teknik Elektro', 'S1', '1', '3', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(12, 'POL42201', 'Pendidikan Agama', 'Teknik Elektro', 'S1', '2', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(13, 'POL42204', 'Kewarganegaraan', 'Teknik Elektro', 'S1', '2', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(14, 'PLT42206', 'Bahasa Inggris Teknik I', 'Teknik Elektro', 'S1', '2', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(15, 'PLT42220', 'Matematika Terapan II', 'Teknik Elektro', 'S1', '2', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(16, 'PLT42223', 'Rangkaian Listrik II', 'Teknik Elektro', 'S1', '2', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(17, 'PLT42225', 'Elektronika ', 'Teknik Elektro', 'S1', '2', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(18, 'PLT42230', 'Instalasi Listrik II', 'Teknik Elektro', 'S1', '2', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(19, 'PLT42210', 'Fisika Terapan', 'Teknik Elektro', 'S1', '2', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(20, 'PLT42263', 'Lab. Listrik Dasar', 'Teknik Elektro', 'S1', '2', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(21, 'PLT42259', 'Praktek Instalasi Listrik I', 'Teknik Elektro', 'S1', '2', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(22, 'PLT42237', 'Mesin Listrik I', 'Teknik Elektro', 'S1', '2', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(23, 'PLT42307', 'Bahasa Inggris Teknik II', 'Teknik Elektro', 'S1', '3', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(24, 'PLT42324', 'Rangkaian Listrik III', 'Teknik Elektro', 'S1', '3', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(25, 'PLT42326', 'Elektronika Digital', 'Teknik Elektro', 'S1', '3', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(26, 'PLT42331', 'Instalasi Listrik III', 'Teknik Elektro', 'S1', '3', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(27, 'PLT42335', 'Rancangan Listrik II', 'Teknik Elektro', 'S1', '3', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(28, 'PLT42360', 'Praktek Instalasi Listrik II', 'Teknik Elektro', 'S1', '3', '3', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(29, 'PLT42364', 'Lab. Elektronika', 'Teknik Elektro', 'S1', '3', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(30, 'PLT42321', 'Matematika Terapan III', 'Teknik Elektro', 'S1', '3', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(31, 'PLT42338', 'Mesin Listrik II', 'Teknik Elektro', 'S1', '3', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(32, 'PLT42313', 'Konversi Energi', 'Teknik Elektro', 'S1', '3', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(33, 'PLT42408', 'Bahasa Inggris Teknik III', 'Teknik Elektro', 'S1', '4', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(34, 'PLT42432', 'Instalasi Listrik IV', 'Teknik Elektro', 'S1', '4', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(35, 'PLT42436', 'Rancangan Listrik III', 'Teknik Elektro', 'S1', '4', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(36, 'PLT42461', 'Praktek Instalasi Listrik III', 'Teknik Elektro', 'S1', '4', '3', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(37, 'PLT42466', 'Lab. Mesin Listrik/Trafo', 'Teknik Elektro', 'S1', '4', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(38, 'PLT42449', 'Instrumentasi Sistem Tenaga Listrik ', 'Teknik Elektro', 'S1', '4', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(39, 'PLT42445', 'Sistem Transmisi', 'Teknik Elektro', 'S1', '4', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(40, 'PLT42448', 'Sistem Proteksi', 'Teknik Elektro', 'S1', '4', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(41, 'PLT42465', 'Lab. Elektronika Digital', 'Teknik Elektro', 'S1', '4', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(42, 'PLT42440', 'Programmable Logic Controller I', 'Teknik Elektro', 'S1', '4', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(43, 'PLT42568', 'Lab. Sistem Transmisi dan Distribusi Tenaga Listrik', 'Teknik Elektro', 'S1', '5', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(44, 'PLT42542', 'Sistem Kontrol ', 'Teknik Elektro', 'S1', '5', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(45, 'PLT42539', 'Penggunaan Motor Listrik ', 'Teknik Elektro', 'S1', '5', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(46, 'PLT42527', 'Elektronika Daya I', 'Teknik Elektro', 'S1', '5', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(47, 'PLT42570', 'Lab. Elektronika Daya I', 'Teknik Elektro', 'S1', '5', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(48, 'PLT42541', 'Programmable Logic Controller II', 'Teknik Elektro', 'S1', '5', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(49, 'PLT42562', 'Praktek Instalasi Listrik IV', 'Teknik Elektro', 'S1', '5', '5', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(50, 'PLT42533', 'Instalasi Listrik V', 'Teknik Elektro', 'S1', '5', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(51, 'PLT42628', 'Elektronika Daya II', 'Teknik Elektro', 'S1', '6', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(52, 'PLT42671', 'Lab. Elektronika Daya II', 'Teknik Elektro', 'S1', '6', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(53, 'PLT42669', 'Lab. Sistem Proteksi', 'Teknik Elektro', 'S1', '6', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(54, 'PLT42644', 'Microprocessor/Microcontroller', 'Teknik Elektro', 'S1', '6', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(55, 'PLT42667', 'Lab. Mikroprosessor', 'Teknik Elektro', 'S1', '6', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(56, 'PLT42656', 'Kualitas Daya', 'Teknik Elektro', 'S1', '6', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(57, 'PLT42653', 'Analisis Sistem Tenaga Listrik 1', 'Teknik Elektro', 'S1', '6', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(58, 'PLT42650', 'Pemrograman Komputer', 'Teknik Elektro', 'S1', '6', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(59, 'PLT42643', 'Kontrol Sistem Tenaga', 'Teknik Elektro', 'S1', '6', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(60, 'PLT42617', 'Manajemen Industri dan MR', 'Teknik Elektro', 'S1', '6', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(61, 'PLT42715', 'Ekonomi Teknik', 'Teknik Elektro', 'S1', '7', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(62, 'PLT42757', 'Teknik Tegangan Tinggi', 'Teknik Elektro', 'S1', '7', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(63, 'PLT42754', 'Analisis Sistem Tenaga Listrik 2', 'Teknik Elektro', 'S1', '7', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(64, 'PLT42747', 'Gardu Induk dan Grounding', 'Teknik Elektro', 'S1', '7', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(65, 'PLT42774', 'Tata Tulis Karya Ilmiah', 'Teknik Elektro', 'S1', '7', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(66, 'PLT42752', 'Kecerdasan Buatan', 'Teknik Elektro', 'S1', '7', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(67, 'PLT42755', 'Operasi dan Pemeliharaan Sistem Tenaga Listrik', 'Teknik Elektro', 'S1', '7', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(68, 'PLT42772', 'Lab. Kualitas Daya', 'Teknik Elektro', 'S1', '7', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(69, 'PLT42773', 'Lab. Sistem Kontrol dan Instrumentasi', 'Teknik Elektro', 'S1', '7', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(70, 'PLT42751', 'Penggunaan Komputer pada Analisis Sistem Tenaga Listrik', 'Teknik Elektro', 'S1', '7', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(71, 'PLT42446', 'Sistem Distribusi Tenaga Listrik', 'Teknik Elektro', 'S1', '7', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(72, 'PLT42816', 'Kewirausahaan', 'Teknik Elektro', 'S1', '8', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(73, 'PLT42818', 'Manajemen Proyek', 'Teknik Elektro', 'S1', '8', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(74, 'PLT42814', 'Komunikasi dan Pengolahan Data', 'Teknik Elektro', 'S1', '8', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(75, 'PLT42875', 'Praktek Kerja Lapangan / PKL', 'Teknik Elektro', 'S1', '8', '2', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27'),
(76, 'PLT42876', 'Skripsi', 'Teknik Elektro', 'S1', '8', '4', '0', '2021-03-14 16:57:27', '2021-03-14 16:57:27');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `title`, `slug`, `gambar`, `content`, `created_at`, `updated_at`) VALUES
(25, 2, 'Judul', 'judul', '20-07-2020-hamid-329387896.png', '<p>ssssssssssssssssssssssssssssssssssssss</p>', '2020-07-19 23:57:01', '2020-07-20 00:06:49'),
(26, 5, 'Profil Prodi D3', 'profil-prodi-d3', NULL, '<p>Profil Prodi D3Profil Prodi D3</p>', '2020-07-25 00:16:29', '2020-07-25 00:16:29'),
(27, 6, 'Profil Prodi D4', 'profil-prodi-d4', NULL, '<p><img title=\"citations.jpg\" src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4QBiRXhpZgAATU0AKgAAAAgABQESAAMAAAABAAEAAAEaAAUAAAABAAAASgEbAAUAAAABAAAAUgEoAAMAAAABAAEAAAITAAMAAAABAAEAAAAAAAAAAAABAAAAAQAAAAEAAAAB/9sAQwAFAwQEBAMFBAQEBQUFBgcMCAcHBwcPCwsJDBEPEhIRDxERExYcFxMUGhURERghGBodHR8fHxMXIiQiHiQcHh8e/9sAQwEFBQUHBgcOCAgOHhQRFB4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4e/8AAEQgAgABVAwEiAAIRAQMRAf/EAB0AAAEFAQEBAQAAAAAAAAAAAAADBAUGBwIBCAn/xAA4EAACAQMCAwQHBgcBAQAAAAABAgMABBEFEgYhMRMiQVEHCDJhcYGRFBVCUqGxI3KiwdHh8LLx/8QAGwEAAQUBAQAAAAAAAAAAAAAAAAECAwQGBQf/xAArEQACAgIBAwIEBwEAAAAAAAAAAQIDBBExBRIhBkFRYYGhEyIycbHB0eH/2gAMAwEAAhEDEQA/AKPRRRWXPYgooooAKKKKACiiigAooooAKKKKACiiigAooplrN+mnWD3DDc3sov5mPQUsYuT0iO2yNUHOb0kPGZVUszBQOpJpJLu1dgqXMLE9AHBzWc6jcTalIxvriZuWcLnag9wpt93TzsttZG4dz7OzDZx4+ea6KwEluUjKWeqNz1XDa+b8mq0Vm3DPEOoaJqn3XrhlMDMAGlyWiPnz8PdWkjnVK6l1PTNDgZ9ebX3R8Ncr4BRRRUReCiiigAooooAKrHF0Lajq2m6Srsgmccx4EnGas9K6Pos15xRpeoCPdDEzxn+cruX9jU1ElCXd+5yesxc8VxXu1/JdeD/RVoSWolvB2rfgL9T8fP8A19bjpPBej6VA4tIo4y3tNtGTUBYalr9vcpBBFJMDklJrPbGQCAQH35B5jGR5noCanOIdW1q3tEls7ONVMe9yiCR+mcKCyjPLxNE5Tk0m+TMQhCKbUeDFvWW4XhSzt9Tt4VWVJCjsq+0CMj9j9aieDLqS94Zsp5ecnZ7WPntJGf0rVONLK+1LhDVPtUczSLCxjWaEI24DI9kkHmPCqENCm4bVdInCiSEZbb0y3M4+tOlNOrtfKZ0+ixaypNcOP32FFFFVzVBRRRQAUUUUAFWPg29WN1hd1GydZFBbGc8j8fD6mq5Xqy9gwm3Bez724nkMUqK+VUranE3y9uGbSw9tEJJSANoIGB44z40np8l40kO6wMKGIb90ytg+I5fp/aqTo2rG8hNrcPNBKF5bXKHPiKnrW2WOJXmdtic+7dMd3x/xT0kl55MhtvgkeM7yNLMl3RI0K5duYAz1NY7xDffeOsXF0G3KzAKfMAYH7VZuN9Ulu7B44ATCsgWRx08cD9KpNMe/c73SaY9rs+gUUUUh2QooooAKb3V7a2o/jzoh8s5P061FcT30sRS1gkKFhucr1x4DPh41X0UDoPnV6jD/ABI90n4M11L1AsWx1VR217vgsVzxHbIpMEEspHie6P8AvlUbY6ld6zrNlZXaolnLcxrLEg5spYZBPwqMmYGVIuQzz+IFP+HNy69YupCubiPBI5A7hir9eLVW9pGZyutZeSnGUtJ+y8f9Psj0l+iptYRta0ELFe7AZIQMCTA6r5Nj6/HrQeEPRpxVrNw0Esk0Furd+SXKqB5c+Z+A51uvFNvrOq8D/d2jX76ZcXMSxy3kZ78K9G2H83UZ8PceYw/1ddO4xseO9au01V7q20+5+7r6CWQv9q2lhlSScFcbh57iM8zUk8aMpbKFeVOEdEj6xnD2lcE+hJrWzy17c38Ci4Yd9nXc2ceAAVhj3++vmqy4pBIW8tSvm8ZyPof819D+vFqm6Lh3SU9g9rcNz6nuqPoM/WvlsxgLnHRsGksx65pJrgs4fU8jGbdcuefgXmz1Gyu+UFwjN+U8m+hp1WelRgHxFPbHWdQs2CmT7REOqScz8j1qhZgNeYM0uH6nhL8t8dfNf4XWik7K5We2juACBIoYA+FFUHFrwaqEozipL3KLqF/9v1CWfaUyQFB/KOleBuQOaZK5WNiV5gcjS0cgaEEVooxUUkjyS22Vs3OT8vyKMY58j8h5+eacWztFdJKhwykMp8sU1ix2px+MY+Y/407jHeBpRh+hmta9Ho3oy1DXnClbeyeaMHozFe6PmxArCvU11x34j13TbuQvJeQLeAsclnViHPx/iLU/6TtaJ9V3TH7TvalBYoefMnYrsPqhrN/VSlKeluBQPas5wT5DaP7gVINXB3659+Lj0i2VkH3La2KkgeDMzZ/YVhDozAEE4xzGetaT6xupDVvS9r0sZzHDMtunu2KFP9W6s2lyIzjPPkKY+R0eDkMGGQQQaSkcAmuZCIoto60iXG/nzAHSgC78H3Pb6Rt8YnKfLqP3oqv8N8Q2+mRTw3EbncwZdo93/wAorl2Y8nJ6ibjB67XXjwhKXlIg5ZO4xPlXGkzdrbjn1ANcXDr2LNjHd5004bfdCuehyP1NdQw5MqxWTHiOYp/FIG2sOhwRUa+UlUnpTm3YruTy7w+BpANr4v4k+2+gbgfSRKGZZbjtV8QYWKJ/S9derZqEOl+kOfUblgsNtplzK5PkFBrIk1G5d7OwaQmCLtZI18i4XP8A5FTGl6sdMTUF3FTd2T24IPPvFT/apY8CaGOtahLqerXmoznMt3O875/MzFj+9RNw43nnyTl869abClxkn8PxppcMViw3teJ86iFG80jNJ7hXCvl28uQNcAjBbNNbCbfJPk9HxSgOyFJJwD8qK4wcnbnHuopwEXdXMMIZJJ51yOjrnNK6EwW3j2t+HNcamyi1cGGRl6d4gge+p3UDYJw7oE9tDtkktirsvQ7cBvidxP8A2KZKfa0tck1VDshOe/0rf31/Z0jiRdknXqDXQZoirEkhT3v5T1pFSskSkc+WQRSizFO7MuR0DUpCO1YLNby59iTGfceVK6lJmUAHoMfWo8vi3aMZ3JzHvHgf0/SvXm7dg0bZD81I8j/ql34AWVgTnwXkPj50zvpdx2g86VdwqgL7IpvtXcXJz5UgCU7COI+QGag9HuQ6yAyKshOcGrTo2lLrd/NZPK0UYhZnZcbh4DA+JFVTTMoezVVdOmcYJ99EZpycfdEkqZxhGxrw96+nJIpK/PEyH+dSv086KVjgVlyuR54yP2NFO0Rn/9k=\" alt=\"\" width=\"85\" height=\"128\" />Profil Prodi DProfil Prodi DProfil Prodi DProfil Prodi D</p>', '2020-07-25 00:16:50', '2021-07-05 20:55:34');

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE `site` (
  `redirect` bigint(20) NOT NULL DEFAULT '0',
  `primary_locale` varchar(14) NOT NULL,
  `min_password_length` tinyint(4) NOT NULL DEFAULT '6',
  `installed_locales` varchar(255) NOT NULL DEFAULT 'en_US',
  `supported_locales` varchar(255) DEFAULT NULL,
  `original_style_file_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `setting_name` varchar(255) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `surat_aktif`
--

CREATE TABLE `surat_aktif` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prodi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_berita`
--

CREATE TABLE `tabel_berita` (
  `id` int(6) NOT NULL,
  `isi` varchar(5) NOT NULL,
  `gambar` varchar(5) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tas`
--

CREATE TABLE `tas` (
  `id` int(10) UNSIGNED NOT NULL,
  `angkatan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_ajaran` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `semester` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `awal_semester` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `akhir_semester` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tas`
--

INSERT INTO `tas` (`id`, `angkatan`, `tahun_ajaran`, `semester`, `awal_semester`, `akhir_semester`, `status`, `created_at`, `updated_at`) VALUES
(1, '2020', '2020/2021', 'Ganjil', '2020-10-05', '2021-01-29', 'Tidak Aktif', '2021-03-14 16:58:22', '2021-03-14 16:58:22'),
(2, '2020', '2020/2021', 'Genap', '2021-03-08', '2021-07-30', 'Aktif', '2021-03-14 16:58:42', '2021-03-14 16:58:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jurusan` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prodi` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT '2021-02-28 16:00:00',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `nim`, `jurusan`, `prodi`, `jabatan`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(13, 'hamid', 'hamid@gmail.com', NULL, NULL, NULL, 'admin', '2019-03-04 10:01:44', '$2y$10$oUXrDdznAC3v6.n3ZOsut.v39Cdf27uj9voI4JIcCH5dEaei3j8H2', '9vdXfUH2cdLvRBA0lmCOxQyJp2Y4ZUsNXSukhNX2JGv9efif3lUFuu45lhO6', '2020-07-12 16:33:06', '2020-07-12 16:33:06');

-- --------------------------------------------------------

--
-- Table structure for table `versions`
--

CREATE TABLE `versions` (
  `major` int(11) NOT NULL DEFAULT '0',
  `minor` int(11) NOT NULL DEFAULT '0',
  `revision` int(11) NOT NULL DEFAULT '0',
  `build` int(11) NOT NULL DEFAULT '0',
  `date_installed` datetime NOT NULL,
  `current` tinyint(4) NOT NULL DEFAULT '0',
  `product_type` varchar(30) DEFAULT NULL,
  `product` varchar(30) DEFAULT NULL,
  `product_class_name` varchar(80) DEFAULT NULL,
  `lazy_load` tinyint(4) NOT NULL DEFAULT '0',
  `sitewide` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absens`
--
ALTER TABLE `absens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alumnis`
--
ALTER TABLE `alumnis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_slug_unique` (`slug`);

--
-- Indexes for table `article_tag`
--
ALTER TABLE `article_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_sources`
--
ALTER TABLE `auth_sources`
  ADD PRIMARY KEY (`auth_id`);

--
-- Indexes for table `borangs`
--
ALTER TABLE `borangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_post_id_foreign` (`post_id`),
  ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Indexes for table `dosens`
--
ALTER TABLE `dosens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatans`
--
ALTER TABLE `jabatans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwals`
--
ALTER TABLE `jadwals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jams`
--
ALTER TABLE `jams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurusans`
--
ALTER TABLE `jurusans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mahasiswas`
--
ALTER TABLE `mahasiswas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mails`
--
ALTER TABLE `mails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mks`
--
ALTER TABLE `mks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD UNIQUE KEY `site_settings_pkey` (`setting_name`,`locale`);

--
-- Indexes for table `surat_aktif`
--
ALTER TABLE `surat_aktif`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_berita`
--
ALTER TABLE `tabel_berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_slug_unique` (`slug`);

--
-- Indexes for table `tas`
--
ALTER TABLE `tas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `versions`
--
ALTER TABLE `versions`
  ADD UNIQUE KEY `versions_pkey` (`product_type`,`product`,`major`,`minor`,`revision`,`build`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absens`
--
ALTER TABLE `absens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `alumnis`
--
ALTER TABLE `alumnis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `article_tag`
--
ALTER TABLE `article_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_sources`
--
ALTER TABLE `auth_sources`
  MODIFY `auth_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `borangs`
--
ALTER TABLE `borangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dosens`
--
ALTER TABLE `dosens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `jabatans`
--
ALTER TABLE `jabatans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jadwals`
--
ALTER TABLE `jadwals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jams`
--
ALTER TABLE `jams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jurusans`
--
ALTER TABLE `jurusans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mahasiswas`
--
ALTER TABLE `mahasiswas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `mails`
--
ALTER TABLE `mails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `mks`
--
ALTER TABLE `mks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `surat_aktif`
--
ALTER TABLE `surat_aktif`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_berita`
--
ALTER TABLE `tabel_berita`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tas`
--
ALTER TABLE `tas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
