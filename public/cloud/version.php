<?php 
$OC_Version = array(10,3,0,4);
$OC_VersionString = '10.3.0';
$OC_Channel = 'stable';
$OC_Edition = 'community';
$OC_VersionCanBeUpgradedFrom = [[8,2,11],[9,0,9],[9,1]];
$OC_Build = '2019-10-15T10:57:13+00:00 b02329a827d19043bbd9747fe14e02d3cd8eb427';
$vendor = 'owncloud';
