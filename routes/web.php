<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     // return view('welcome');
//     return view('jte');
// });
Route::get('/', 'GuiController@default')->name('gui.default');
Route::get('/jte', 'GuiController@jte')->name('gui.jte');
Route::get('/optimize', 'GuiController@optimize')->name('optimize');

Route::post('/loginapps', 'Auth\LoginController@login')->name('loginapps');

// Route::get('/unisharp', function () {return view('unisharp.demo');});
Route::get('/loker', 'GuiController@loker')->name('gui.loker');
Route::get('/pengajar/{post}', 'GuiController@pengajar')->name('gui.pengajar');
Route::get('/lokasi_kampus', 'GuiController@show_lokasi')->name('gui.lokasi');
Route::get('/informasi_akademik', 'GuiController@informasi_akademik')->name('gui.informasi_akademik');
Route::get('/kalender_akademik', 'GuiController@kalender_akademik')->name('gui.kalender_akademik');
Route::get('/profil_jte', 'GuiController@profil_jte')->name('gui.profil_jte');
Route::get('/ealumni', 'GuiController@ealumni')->name('gui.ealumni');
Route::get('/ealumni/{alumni}', 'GuiController@showalumni')->name('gui.showalumni');
Route::get('/cariealumni', 'GuiController@search')->name('gui.cari');
Route::get('/info/{post}', 'GuiController@show')->name('gui.show');
Route::get('/prodid3/{post}', 'GuiController@prodid3')->name('gui.prodid3');
Route::get('/prodid4/{post}', 'GuiController@prodid4')->name('gui.prodid4');
Route::get('/login_gui', 'GuiController@login_gui')->name('gui.login_gui');
Route::get('/isiabsen', 'AbsenController@isicreate')->name('absen.isicreate');
Route::post('/isiabsen', 'AbsenController@isistore')->name('absen.isistore');
Route::get('/isiabsen/gagal', 'AbsenController@gagal')->name('absen.gagal');
Route::get('/isiabsen/done', 'AbsenController@done')->name('absen.done');
Route::get('/getmk/{id}/kelas/{kelas}','AbsenController@getmk')->name('getmk');
Route::get('/getmhs/{id}','AbsenController@getmhs')->name('getmhs');

Auth::routes(['verify' => true]);
Route::middleware('verified')->group(function() {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
    Route::get('/post', 'PostController@index')->name('post.index');
    Route::get('/post/create', 'PostController@create')->name('post.create');
    Route::post('/post/create', 'PostController@store')->name('post.store');
    Route::get('/post/{post}', 'PostController@show')->name('post.show');
    Route::get('/post/{post}/edit', 'PostController@edit')->name('post.edit');
    Route::patch('/post/{post}/edit', 'PostController@update')->name('post.update');
    Route::delete('/post/{post}/delete', 'PostController@destroy')->name('post.destroy');
    Route::post('/post/{post}/comment', 'PostCommentController@store')->name('post.comment.store');
    Route::patch('/absen/{absen}/edit', 'AbsenController@update')->name('absen.update');

    // Ruoute mail
    Route::get('/mail', 'MailController@index')->name('mail.index');
    // Route::get('/mail/create', 'MailController@create')->name('mail.create');
    Route::get('/mail/surat_aktif', 'MailController@surat_aktif')->name('surat_aktif.create');
    Route::get('/mail/pengantar_perusahaan', 'MailController@pengantar_perusahaan')->name('pengantar_perusahaan.create');
    // Route::get('/mail/create', 'MailController@create')->name('mail.create');
    Route::post('/mail/create', 'MailController@store')->name('mail.store');
    Route::get('/mail/{mail}', 'MailController@show')->name('mail.show');
    Route::get('/mail/{mail}/edit', 'MailController@edit')->name('mail.edit');
    Route::patch('/mail/{mail}/edit', 'MailController@update')->name('mail.update');
    Route::delete('/mail/{mail}/delete', 'MailController@destroy')->name('mail.destroy');
    Route::get('/mail/{mail}/cetak', 'MailController@cetak')->name('mail.cetak');

    // Ruoute mail
    Route::get('/alumni', 'AlumniController@index')->name('alumni.index');
    Route::get('/alumni/create', 'AlumniController@create')->name('alumni.create');
    Route::post('/alumni/create', 'AlumniController@store')->name('alumni.store');
    Route::get('/alumni/{alumni}', 'AlumniController@show')->name('alumni.show');
    Route::get('/alumni/{alumni}/edit', 'AlumniController@edit')->name('alumni.edit');
    Route::patch('/alumni/{alumni}/edit', 'AlumniController@update')->name('alumni.update');
    Route::delete('/alumni/{alumni}/delete', 'AlumniController@destroy')->name('alumni.destroy');
    Route::get('/carialumni', 'AlumniController@search')->name('alumni.cari');

    // Ruoute borang
    Route::get('/borang', 'BorangController@index')->name('borang.index');
    Route::get('/cariborang', 'BorangController@search')->name('borang.cari');


    Route::middleware('admin')->group(function() {
    // Ruoute akun user
    Route::get('/user_panel', 'UserPanelController@index')->name('user_panel.index');
    Route::get('/user_panel/create', 'UserPanelController@create')->name('user_panel.create');
    Route::post('/user_panel/create', 'UserPanelController@store')->name('user_panel.store');
    Route::get('/user_panel/{user}/edit', 'UserPanelController@edit')->name('user_panel.edit');
    Route::patch('/user_panel/{user}/edit', 'UserPanelController@update')->name('user_panel.update');
    Route::delete('/user_panel/{user}/delete', 'UserPanelController@destroy')->name('user_panel.destroy');

    Route::delete('/borang/{borang}/delete', 'BorangController@destroy')->name('borang.destroy');
    Route::get('/borang/create', 'BorangController@create')->name('borang.create');
    Route::post('/borang/create', 'BorangController@store')->name('borang.store');
    Route::get('/borang/{borang}/edit', 'BorangController@edit')->name('borang.edit');
    Route::patch('/borang/{borang}/edit', 'BorangController@update')->name('borang.update');

    Route::get('/user_panel_export', 'HomeController@user_panel_export')->name('user_panel.export');
    Route::get('/user_panel_ieview', 'HomeController@user_panel_ieview')->name('user_panel.ieview');
    Route::post('/user_panel_import', 'HomeController@user_panel_import')->name('user_panel.import');

    Route::get('/mk_export', 'MkController@mk_export')->name('mk.export');
    Route::get('/mk_ieview', 'MkController@mk_ieview')->name('mk.ieview');
    Route::post('/mk_import', 'MkController@mk_import')->name('mk.import');

    Route::get('/mahasiswa_export', 'MahasiswaController@mahasiswa_export')->name('mahasiswa.export');
    Route::get('/mahasiswa_ieview', 'MahasiswaController@mahasiswa_ieview')->name('mahasiswa.ieview');
    Route::post('/mahasiswa_import', 'MahasiswaController@mahasiswa_import')->name('mahasiswa.import');

    Route::get('/dosen_export', 'DosenController@dosen_export')->name('dosen.export');
    Route::get('/dosen_ieview', 'DosenController@dosen_ieview')->name('dosen.ieview');
    Route::post('/dosen_import', 'DosenController@dosen_import')->name('dosen.import');

    Route::get('/mk', 'MkController@index')->name('mk.index');
    Route::get('/mk/create', 'MkController@create')->name('mk.create');
    Route::post('/mk/create', 'MkController@store')->name('mk.store');
    Route::get('/mk/{mk}/edit', 'MkController@edit')->name('mk.edit');
    Route::patch('/mk/{mk}/edit', 'MkController@update')->name('mk.update');
    Route::delete('/mk/{mk}/delete', 'MkController@destroy')->name('mk.destroy');

    Route::get('/absen', 'AbsenController@index')->name('absen.index');
    Route::post('/absen', 'AbsenController@index')->name('absen.post');
    Route::get('/absen/buka', 'AbsenController@search')->name('absen.search');
    Route::get('/absen/create', 'AbsenController@create')->name('absen.create');
    Route::post('/absen/create', 'AbsenController@store')->name('absen.store');
    Route::get('/absen/{absen}/edit', 'AbsenController@edit')->name('absen.edit');
    Route::patch('/absen/{absen}/edit', 'AbsenController@update')->name('absen.update');
    Route::delete('/absen/{absen}/delete', 'AbsenController@destroy')->name('absen.destroy');

    Route::get('/jadwal', 'JadwalController@index')->name('jadwal.index');
    Route::get('/jadwal/create', 'JadwalController@create')->name('jadwal.create');
    Route::post('/jadwal/create', 'JadwalController@store')->name('jadwal.store');
    Route::get('/jadwal/{jadwal}/edit', 'JadwalController@edit')->name('jadwal.edit');
    Route::patch('/jadwal/{jadwal}/edit', 'JadwalController@update')->name('jadwal.update');
    Route::delete('/jadwal/{jadwal}/delete', 'JadwalController@destroy')->name('jadwal.destroy');

    Route::get('/jam', 'JamController@index')->name('jam.index');
    Route::get('/jam/create', 'JamController@create')->name('jam.create');
    Route::post('/jam/create', 'JamController@store')->name('jam.store');
    Route::get('/jam/{jam}/edit', 'JamController@edit')->name('jam.edit');
    Route::patch('/jam/{jam}/edit', 'JamController@update')->name('jam.update');
    Route::delete('/jam/{jam}/delete', 'JamController@destroy')->name('jam.destroy');

    Route::get('/ta', 'TaController@index')->name('ta.index');
    Route::get('/ta/create', 'TaController@create')->name('ta.create');
    Route::post('/ta/create', 'TaController@store')->name('ta.store');
    Route::get('/ta/{ta}/edit', 'TaController@edit')->name('ta.edit');
    Route::patch('/ta/{ta}/edit', 'TaController@update')->name('ta.update');
    Route::delete('/ta/{ta}/delete', 'TaController@destroy')->name('ta.destroy');

    Route::get('/dosen', 'DosenController@index')->name('dosen.index');
    Route::get('/dosen/create', 'DosenController@create')->name('dosen.create');
    Route::post('/dosen/create', 'DosenController@store')->name('dosen.store');
    Route::get('/dosen/{dosen}/edit', 'DosenController@edit')->name('dosen.edit');
    Route::patch('/dosen/{dosen}/edit', 'DosenController@update')->name('dosen.update');
    Route::delete('/dosen/{dosen}/delete', 'DosenController@destroy')->name('dosen.destroy');

    Route::get('/mahasiswa', 'MahasiswaController@index')->name('mahasiswa.index');
    Route::get('/mahasiswa/create', 'MahasiswaController@create')->name('mahasiswa.create');
    Route::post('/mahasiswa/create', 'MahasiswaController@store')->name('mahasiswa.store');
    Route::get('/mahasiswa/{mahasiswa}/edit', 'MahasiswaController@edit')->name('mahasiswa.edit');
    Route::patch('/mahasiswa/{mahasiswa}/edit', 'MahasiswaController@update')->name('mahasiswa.update');
    Route::delete('/mahasiswa/{mahasiswa}/delete', 'MahasiswaController@destroy')->name('mahasiswa.destroy');

    Route::get('/menu', 'MenusController@index')->name('menu.index');
    Route::get('/menu/create', 'MenusController@create')->name('menu.create');
    Route::post('/menu/create', 'MenusController@store')->name('menu.store');
    Route::get('/menu/{menu}/edit', 'MenusController@edit')->name('menu.edit');
    Route::patch('/menu/{menu}/edit', 'MenusController@update')->name('menu.update');
    Route::delete('/menu/{menu}/delete', 'MenusController@destroy')->name('menu.destroy');
    });
});
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
