<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $fillable = ['nim', 'nama', 'jurusan', 'prodi', 'angkatan', 'status']; /* yang bsa di isi */
}
