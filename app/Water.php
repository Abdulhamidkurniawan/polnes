<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Water extends Model
{
    protected $fillable = ['id_node','id_message','battery','valve','level','consumption']; /* yang bsa di isi */
}
