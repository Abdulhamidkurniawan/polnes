<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absen extends Model
{
    protected $fillable = ['nim', 'nama', 'nama_mk', 'dosen', 'jurusan', 'prodi', 'semester',
     'kelas', 'jumlah_jam', 'jam_pertemuan', 'tanggal', 'materi', 'catatan', 'persetujuan','tahun_ajaran','semester_ta']; /* yang bsa di isi */
}
