<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $fillable = ['kode_mk', 'nama_mk', 'dosen_mk', 'jam_awal', 'jam_akhir', 'jurusan', 'prodi', 'kelas']; /* yang bsa di isi */
}
