<?php

namespace App\Http\Controllers;

use App\Borang;
use Illuminate\Http\Request;

class BorangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borangs = Borang::all();

        return view('borang.index', compact('borangs'));
    }

    public function search(Request $request)
    {
        $cari= $request->get('jenjang');
        //  dd($cari);
        $borangs = Borang::where('jenjang', 'LIKE', '%'.$cari.'%')->paginate(1000);
        return view('borang.index', compact('borangs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('borang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'jenjang' => 'required',
            'jenis' => 'required',
            'butir' => 'required',
            'judul' => 'required',
            'link' => 'required' 
        ]);
        Borang::create([ 
            'jenjang' => request('jenjang'),
            'jenis' => request('jenis'),
            'butir' => request('butir'),
            'judul' => request('judul'),
            'link' => request('link'),
        ]); 
        return redirect()->route('borang.index')->withSuccess('Borang berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Borang  $borang
     * @return \Illuminate\Http\Response
     */
    public function show(Borang $borang)
    {
        return view('borang.show', compact('borang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Borang  $borang
     * @return \Illuminate\Http\Response
     */
    public function edit(Borang $borang)
    {
        return view('borang.edit', compact('borang'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Borang  $borang
     * @return \Illuminate\Http\Response
     */
    public function update(Borang $borang)
    {
        $borang->update([ 
            'jenjang' => request('jenjang'),
            'jenis' => request('jenis'),
            'butir' => request('butir'),
            'judul' => request('judul'),
            'link' => request('link'),
        ]); 
        return redirect()->route('borang.index')->withInfo('data borang berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Borang  $borang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Borang $borang)
    {
        $borang->delete();

        return redirect()->route('borang.index')->withDanger('data borang berhasil dihapus');    }
}
