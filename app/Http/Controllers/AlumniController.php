<?php

namespace App\Http\Controllers;

use App\Alumni;
use Illuminate\Http\Request;

class AlumniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alumnis = Alumni::latest()->Paginate(10);

        return view('alumni.index', compact('alumnis')); /* kirim var */
    }

    public function search(Request $request)
    {
        $cari= $request->get('nim');
        //  dd($cari);
        $alumnis = Alumni::where('nim', 'LIKE', '%'.$cari.'%')->paginate(1000);
        return view('alumni.index', compact('alumnis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('alumni.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nim' => 'required|min:10'
        ]);
        Alumni::create([ 
            'nim' => request('nim'),
            'nama' => request('nama'),
            'jenis_kelamin' => request('jenis_kelamin'),
            'hp' => request('hp'),
            'jenjang' => request('jenjang'),
            'tahun_angkatan' => request('tahun_angkatan'),
            'judul' => request('judul'),
            'pembimbing_1' => request('pembimbing_1'),
            'pembimbing_2' => request('pembimbing_2'),
        ]); 
        return redirect()->route('alumni.index')->withSuccess('Borang berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Alumni  $alumni
     * @return \Illuminate\Http\Response
     */
    public function show(Alumni $alumni)
    {
        return view('alumni.show', compact('alumni'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Alumni  $alumni
     * @return \Illuminate\Http\Response
     */
    public function edit(Alumni $alumni)
    {
        return view('alumni.edit', compact('alumni'));          /* $Alumni dikirim view edit,dll*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alumni  $alumni
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Alumni $alumni)
    {
        $alumni->update([ 
            'nim' => request('nim'),
            'nama' => request('nama'),
            'jenis_kelamin' => request('jenis_kelamin'),
            'hp' => request('hp'),
            'jenjang' => request('jenjang'),
            'tahun_angkatan' => request('tahun_angkatan'),
            'judul' => request('judul'),
            'pembimbing_1' => request('pembimbing_1'),
            'pembimbing_2' => request('pembimbing_2'),
        ]); 
        return redirect()->route('alumni.index')->withInfo('data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Alumni  $alumni
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alumni $alumni)
    {
        $alumni->delete();

        return redirect()->route('alumni.index')->withDanger('data berhasil dihapus');
    }
}
