<?php

namespace App\Http\Controllers;

use App\Menu;
use DB;
use Illuminate\Http\Request;

class MenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::orderBy('urutan', 'ASC')->orderBy('tipe', 'ASC')->get();
        $sub_menus = Menu::where('status', 'Aktif')->where('tipe', '!=', 'Deep Menu')->orderBy('urutan', 'ASC')->get();
        return view('menu.index', compact('menus','sub_menus')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Menu::create([
            'urutan' => request('urutan'),
            'tipe' => request('tipe'),
            'sub_menu' => request('sub_menu'),
            'judul' => request('judul'),
            'link' => request('link'),
            'status' => request('status'),
        ]);
        return redirect()->route('menu.index')->withSuccess('Data berhasil ditambahkan');    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        $status_menus = DB::select("SELECT * FROM menus WHERE id IN (
            SELECT MAX(id)
            FROM menus
            GROUP BY status
        )ORDER BY status ASC");
        // dd($status_menus);
        return view('menu.edit', compact('menu','status_menus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        $update = $request->All();
        $menu->update($update);
        return redirect()->route('menu.index')->withInfo('data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();
        return redirect()->route('menu.index')->withDanger('data berhasil dihapus');
    }
}
