<?php

namespace App\Http\Controllers;

use App\Power;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DB;

class ApiPowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $powers = power::latest()->get();
        // dd($powers);

        $powers->map(function($power) {
            $power->time=$power->created_at->shortRelativeDiffForHumans();
            return $power;
        });
        $data = ['status'=>"success",'data'=>$powers];
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $power=new Power();
        $power->id_node=$request->id_node;
        $power->id_message=$request->id_message;
        $power->fasa_r=$request->fasa_r*90;
        $power->fasa_s=$request->fasa_s*90;
        $power->fasa_t=$request->fasa_t*90;
        $power->tiga_fasa=$request->tiga_fasa*4;
        $power->save();

        $data = ['status'=>"success"];
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $power = Power::find($id);
        return $power;    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $power = Power::find($request->id);
        $power->id_node=$request->id_node;
        $power->id_message=$request->id_message;
        $power->fasa_r=$request->fasa_r*90;
        $power->fasa_s=$request->fasa_s*90;
        $power->fasa_t=$request->fasa_t*90;
        $power->tiga_fasa=$request->tiga_fasa*4;
        $power->save();
        $data = ['status'=>"success"];
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $power = Power::find($id);
        $power->delete();
        $data = ['status'=>"success"];
        return $data;
    }

    public function node()
    {
        $powers = DB::select("SELECT * FROM powers WHERE id IN (
            SELECT MAX(id)
            FROM powers
            GROUP BY id_node
        )ORDER BY id_node ASC");
        // $powers= json_decode( json_encode($powers), true);
        // dd($powers);
        // foreach ($powers as $power){
        // $power['time'] = Carbon::parse($power['created_at'])->shortRelativeDiffForHumans();
        $collection = collect($powers);
        // dd($collection);
        $collection->map(function($power) {
            $power->time=Carbon::parse($power->created_at)->shortRelativeDiffForHumans();
            return $power;
        });
        $data = ['status'=>"success",'data'=>$powers];
        return $data;
    }
}
