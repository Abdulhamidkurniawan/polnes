<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Alumni;
use App\Menu;
use Artisan;

class GuiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('category_id', 3)->latest()->Paginate(5);

        return view('post.index', compact('gui')); /* kirim var */
    }

    public function default()
    {
        $posts = Post::where('category_id', 2)->latest()->Paginate(12);
        $prodid3s = Post::where('category_id', 5)->latest()->take(1)->get();
        $prodid4s = Post::where('category_id', 6)->latest()->take(1)->get();
        $pengajars = Post::where('category_id', 8)->latest()->take(1)->get();
        $categories = Category::all();
        $guis = Post::latest()->get();
        $menus = Menu::where('sub_menu', 'Main Menu')->where('status', 'Aktif')->orderBy('urutan', 'ASC')->get();
        $menus->map(function ($menu) {
            // Menghitung jumlah menu dengan sub_menu yang sama, tapi sub_menu bukan 'Main Menu'
            $menu->count = Menu::where('sub_menu', $menu->judul)
                               ->where('status', 'Aktif')
                               ->count();

            return $menu;
        });
        $sub_menu_statuses = Post::where('category_id', 8)->latest()->take(1)->get();
        $sub_menus = Menu::where('tipe', 'Sub Menu')->where('status', 'Aktif')->orderBy('urutan', 'ASC')->get();
        $sub_menus->map(function ($sub_menus) {
            // Menghitung jumlah menu dengan sub_menu yang sama, tapi sub_menu bukan 'Main Menu'
            $sub_menus->count = Menu::where('sub_menu', $sub_menus->judul)
                               ->where('status', 'Aktif')
                               ->count();

            return $sub_menus;
        });
        $deep_menus = Menu::where('tipe', 'Deep Menu')->where('status', 'Aktif')->orderBy('urutan', 'ASC')->get();
        // dd($menus,$sub_menus,$deep_menus);
        return view('rumah', compact('posts', 'categories', 'guis', 'prodid3s', 'prodid4s', 'pengajars','menus','sub_menus','deep_menus'));
    }

    public function jte()
    {
        $posts = Post::where('category_id', 2)->latest()->Paginate(12);
        $prodid3s = Post::where('category_id', 5)->latest()->take(1)->get();
        $prodid4s = Post::where('category_id', 6)->latest()->take(1)->get();
        $pengajars = Post::where('category_id', 8)->latest()->take(1)->get();
        $categories = Category::all();
        $guis = Post::latest()->get();
        return view('jte', compact('posts', 'categories', 'guis', 'prodid3s', 'prodid4s', 'pengajars'));
    }

    public function show(Post $post){
        $menus = Menu::where('sub_menu', 'Main Menu')->where('status', 'Aktif')->orderBy('urutan', 'ASC')->get();
        $menus->map(function ($menu) {
            // Menghitung jumlah menu dengan sub_menu yang sama, tapi sub_menu bukan 'Main Menu'
            $menu->count = Menu::where('sub_menu', $menu->judul)
                               ->where('status', 'Aktif')
                               ->count();

            return $menu;
        });
        $sub_menu_statuses = Post::where('category_id', 8)->latest()->take(1)->get();
        $sub_menus = Menu::where('tipe', 'Sub Menu')->where('status', 'Aktif')->orderBy('urutan', 'ASC')->get();
        $sub_menus->map(function ($sub_menus) {
            // Menghitung jumlah menu dengan sub_menu yang sama, tapi sub_menu bukan 'Main Menu'
            $sub_menus->count = Menu::where('sub_menu', $sub_menus->judul)
                               ->where('status', 'Aktif')
                               ->count();

            return $sub_menus;
        });
        $deep_menus = Menu::where('tipe', 'Deep Menu')->where('status', 'Aktif')->orderBy('urutan', 'ASC')->get();
        // dd($menus,$sub_menus);
        return view('gui.prodi', compact('post','menus','sub_menus','deep_menus'));
    }

    public function loker(Post $post){
        $posts = Post::where('category_id', 7)->latest()
        ->get();
        return view('gui.loker', compact('posts'));
    }

    public function pengajar(Post $post){
        return view('gui.prodi', compact('post'));
    }

    public function informasi_akademik()
    {
        $posts = Post::where('category_id', 2)->latest()->Paginate(5);
        $categories = Category::all();
        $guis = Post::latest()->get();
        return view('gui.informasi_akademik', compact('posts', 'categories', 'guis')); /* kirim var */
    }

    public function kalender_akademik()
    {
        $posts = Post::where('category_id', 4)->latest()->take(1)->get();
        $categories = Category::all();
        return view('gui.kalender_akademik', compact('posts', 'categories')); /* kirim var */
    }

    public function profil_jte()
    {
        $posts = Post::where('category_id', 1)->latest()->take(1)->get();
        $categories = Category::all();
        return view('gui.profil_jte', compact('posts', 'categories')); /* kirim var */
    }

    public function show_lokasi()
    {
        return view('gui.lokasi_kampus');
    }

    public function ealumni()
    {
        $alumnis = Alumni::latest()->Paginate(10);
        return view('gui.ealumni', compact('alumnis'));
    }

    public function search(Request $request)
    {
        $cari= $request->get('nim');
        //  dd($cari);
        $alumnis = Alumni::where('nim', 'LIKE', '%'.$cari.'%')->paginate(1000);
        return view('gui.ealumni', compact('alumnis'));
    }

    public function showalumni(Alumni $alumni){
        return view('gui.showalumni', compact('alumni'));
    }

    public function prodi(Post $post){
        $posts = Post::where('category_id', 1)->latest()->take(1)->get();
        $categories = Category::all();
        return view('gui.show', compact('posts', 'categories'));
    }

    public function optimize()
    {
        $exitCode = Artisan::call('optimize');
        $exitCode = Artisan::call('cache:clear');
        $exitCode = Artisan::call('view:clear');
        $exitCode = Artisan::call('config:cache');
        return '<h1>Reoptimized class loader</h1>';
    }

}
