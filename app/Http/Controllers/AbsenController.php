<?php

namespace App\Http\Controllers;

use App\Absen;
use App\Dosen;
use App\Ta;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class AbsenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nim = $request->nim;
        $prodi = $request->prodi;
        $tahun_ajaran = $request->tahun_ajaran;
        $nama_mk = $request->nama_mk;
        $semester_ta = "";
        $semester = substr($request->kelas, -2, 1);
        $kelas = substr($request->kelas, 1);
        $tanggal = "";
        if ($request->tanggal == "" or $request->tanggal == null )
        {
            if ($request->nama_mk == "" or $request->nama_mk == null ){
            $tanggal = Carbon::today()->toDateString();}
        }
        else{ $tanggal = $request->tanggal; }
        if ($request->kelas != "" or $request->kelas != null )
        {
        if (substr($request->kelas, -2, 1)%2 == 0){
            $semester_ta = 'Genap';}
        else {$semester_ta = 'Ganjil';}
        }
        //  $absens = Absen::where('nim', 'LIKE', '%'.$nim.'%')
        //  ->where('prodi', 'LIKE', '%'.$prodi.'%')
        //  ->where('tahun_ajaran', 'LIKE', '%'.$tahun_ajaran.'%')
        //  ->where('semester_ta', 'LIKE', '%'.$semester_ta.'%')
        //  ->where('semester', 'LIKE', '%'.$semester.'%')
        //  ->where('kelas', 'LIKE', '%'.$kelas.'%')
        //  ->where('nama_mk', 'LIKE', '%'.$nama_mk.'%')
        //  ->whereDate('tanggal','LIKE','%'.$tanggal.'%')
        //  ->get();

        $absens = Absen::where([
            ['nim', 'LIKE', '%'.$nim.'%'],
            ['prodi', 'LIKE', '%'.$prodi.'%'],
            ['tahun_ajaran', 'LIKE', '%'.$tahun_ajaran.'%'],
            ['semester_ta', 'LIKE', '%'.$semester_ta.'%'],
            ['semester', 'LIKE', '%'.$semester.'%'],
            ['kelas', 'LIKE', '%'.$kelas.'%'],
            ['nama_mk', 'LIKE', '%'.$nama_mk.'%']
            ])
            ->whereDate('tanggal','LIKE','%'.$tanggal.'%')
            ->get();
        $tas = Ta::latest()->get();
        // $powers = Power::where('id_node','LIKE', "%$request->node%")
        // ->whereBetween('created_at', [$request->dari." 00:00:00",$request->sampai." 23:59:59"])->get();
        // dd($nim,$prodi,$tahun_ajaran,$semester_ta,$semester,$kelas,$nama_mk,$request->tanggal,$tanggal,$absens);
        return view('absen.index', compact('absens','tas'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function search(Request $request)
    {
        $cari= $request->get('nim');
        //  dd($cari);
        $absens = Absen::where('nim', 'LIKE', '%'.$cari.'%')->get();
        $tas = Ta::latest()->get();
        $tas = Ta::where('status', '=', 'Aktif')->get();
        return view('absen.index', compact('absens','tas'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Absen  $absen
     * @return \Illuminate\Http\Response
     */
    public function show(Absen $absen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Absen  $absen
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Absen $absen)
    {
        $update = $request->All();
        $absen->update($update);

        return redirect()->route('absen.index')->withInfo('data berhasil diubah');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Absen  $absen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Absen $absen)
    {
        $update = $request->All();
        $absen->update($update);

        return redirect()->route('absen.index')->withInfo('data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Absen  $absen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Absen $absen)
    {
        $absen->delete();

        return redirect()->route('absen.index');
    }

    public function isicreate()
    {
        $dosens = Dosen::orderBy('nama', 'ASC')->get();
        $tas = Ta::where('status', '=', 'Aktif')->get();

        $now = Carbon::now();

        $startDate = Carbon::createFromFormat('H:i a', '07:00 AM');
        $endDate = Carbon::createFromFormat('H:i a', '06:00 PM');

        $check = $now->between($startDate, $endDate, true);

        if($check){
            return view('absen.isi', compact('dosens','tas')); /* kirim var */
        }else{
            return view('absen.gagal'); /* kirim var */
        }
    }

    public function isistore(Request $request)
    {
        // $semester = substr(request('kelas'), -2, 1);
        // dd($semester);
        if (substr(request('kelas'), -2, 1)%2 == 0){
            $semester_ta = 'Genap';}
        else {$semester_ta = 'Ganjil';}
            // dd(count(request('jam_pertemuan')));
        Absen::create([
            'nim' => request('nim'),
            'nama' => request('nama'),
            'nama_mk' => request('nama_mk'),
            'dosen' => request('dosen'),
            'jurusan' => 'Teknik Elektro',
            'prodi' => request('prodi'),
            'tahun_ajaran' => request('tahun_ajaran'),
            'semester_ta' => $semester_ta,
            'semester' => substr(request('kelas'), -2, 1),
            'kelas' => substr(request('kelas'), -1, 1),
            'jumlah_jam' => count(request('jam_pertemuan')),
            'jam_pertemuan' => implode(", ", request('jam_pertemuan')),
            'tanggal' => request('tanggal'),
            'materi' => request('materi'),
            // 'catatan' => request('catatan'),
            // 'persetujuan' => 'Teknik Elektro',
        ]);

        // return redirect()->route('absen.done');
        return view('absen.done');

    }

    public function gagal()
    {
        return redirect()->route('absen.gagal');
    }

    public function done()
    {
        return redirect()->route('absen.done');
    }

    public function getmk($id, $kelas)
    {
        $mks = DB::table("mks")
        ->where("prodi",$id)
        ->where("semester",substr($kelas, -2, 1))
        ->pluck("nama_mk","id");
        return json_encode($mks);
    }

    public function getmhs($id)
    {
        $mahasiswas = DB::table("mahasiswas")
        ->where("nim",$id)
        ->where("status","Aktif")
        ->pluck("nama","id");
        return json_encode($mahasiswas);
    }
}
