<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Jabatan;
use Hash;
use Session;
use Auth;

class ApiUserPanelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cari(Request $request)
    {
        if($request->tipe == "apps"){
        $email = $request->email;
        $pass = Hash::make($request->password);
        // dd($email);
        $users = User::where('email', $email)->get();
        if (count($users)>0){
        // return $users;
        foreach ($users as $user){
            $data = [
                'email'     => $request->input('email'),
                'password'  => $request->input('password'),
            ];
            Auth::attempt($data);
            if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
                //Login Success
                return $user;
            }
            else { // false
                //Login Fail
                $data = ['status'=>"gagal"];
                return $data;
            }
        }
    }
    else{$data = ['status'=>"gagal"];
        return $data;}
        }
    else{$data = ['status'=>"gagal"];
        return $data;}
    }
}
