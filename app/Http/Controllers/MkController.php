<?php

namespace App\Http\Controllers;

use App\Mk;
use Illuminate\Http\Request;
use App\Exports\MksExport;
use App\Imports\MksImport;
use Maatwebsite\Excel\Facades\Excel;

class MkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mks = Mk::latest()->get();

        return view('mk.index', compact('mks')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Mk::create([
            'kode_mk' => request('kode_mk'),
            'nama_mk' => request('nama_mk'),
            'jurusan' => request('Teknik Elektro'),
            'prodi' => request('prodi'),
            'semester' => request('semester'),
            'sks' => request('sks'),
            'jumlah_jam' => request('jumlah_jam'),
        ]);
        return redirect()->route('mk.index')->withSuccess('Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mk  $mk
     * @return \Illuminate\Http\Response
     */
    public function show(Mk $mk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mk  $mk
     * @return \Illuminate\Http\Response
     */
    public function edit(Mk $mk)
    {
        return view('mk.edit', compact('mk'));          /* $Alumni dikirim view edit,dll*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mk  $mk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mk $mk)
    {
        $update = $request->All();
        $mk->update($update);
        return redirect()->route('mk.index')->withInfo('data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mk  $mk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mk $mk)
    {
        $mk->delete();

        return redirect()->route('mk.index')->withDanger('data berhasil dihapus');
    }

    public function mk_import()
    {
        Excel::import(new MksImport,request()->file('file'));

        return back();
    }

    public function mk_export()
    {
        return Excel::download(new MksExport, 'MK Elektro.xlsx');
    }

    public function mk_ieview()
    {
        return view('import');
    }
}
