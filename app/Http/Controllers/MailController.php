<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Mail;
use App\Jurusan;

class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (Auth::user()->jabatan == 'admin' or Auth::user()->jabatan == 'karyawan'){$mails = Mail::latest()->Paginate(10);}
        // else {$mails = Mail::where('nama', 'LIKE', '%'.Auth::user()->name.'%')->paginate(1000);}
        $mails = Mail::latest()->get();

        return view('mail.index', compact('mails')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $this->validate(request(), [
            'nim' => 'required',
            'email' => 'required|min:10',
            'hp' => 'required|min:10'
        ]);
        Mail::create($data);
        return redirect()->route('mail.index')->withSuccess('Surat berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mail  $mail
     * @return \Illuminate\Http\Response
     */
    public function show(Mail $mail)
    {
        return view('mail.show', compact('mail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mail  $mail
     * @return \Illuminate\Http\Response
     */
    public function edit(Mail $mail)
    {
        $jurusans = Jurusan::all();
        $data = $mail->jenis;
        // return view('mail.edit_surat_aktif', compact('mail','mails'));
        // dd($data);
        if ($data == "Surat Aktif"){
            // dd($mails);
            return view('mail.edit_surat_aktif', compact('mail','jurusans'));
        }
        else {
            // dd($mails);
            return view('mail.edit_surat_pengantar', compact('mail','jurusans'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mail  $mail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mail $mail)
    {
        $update = $request->All();
        $mail->update($update);
        return redirect()->route('mail.index')->withInfo('data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mail  $mail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mail $mail)
    {
        $mail->delete();

        return redirect()->route('mail.index')->withDanger('data berhasil dihapus');
    }

    public function surat_aktif()
    {
        return view('mail.surat_aktif');
    }

    public function pengantar_perusahaan()
    {
        return view('mail.pengantar_perusahaan');
    }

    public function cetak(Mail $mail)
    {
        function tgl_indo($tanggal){
            $bulan = array (
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
        }
    // dd($mail->jenis);
    if ($mail->jenis == 'Surat Aktif'){
        $tpl_file = public_path('tpl/ak.rtf');
        $target = public_path('tmp/ak.rtf') ;
    }
    else if ($mail->jenis == 'Tugas Akhir'){
        $tpl_file = public_path('tpl/spd3.rtf');
        $target = public_path('tmp/spd3.rtf') ;
    }
    else if ($mail->jenis == 'Skripsi'){
        $tpl_file = public_path('tpl/spd4.rtf');
        $target = public_path('tmp/spd4.rtf') ;
    }


    if (file_exists($tpl_file)) {
    // $target = public_path('tmp/ak.rtf') ;
    $f = fopen($tpl_file, "r+");
    $isi = fread($f, filesize($tpl_file));
    fclose($f);

    $isi = str_replace('nama_mhs', $mail->nama, $isi);
    $isi = str_replace('nim_mhs', $mail->nim, $isi);
    $isi = str_replace('judul_mhs', $mail->judul, $isi);
    $isi = str_replace('tujuan_mhs', $mail->tujuan, $isi);
    $isi = str_replace('alamat_mhs', $mail->alamat, $isi);
    $isi = str_replace('tanggal', tgl_indo(date('Y-m-d')), $isi);

    $f = fopen($target, "w+");
    fwrite($f, $isi);
    fclose($f);
        $nama_file=$mail->nim.'.doc';
    // header('Location:'.$target);
    header("Content-disposition: attachment; filename=$nama_file");
    header("Content-type: application/octet-stream");
    readfile($target);
    }else{echo 'File tidak ada!';}
            return redirect()->route('mail.index')->withSuccess('data berhasil dicetak');
    }
}
