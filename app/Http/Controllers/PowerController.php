<?php

namespace App\Http\Controllers;

use App\Power;
use Illuminate\Http\Request;
use DB;

class PowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $powers = Power::latest()->get();
        return view('power.index', compact('powers')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Power  $power
     * @return \Illuminate\Http\Response
     */
    public function show(Power $power)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Power  $power
     * @return \Illuminate\Http\Response
     */
    public function edit(Power $power)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Power  $power
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Power $power)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Power  $power
     * @return \Illuminate\Http\Response
     */
    public function destroy(Power $power)
    {
        $power->delete();

        return redirect()->route('power.index')->withDanger('data berhasil dihapus');
    }

    public function dashboard()
    {
        $powers = Power::latest()->get();
        $valp = $powers->count();
        $nodes = DB::select("SELECT * FROM powers WHERE id IN (
            SELECT MAX(id)
            FROM powers
            GROUP BY id_node
        )ORDER BY id_node ASC");
        // $nodes = json_decode( json_encode($powers), true);
        // dd($nodes);
        $nodep = count($nodes);
        return view('dashboard.power_dashboard', compact('valp','nodep'));
    }

    public function node()
    {
        // $powers = Power::latest()->get();
        $powers = DB::select("SELECT * FROM powers WHERE id IN (
            SELECT MAX(id)
            FROM powers
            GROUP BY id_node
        )ORDER BY id_node ASC");
        // $barangs = array($barangs);
        // $graph = array($powers);
        if (count($powers) > 0){
        foreach ($powers as $idnode){
            $graph_idnode[]=$idnode->id_node;
        }
        foreach ($powers as $prst){
            $graph_prst[]=$prst->tiga_fasa;
        }
        $graph_idnode = json_encode($graph_idnode);
        $graph_prst = json_encode($graph_prst);}

        else{
        $powers = Power::latest()->get();
        $graph_idnode=$powers;
        $graph_prst=$powers;
        }
        // dd($graph_idnode,$graph_prst);
        $powers= json_decode( json_encode($powers), true);
        return view('power.node', compact('powers','graph_idnode','graph_prst')); /* kirim var */
    }
}
