<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\User;
use App\Absen;
use App\Jabatan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('dashboard');
    }

    public function dashboard()
    {
        $users = User::latest()->get();
        $absens =  Absen::latest()->get();
        $valu = $users->count();
        $vala = $absens->count();
        return view('dashboard', compact('valu','vala'));
    }

    public function user_panel_import()
    {
        Excel::import(new UsersImport,request()->file('file'));

        return back();
    }

    public function user_panel_export()
    {
        return Excel::download(new UsersExport, 'User Elektro.xlsx');
    }

    public function user_panel_ieview()
    {
        return view('import');
    }
}
