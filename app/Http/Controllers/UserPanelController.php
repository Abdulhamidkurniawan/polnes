<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Jabatan;
use Carbon\Carbon;

class UserPanelController extends Controller
{
    /* fungsi2 yang bisa dipanggil */
    public function index()
    {
        $users = User::latest()->get();

        return view('user_panel.index', compact('users')); /* kirim var */
    }
    public function create()
    {
        // $categories = Category::All();

        return view('user_panel.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'username' => 'required|min:5'
        ]);
        User::create([
            'username' => request('username'),
            'password' => bcrypt(request('password')),
            'jabatan' => request('jabatan'),
            'email_verified_at' => Carbon::now(),
        ]);
        return redirect()->route('user_panel.index')->withSuccess('data berhasil ditambahkan');
    }

    // public function show(User $user){
    //     return view('user_panel.show', compact('post'));
    // }

    public function edit(User $user)  /* public function edit($id)   */
    {
         /* $post = Post::find($id); */
         $jabatans = Jabatan::All();

         return view('user_panel.edit', compact('user','jabatans'));          /* $post dikirim view edit,dll*/
    }

    public function Update(User $user)
    {
        /*$post = Post::find($id);*/
        $user->update([
            'username' => request('username'),
            'jabatan' => request('jabatan'),
            'password' => bcrypt(request('password'))
        ]);
        return redirect()->route('user_panel.index')->withInfo('data berhasil diubah');
    }
    public function destroy(User $user)  /* public function edit($id)   */
    {
        $user->delete();

        return redirect()->route('user_panel.index')->withDanger('data berhasil dihapus');
    }
}
