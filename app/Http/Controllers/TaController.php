<?php

namespace App\Http\Controllers;

use App\Ta;
use Illuminate\Http\Request;

class TaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tas = Ta::latest()->get();

        return view('ta.index', compact('tas')); /* kirim var */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tahun_depan = $request->angkatan+1;
        $tahun_ajaran = $request->angkatan."/".$tahun_depan;
        // dd($tahun_ajaran,$request->awal_semester, $request->status);
        Ta::create([
            'angkatan' => request('angkatan'),
            'tahun_ajaran' => $tahun_ajaran,
            'semester' => request('semester'),
            'awal_semester' => request('awal_semester'),
            'akhir_semester' => request('akhir_semester'),
            'status' => request('status'),
        ]);
        return redirect()->route('ta.index')->withSuccess('Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ta  $ta
     * @return \Illuminate\Http\Response
     */
    public function show(Ta $ta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ta  $ta
     * @return \Illuminate\Http\Response
     */
    public function edit(Ta $ta)
    {
        return view('ta.edit', compact('ta'));          /* $Alumni dikirim view edit,dll*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ta  $ta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ta $ta)
    {
        $update = $request->All();
        $ta->update($update);
        return redirect()->route('ta.index')->withInfo('data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ta  $ta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ta $ta)
    {
        $ta->delete();

        return redirect()->route('ta.index')->withDanger('data berhasil dihapus');
    }
}
