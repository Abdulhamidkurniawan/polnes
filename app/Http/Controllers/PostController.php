<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Post;
use App\Category;

class PostController extends Controller
{
    /* fungsi2 yang bisa dipanggil */
    public function index()
    {
        $posts = Post::latest()->get();

        return view('post.index', compact('posts')); /* kirim var */
    }
    public function create()
    {
        $categories = Category::All();

        return view('post.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'title' => 'required',
            'content' => 'required|min:10'
        ]);
        if (empty($request->file('gambar'))){
            Post::create([
                'title' => request('title'),
                'slug' => str_slug(request('title')),
                'content' => request('content'),
                'category_id' => request('category_id')
            ]);
        }
        else{
        $file = $request->file('gambar');
        $ext = $file->getClientOriginalExtension();
        $newName = date("d-m-Y")."-".Auth::user()->name."-".rand(100000,1001238912).".".$ext;
        $file->move('uploads/file',$newName);

        Post::create([
            'title' => request('title'),
            'slug' => str_slug(request('title')),
            'gambar' => $newName,
            'content' => request('content'),
            'category_id' => request('category_id')
        ]);
    }
        return redirect()->route('post.index')->withSuccess('data berhasil ditambahkan');
    }

    public function show(Post $post){
        return view('post.show', compact('post'));
    }

    public function edit(Post $post)  /* public function edit($id)   */
    {
         /* $post = Post::find($id); */
        $categories = Category::all();

        return view('post.edit', compact('post', 'categories'));          /* $post dikirim view edit,dll*/
    }

    public function Update(Request $request, Post $post)
    {
        /*$post = Post::find($id);*/
        // $post->update([
        //     'title' => request('title'),
        //     'category_id' => request('category_id'),
        //     'content' => request('content')
        // ]);
            // $data = $barang->All();
            // $datas = $request->All();
            // dd($request,$request->gambar,$request->file('gambar'));
            if (empty($request->file('gambar'))){
                $update = $request->All();
                $update['gambar'] = $post->gambar;
                $post->update($update);
            }
            else{
                unlink(public_path().'/uploads/file/'.$post->gambar); //menghapus file lama
                $file = $request->file('gambar');
                $ext = $file->getClientOriginalExtension();
                $newName = date("d-m-Y")."-".Auth::user()->name."-".rand(100000,1001238912).".".$ext;
                $file->move('uploads/file',$newName);
                $update = $request->All();
                $update['gambar'] = $newName;
                $post->update($update);
        }
        return redirect()->route('post.index')->withInfo('data berhasil diubah');
    }
    public function destroy(Post $post)  /* public function edit($id)   */
    {
        // dd($post->gambar);
        if (!empty($post->gambar)){
        unlink(public_path().'/uploads/file/'.$post->gambar); //menghapus file lama
        }
        $post->delete();
        return redirect()->route('post.index')->withDanger('data berhasil dihapus');
    }
}
