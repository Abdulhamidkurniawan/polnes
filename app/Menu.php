<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['urutan', 'tipe', 'sub_menu', 'judul', 'link', 'status']; /* yang bsa di isi */
}
