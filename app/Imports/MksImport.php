<?php

namespace App\Imports;

use App\Mk;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MksImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Mk([
            'kode_mk'     => $row['kode'],
            'nama_mk'    => $row['nama'],
            'jurusan'    => $row['jurusan'],
            'prodi'    => $row['prodi'],
            'semester'    => $row['semester'],
            'sks'    => $row['sks'],
            'jumlah_jam'    => $row['jumlah'],
        ]);
    }
}
