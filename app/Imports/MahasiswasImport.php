<?php

namespace App\Imports;

use App\Mahasiswa;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MahasiswasImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Mahasiswa([
            'nim'     => $row['nim'],
            'nama'    => $row['nama'],
            'jurusan'    => $row['jurusan'],
            'prodi'    => $row['prodi'],
            'status'    => $row['status'],

        ]);
    }
}
