<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jam extends Model
{
    protected $fillable = ['hari', 'jam_awal', 'jam_akhir']; /* yang bsa di isi */
}
