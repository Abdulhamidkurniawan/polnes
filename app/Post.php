<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'content', 'category_id', 'slug', 'gambar']; /* yang bsa di isi */
    
    public function category () 
    {
        return $this->belongsTo(Category::class);
    }
    
    public function comments ()
    {
        return $this->hasMany(Comment::class); //1 post punya banyak komentar
    } 

}
