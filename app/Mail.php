<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    protected $fillable = ['nomor', 'judul', 'tujuan', 'alamat', 'nim', 'nama', 'email', 'hp', 'prodi', 'jenis']; /* yang bsa di isi */
    
}
