<?php

namespace App\Exports;

use App\Mk;
use Maatwebsite\Excel\Concerns\FromCollection;

class MksExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Mk::all();
    }
}
