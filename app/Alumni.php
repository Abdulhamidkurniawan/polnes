<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumni extends Model
{
    protected $fillable = ['nim', 'nama', 'jenis_kelamin', 'hp', 'jenjang', 'tahun_angkatan', 'judul', 'pembimbing_1', 'pembimbing_2']; /* yang bsa di isi */

}
