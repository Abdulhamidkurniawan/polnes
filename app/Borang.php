<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borang extends Model
{
    protected $fillable = ['jenjang', 'jenis', 'butir', 'judul', 'link']; /* yang bsa di isi */
}
