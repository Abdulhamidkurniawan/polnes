@extends('layouts.dashboard')
@section('content')



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Daftar Navigation Bar (Menu)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">

<div class="box">

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th style="width: 5%;">Urutan</th>
                    <th style="width: 5%;">Tipe</th>
                    <th style="width: 10%;">Sub_Menu</th>
                    <th style="width: 45%;">Judul</th>
                    <th style="width: 20%;">Link + <a href="{{ route('post.index') }}" target="_blank" >Id Post</a></th>
                    <th style="width: 5%;">Status</th>
                    <th style="width: 10%;"><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($menus as $menu)
                    <tr>
                  <td>{{$menu->urutan}}</td>
                  <td>{{$menu->tipe}}</td>
                  <td>{{$menu->sub_menu}}</td>
                  <td>{{$menu->judul}}</td>
                  <td>{{$menu->link}}</td>
                  <td>{{$menu->status}}</td>
                  <td width=100><center>
                    <form action="{{ route('menu.destroy', $menu) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }} <!-- membuat delete PostController bisa dibaca -->
                    <a href="{{ route('menu.edit', $menu) }}" class="btn btn-sm btn-primary" target="_blank">Edit</a>
                    <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Hapus {{$menu->nama}} ?')">Hapus</button>
                    </form>
                  </center>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>

              <button type="button" class="btn btn-success float-right mb-1" data-toggle="modal" data-target="#modalTambahBarang">Tambah Data</button>
<div class="modal fade" id="modalTambahBarang" tabindex="-1" aria-labelledby="modalTambahBarang" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
    <h5 class="modal-title">Tambah Data</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
<!--FORM TAMBAH BARANG-->
<form method="POST" action="{{ route('menu.store') }}">
    @csrf
    <div class="form-group row">
    <label for="turutan" class="col-md-4 col-form-label text-md-right">Tambah Data</label>
    <div class="col-md-6">
      <input id="urutan" type="number" class="form-control" name="urutan" value="{{ old('urutan') }}" autofocus placeholder="Urutan di Navbar">
    </div>
    </div>
    <div class="form-group row">
        <label for="ttest" class="col-md-4 col-form-label text-md-right"></label>
        <div class="col-md-6">
          <select name="tipe" class="form-control">
                <option value="">Pilih Tipe Menu</option>
                <option value="Main Menu">Main Menu</option>
                <option value="Sub Menu">Sub Menu</option>
                <option value="Deep Menu">Deep Menu</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="ttest" class="col-md-4 col-form-label text-md-right"></label>
        <div class="col-md-6">
          <select name="sub_menu" class="form-control">
                <option value="Main Menu">Pilih Sub Menu</option>
                @foreach ($sub_menus as $sub_menu)
                <option @if ($sub_menu->tipe == 'Main Menu') style="background-color: #abebc6;" @else style="background-color: #f9e79f;" @endif value="{{$sub_menu->judul}}">{{ $sub_menu->tipe.' - '.$sub_menu->judul }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="ttipe" class="col-md-4 col-form-label text-md-right">Judul Menu</label>
        <div class="col-md-6">
          <input id="judul" type="text" class="form-control" name="judul" value="{{ old('judul') }}" autofocus placeholder="Nama Menu">
        </div>
    </div>
    <div class="form-group row">
        <label for="ttipe" class="col-md-4 col-form-label text-md-right">Link Menu (Tambahkan id dari <a href="{{ route('post.index') }}" target="_blank" >Id Post</a>)</label>
        <div class="col-md-6">
            <input id="link" type="text" class="form-control" name="link" value="https://elektro.polnes.ac.id/info/" autofocus placeholder="Cth: akademik">
        </div>
    </div>
    <div class="form-group row">
        <label for="ttest" class="col-md-4 col-form-label text-md-right"></label>
        <div class="col-md-6">
          <select name="status" id="status" class="form-control">
                <option value="">Status Menu</option>
                <option value="Aktif">Aktif</option>
                <option value="Non Aktif">Non Aktif</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </div>
    </form>
</div>
</div>
</div>
</div>
            </div>
            <!-- /.box-body -->
          </div>
        <!-- right col -->
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2022 <a href="#">Jurusan Teknik Elektro - Politeknik Negeri Samarinda</a>
    </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').dataTable( {
      'aaSorting': []
  } );
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>

@endsection
