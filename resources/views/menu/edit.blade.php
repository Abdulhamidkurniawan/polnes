@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                <div class="card-header">Edit Navigation Bar (Menu)</div>
                <div class="card-body">
                    <form class="" action="{{ route('menu.update', $menu) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                        <form method="POST" action="{{ route('menu.store') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="tnama" class="col-md-4 col-form-label text-md-right">Urutan Menu</label>
                                <div class="col-md-6">
                                    <input id="urutan" type="number" class="form-control" name="urutan"
                                        value="{{ $menu->urutan }}" autofocus placeholder="Urutan Menu">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="ttest" class="col-md-4 col-form-label text-md-right">Tipe Menu</label>
                                <div class="col-md-6">
                                    <input id="tipe" type="text" class="form-control" name="tipe"
                                        value="{{ $menu->tipe }}" autofocus placeholder="Tipe Menu" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="ttest" class="col-md-4 col-form-label text-md-right">Sub Menu</label>
                                <div class="col-md-6">
                                    <input id="sub_menu" type="text" class="form-control" name="sub_menu"
                                        value="{{ $menu->sub_menu }}" autofocus placeholder="Sub Menu" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="ttest" class="col-md-4 col-form-label text-md-right">Judul Menu</label>
                                <div class="col-md-6">
                                    <input id="judul" type="text" class="form-control" name="judul"
                                        value="{{ $menu->judul }}" autofocus placeholder="Judul Menu">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="ttest" class="col-md-4 col-form-label text-md-right">Link</label>
                                <div class="col-md-6">
                                    <input id="link" type="text" class="form-control" name="link"
                                        value="{{ $menu->link }}" autofocus placeholder="Link Menu">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="ttest" class="col-md-4 col-form-label text-md-right">Status</label>
                                <div class="col-md-6">
                                    <select name="status" id="status" class="form-control">
                                    @foreach ($status_menus as $status_menu)   <!-- $categories dari PostController -->
                                    <option value="{{$status_menu->status}}"
                                    @if ($status_menu->status === $menu->status)
                                        selected
                                    @endif
                                    > {{$status_menu->status}} </option>
                                    @endforeach
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
