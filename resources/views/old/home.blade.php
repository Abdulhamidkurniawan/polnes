@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-body">
                    @if (Auth::user()->jabatan == 'admin')
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}Anda Login sebagai admin!
                    </div>                   
                    @elseif (Auth::user()->jabatan == 'karyawan')
                    Anda Login sebagai karyawan!
                    @else
                    Anda Login sebagai Mahasiswa!
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
