@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit Tahun Ajaran</div>
                            <div class="card-body">
                                    <form class="" action="{{ route('mahasiswa.update', $mahasiswa)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                                            <form method="POST" action="{{ route('mahasiswa.store') }}">
                                                @csrf
                                                <div class="form-group row">
                                                <label for="tnama" class="col-md-4 col-form-label text-md-right">Tambah Data</label>
                                                <div class="col-md-4">
                                                <input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="nim" type="number" class="form-control" name="nim" value="{{$mahasiswa->nim}}" autofocus placeholder="NIM">
                                                </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="ttest" class="col-md-4 col-form-label text-md-right"></label>
                                                    <div class="col-md-4">
                                                    <input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="nama" type="text" class="form-control" name="nama" value="{{$mahasiswa->nama}}" autofocus placeholder="Nama Lengkap">
                                                    </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="ttest" class="col-md-4 col-form-label text-md-right"></label>
                                                        <div class="col-md-4">
                                                            <select name="prodi" class="form-control">
                                                                <option value="">Pilih Prodi</option>
                                                                <option value="D3">D3</option>
                                                                <option value="S1">D4 (S1-Terapan)</option>
                                                            </select>
                                                        </div>
                                                        </div>
                                                                <div class="form-group row">
                                                                    <label for="ttest" class="col-md-4 col-form-label text-md-right"></label>
                                                                    <div class="col-md-4">
                                                                        <select name="status" id="status" class="form-control">
                                                                            <option value="">Status</option>
                                                                            <option value="Aktif">Aktif</option>
                                                                            <option value="Tidak Aktif">Tidak Aktif</option>
                                                                        </select>
                                                                    </div>
                                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                                    </div>
                                                </form>
                                            </form>
                            </div>
                    </div>
        </div>
    </div>
@endsection
