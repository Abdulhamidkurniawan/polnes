<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'Laravel') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css')}}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{asset('bower_components/morris.js/morris.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('bower_components/jvectormap/jquery-jvectormap.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<div class="main-header">
    <!-- Logo -->
    <a href="{{ route('dashboard') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>J</b>TE</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Teknik Elektro</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->

          <!-- Notifications: style can be found in dropdown.less -->

          <!-- Tasks: style can be found in dropdown.less -->

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">{{ Auth::user()->username }}</span>
            </a>
            <ul class="dropdown-menu">
              </li>
              <li class="user-footer">

                <div class="pull-right">
                <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                </div>

              </li>
            </ul>
          </li>

        </ul>
      </div>
    </nav>
  </div>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        @if (Auth::user()->jabatan == 'admin' || Auth::user()->jabatan == 'karyawan')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-newspaper-o"></i> <span>Post</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li class="active"><a href="{{ route('post.index') }}"><i class="fa fa-circle-o"></i> List </a></li>
          @if (Auth::user()->jabatan != 'tamu')
            <li><a href="{{ route('post.create') }}"><i class="fa fa-circle-o"></i> Input </a></li>
          @endif
            {{-- <li><a href="index2.html"><i class="fa fa-circle-o"></i> Control </a></li> --}}
          </ul>
        </li>
        <li class="treeview">
            <a href="#">
              <i class="fa fa-map-o"></i> <span>Menu</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
            <li class="active"><a href="{{ route('menu.index') }}"><i class="fa fa-circle-o"></i> Daftar Menu </a></li>
            {{-- @if (Auth::user()->jabatan != 'tamu')
              <li><a href="{{ route('post.create') }}"><i class="fa fa-circle-o"></i> Input </a></li>
            @endif --}}
            </ul>
          </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-envelope"></i> <span>Surat</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ route('mail.index') }}"><i class="fa fa-circle-o"></i> List </a></li>
            <li class="active"><a href="{{ route('surat_aktif.create') }}"><i class="fa fa-circle-o"></i> Surat Pengantar </a></li>
            <li class="active"><a href="{{ route('pengantar_perusahaan.create') }}"><i class="fa fa-circle-o"></i> Surat Aktif </a></li>
        </ul>
        </li>
        @endif

        @if (Auth::user()->jabatan == 'admin' || Auth::user()->jabatan == 'karyawan' || Auth::user()->jabatan == 'mahasiswa')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-graduation-cap"></i> <span>Alumni</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ route('alumni.index') }}"><i class="fa fa-circle-o"></i> List </a></li>
            <li class="active"><a href="{{ route('alumni.create') }}"><i class="fa fa-circle-o"></i> Tambah Alumni </a></li>
        </ul>
        </li>
        @endif
        @if (Auth::user()->jabatan == 'admin' || Auth::user()->jabatan == 'karyawan')
        <li class="treeview">
            <a href="#">
              <i class="fa fa-cloud"></i> <span>Absen</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              {{-- <li class="active"><a href=""><i class="fa fa-circle-o"></i> User </a></li> --}}
              <li class="active"><a href="{{ route('ta.index') }}"><i class="fa fa-circle-o"></i> Tahun Ajaran </a></li>
              <li class="active"><a href="{{ route('mk.index') }}"><i class="fa fa-circle-o"></i> Daftar Matakuliah </a></li>
              <li class="active"><a href="{{ route('dosen.index') }}"><i class="fa fa-circle-o"></i> Daftar Dosen </a></li>
              <li class="active"><a href="{{ route('mahasiswa.index') }}"><i class="fa fa-circle-o"></i> Daftar Mahasiswa </a></li>
              <li class="active"><a href="{{ route('absen.index') }}"><i class="fa fa-circle-o"></i> Rekap Absen </a></li>
              <li class="active"><a href=""><i class="fa fa-circle-o"></i> Absen </a></li>
          </ul>
          </li>
        @endif
        @if (Auth::user()->username == 'hamid')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-group"></i> <span>User</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ route('user_panel.index') }}"><i class="fa fa-circle-o"></i> List </a></li>
            <li class="active"><a href="{{ route('user_panel.store') }}"><i class="fa fa-circle-o"></i> Tambah User </a></li>
        </ul>
        </li>
        @endif
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Layout Options</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li> -->
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <main class="py-4">
            @yield('content')
        </main>
    </body>
</html>
