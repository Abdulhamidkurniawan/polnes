@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit Tahun Ajaran</div>
                            <div class="card-body">
                                    <form class="" action="{{ route('ta.update', $ta)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                                            <form method="POST" action="{{ route('ta.store') }}">
                                                @csrf
                                                <div class="form-group row">
                                                <label for="tnama" class="col-md-4 col-form-label text-md-right"></label>
                                                <div class="col-md-4">
                                                <input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="angkatan" type="number" class="form-control" name="angkatan" value="{{$ta->angkatan}}" autofocus placeholder="Tahun Angkatan">
                                                </div>
                                                </div>
                                                    <div class="form-group row">
                                                        <label for="ttest" class="col-md-4 col-form-label text-md-right"></label>
                                                        <div class="col-md-4">
                                                            <select name="semester" id="semester" class="form-control">
                                                                <option value="">Semester</option>
                                                                <option value="Ganjil">Ganjil</option>
                                                                <option value="Genap">Genap</option>
                                                            </select>
                                                        </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="ttest" class="col-md-4 col-form-label text-md-right"></label>
                                                            <div class="col-md-4">
                                                            <input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="awal_semester" type="date" class="form-control" name="awal_semester" value="{{$ta->awal_semester}}" autofocus placeholder="Tanggal Mulai Kuliah">
                                                            </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="ttest" class="col-md-4 col-form-label text-md-right"></label>
                                                                <div class="col-md-4">
                                                                <input oninput="let p=this.selectionStart;this.value=this.value.toUpperCase();this.setSelectionRange(p, p);" id="akhir_semester" type="date" class="form-control" name="akhir_semester" value="{{$ta->akhir_semester }}" autofocus placeholder="Tanggal Akhir Kuliah">
                                                                </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="ttest" class="col-md-4 col-form-label text-md-right"></label>
                                                                    <div class="col-md-4">
                                                                        <select name="status" id="status" class="form-control">
                                                                            <option value="">Status</option>
                                                                            <option value="Aktif">Aktif</option>
                                                                            <option value="Tidak Aktif">Tidak Aktif</option>
                                                                        </select>
                                                                    </div>
                                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                                    </div>
                                                </form>
                                            </form>
                            </div>
                    </div>
        </div>
    </div>
@endsection
