@extends('layouts.app')

@section('content')
    <div class="container">
    <form action="{{ route('post.store')}}" enctype="multipart/form-data" method="post">
        {{ csrf_field() }}
    <div class="form-group has-feedback {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="">Title</label>
    <input type="text" class="form-control" name="title" placeholder="Post Title" value="{{old('title')}}">
                @if ($errors->has('title'))
                <span class="help-block">
                <p>{{$errors->first('title')}}</p>
                </span>
                @endif
            </div>
            <div class="form-group">
                    <label for="">Kategori</label>
                    <select name="category_id" id="" class="form-control">
                    <option value="Pilih Kategori"> Pilih Kategori </option>
                      @foreach ($categories as $category)
                    <option value="{{$category->id}}"> {{$category->name}} </option>
                            @endforeach
                    </select>
            </div>
            <div class="form-group">
              <label for="">Gambar</label>
              <input type="file" class="form-control" name="gambar" placeholder="Upload Gambar" value="{{ old('gambar') }}" style="width:300px">
            </div>
            <div class="form-group has-feedback {{$errors->has('content') ? 'has-error' : ''}}">
                    <label for="">Content</label>
                    <textarea name="content" class="form-control" rows="5" placeholder="Post Content">{{old('content')}}</textarea>
                    @if ($errors->has('content'))
                    <span class="help-block">
                    <p>{{$errors->first('content')}}</p>
                    </span>
                    @endif
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Save">
            </div>
        </form>

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script>
         var route_prefix = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
        </script>

          <!-- TinyMCE init -->
    <script src="https://cdn.tiny.cloud/1/k2heelpz9ksb7b97v9imhk4lttpqo51jee7uksohp2ok5vnz/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>  <script>
    var editor_config = {
      path_absolute : "",
      selector: "textarea[name=content]",
      plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      relative_urls: false,
      height: 1500,
      file_picker_callback (callback, value, meta) {
        let x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth
        let y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight

        tinymce.activeEditor.windowManager.openUrl({
          url : '/file-manager/tinymce5',
          title : 'Laravel File manager',
          width : x * 0.8,
          height : y * 0.8,
          onMessage: (api, message) => {
            callback(message.content, { text: message.text })
          }
        })
      }
    };

    tinymce.init(editor_config);
  </script>
    </div>

@endsection
