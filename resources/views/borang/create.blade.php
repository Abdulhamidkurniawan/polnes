@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('borang.store') }}">
                        @csrf
                        {{-- {{ route('register') }} --}}
                        <div class="form-group">
                                                    
                                                    <label for="">Jenjang</label>
                                                <input type="text" class="form-control" name="jenjang" placeholder="Cth : D3/D4" value="{{ old('jenjang') }}" required><!-- $borang dari route dan borangController -->
                                                </div>
                                                <div class="form-group">    
                                                    <label for="">Jenis</label>
                                                <input type="text" class="form-control" name="jenis" placeholder="Cth : 3A/3B" value="{{ old('jenis') }}" required><!-- $borang dari route dan borangController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Butir</label>
                                                <input type="text" class="form-control" name="butir" placeholder="Cth : 3.1.1" value="{{ old('butir') }}" required><!-- $borang dari route dan borangController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Judul</label>
                                                <input type="text" class="form-control" name="judul" placeholder="Judul Borang" value="{{ old('judul') }}" required><!-- $borang dari route dan borangController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Link</label>
                                                <input type="text" class="form-control" name="link" placeholder="Link Google Sheets" value="{{ old('link') }}" required><!-- $borang dari route dan borangController -->
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
