@extends('layouts.dashboard')
@section('content')



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Absen</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">

<div class="box">

            <!-- /.box-header -->
            <div class="box-body">
                <form class="" action="{{ route('absen.index') }}" method="post">
                    @csrf
                <div class="form-group row">
                    <label for="nama_mk" class="col-md-1 col-form-label text-md-right">{{ __('NIM') }}</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="nim" placeholder="NIM Mhs" value="{{ old('nim') }}"><!-- $alumni dari route dan alumniController -->
                        </div>
                </div>
                <div class="form-group row">
                    <label for="tahun_angkatan" class="col-md-1 col-form-label text-md-right">{{ __('Tahun Ajaran') }}</label>
                        <div class="col-md-2">
                            <select name="tahun_ajaran" class="form-control">
                                <option value="">Tahun Ajaran</option>
                                @foreach ($tas as $ta)
                                <option value="{{$ta->tahun_ajaran}}">{{$ta->tahun_ajaran}} - {{$ta->semester}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select name="prodi" class="form-control">
                                <option value="">Pilih Prodi</option>
                                <option value="D3">D3 Teknik Listrik</option>
                                <option value="S1">S1 Teknik Listrik</option>
                                <option value="ALJ">S1 Alih Jenjang</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select name="kelas" class="form-control">
                                <option value="">Pilih Kelas</option>
                                <option value="1A">1A</option>
                                <option value="1B">1B</option>
                                <option value="2A">2A</option>
                                <option value="2B">2B</option>
                                <option value="3A">3A</option>
                                <option value="3B">3B</option>
                                <option value="4A">4A</option>
                                <option value="4B">4B</option>
                                <option value="5A">5A</option>
                                <option value="5B">5B</option>
                                <option value="6A">6A</option>
                                <option value="6B">6B</option>
                                <option value="7A">7A</option>
                                <option value="7B">7B</option>
                                <option value="8A">8A</option>
                                <option value="8B">8B</option>
                            </select>
                        </div>
                </div>
                <div class="form-group row">
                    <label for="nama_mk" class="col-md-1 col-form-label text-md-right">{{ __('Mata Kuliah') }}</label>
                        <div class="col-md-4">
                            <select name="nama_mk" id="nama_mk" class="form-control">
                            </select>
                        </div>
                </div>
                <div class="form-group row">
                    <label for="tanggal" class="col-md-1 col-form-label text-md-right">{{ __('Tanggal') }}</label>
                        <div class="col-md-4">
                            <input type="date" class="form-control" name="tanggal" placeholder="Pilih Tanggal" value="{{ old('tanggal') }}"><!-- $alumni dari route dan alumniController -->
                        </div>
                </div>
                <div class="form-group row">
                    <label for="button" class="col-md-1 col-form-label text-md-right">{{ __('') }}</label>
                        <div class="col-md-2">
                            <input type="submit" class="btn btn-primary" value="Buka">
                        </div>
                </div>
                </form>
                {{-- <a href="{{ route('absen.create') }}" class="btn btn-sm btn-success" target="_blank">Tambah Data</a> --}}
                <p> </p>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NIM</th>
                  <th>Nama</th>
                  <th>Matakuliah</th>
                  <th>Dosen Pengajar</th>
                  <th>Prodi</th>
                  <th>Semester</th>
                  <th>Kelas</th>
                  <th>Jumlah Jam</th>
                  <th>Jam Pertemuan</th>
                  <th>Tanggal</th>
                  <th>Materi</th>
                  {{-- <th>SKS</th> --}}
                  <th><center>Aksi</center></th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($absens as $absen)
                    <tr>
                  <td>{{$absen->nim}}</td>
                  <td>{{$absen->nama}}</td>
                  <td>{{$absen->nama_mk}}</td>
                  <td>{{$absen->dosen}}</td>
                  <td>{{$absen->prodi}}</td>
                  <td>{{$absen->semester}}</td>
                  <td>{{$absen->kelas}}</td>
                  <td>{{$absen->jumlah_jam}}</td>
                  <td>{{$absen->jam_pertemuan}}</td>
                  <td>{{$absen->tanggal}}</td>
                  <td>{{$absen->materi}}</td>
                  <td width=200>
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modaledit">Edit</button>
                    <button type="submit" class="btn btn-sm btn-danger" form="delform" onclick="return confirm('Hapus {{$absen->email}} ?')">Hapus</button>

                    <div class="modal fade" id="modaledit" tabindex="-1" aria-labelledby="modaledit" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    <!--FORM TAMBAH BARANG-->
                    <form class="" action="{{ route('absen.update', $absen)}}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <input type="date" class="form-control" name="tanggal" placeholder="Pilih Tanggal" value="{{$absen->tanggal}}"><!-- $alumni dari route dan alumniController -->
                        <br>
                        <br>
                        <button type="submit" class="btn btn-sm btn-success">Simpan</button>
                    </form>
                    <!--END FORM TAMBAH BARANG-->
                    </div>
                    </div>
                    </div>
                    </div>

                    <form id="delform" action="{{ route('absen.destroy', $absen) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }} <!-- membuat delete PostController bisa dibaca -->
                    {{-- <a href="{{ route('absen.edit', $absen) }}" class="btn btn-sm btn-primary" target="_blank">Edit</a> --}}
                    {{-- <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Hapus {{$absen->email}} ?')">Hapus</button> --}}
                    </form>
                  </td>
                </tr>
                @endforeach
                </tbody>

              </table>
              {{-- <button type="button" class="btn btn-success float-right mb-1" data-toggle="modal" data-target="#modalexim">Import/Export</button>
<div class="modal fade" id="modalexim" tabindex="-1" aria-labelledby="modalexim" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title">Import/Export Data</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<!--FORM TAMBAH BARANG-->
<form action="" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="file" name="file" class="form-control">
    <br>
    <button class="btn btn-success">Import Data</button>
    <a class="btn btn-warning" href="">Export Data</a>
</form>
<!--END FORM TAMBAH BARANG-->
</div>
</div>
</div>
</div> --}}
            </div>
            <!-- /.box-body -->
          </div>
        <!-- right col -->
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2022 <a href="#">Jurusan Teknik Elektro - Politeknik Negeri Samarinda</a>
  </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
  $(function () {
    $('#example1').dataTable( {
      'aaSorting': []
  } );
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
<script type="text/javascript">
    jQuery(document).ready(function ()
    {
            jQuery('select[name="kelas"]').on('change',function(){
               var kelas = jQuery(this).val();
               console.log(kelas)
               var prodi = jQuery('select[name="prodi"]').val();
               console.log(prodi)

            //    alert(countryID);
               if(prodi)
               {
                  jQuery.ajax({
                     url : 'https://'+document.location.hostname+'/getmk/'+prodi+'/kelas/'+kelas,
                     type : "GET",
                     dataType : "json",
                     cache: false,
                     success:function(data)
                     {
                        // alert(countryID);
                        console.log(data);
                        jQuery('select[name="nama_mk"]').empty();
                        jQuery.each(data, function(key,value){
                           $('select[name="nama_mk"]').append('<option value="'+ value +'">'+ value +'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="nama_mk"]').empty();
               }
            });
    });
</script>
@endsection
