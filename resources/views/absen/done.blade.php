<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>JTE</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">


            <div class="content">
                <div class="title m-b-md">
                    Terima kasih !
                </div>
                @if (Route::has('login'))
                <div class="links">
                        <a href="#">Profil</a>
                        <a href="#">Informasi Akademik</a>
                        <a href="#">E-Alumni</a>
                        <a href="#">E-Modul</a>
                        <a href="#">Kalender Akademik</a>
                        <a href="#">Lokasi</a>
                </div>
            @endif
            </div>
        </div>
    </body>
    <script>
        //Using setTimeout to execute a function after 5 seconds.
        setTimeout(function () {
           //Redirect with JavaScript
           window.location.href= 'https://elektro.polnes.ac.id';
        }, 5000);
    </script>
</html>
