@extends('layouts.app') <!-- --> <!-- include-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Isi Absen - <label id="time"></label></div>
                        @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="card-body">
                                    <form class="" action="{{ route('absen.isistore') }}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                    <label for="">NIM</label>
                                                    <input type="text" class="form-control" name="nim" placeholder="NIM Mahasiswa" value="" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Nama</label>
                                                    <select name="nama" id="nama" class="form-control">
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Program Studi</label>
                                                    <select name="prodi" class="form-control">
                                                        <option value="">Pilih Prodi</option>
                                                        <option value="D3">D3</option>
                                                        <option value="S1">S1 Terapan</option>
                                                        <option value="ALJ">S1 Alih Jenjang</option>
                                                    </select>
                                                </div>
                                                {{-- <div class="form-group">
                                                    <label for="">Semester</label>
                                                    <input type="number" class="form-control" name="semester" min="1" max="8" placeholder="Semester" value="" required><!-- $alumni dari route dan alumniController -->
                                                </div> --}}
                                                <div class="form-group">
                                                    <label for="">Tahun Ajaran</label>
                                                    <select name="tahun_ajaran" class="form-control">
                                                        @foreach ($tas as $ta)
                                                        <option value="{{$ta->tahun_ajaran}}">{{$ta->tahun_ajaran}} - {{$ta->semester}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Kelas</label>
                                                    <select name="kelas" class="form-control">
                                                        <option value="">Pilih Kelas</option>
                                                        <option value="1A">1A</option>
                                                        <option value="1B">1B</option>
                                                        <option value="2A">2A</option>
                                                        <option value="2B">2B</option>
                                                        <option value="3A">3A</option>
                                                        <option value="3B">3B</option>
                                                        <option value="4A">4A</option>
                                                        <option value="4B">4B</option>
                                                        <option value="5A">5A</option>
                                                        <option value="5B">5B</option>
                                                        <option value="6A">6A</option>
                                                        <option value="6B">6B</option>
                                                        <option value="7A">7A</option>
                                                        <option value="7B">7B</option>
                                                        <option value="8A">8A</option>
                                                        <option value="8B">8B</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Mata Kuliah</label>
                                                    <select name="nama_mk" id="nama_mk" class="form-control">
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Dosen MK</label>
                                                    <select name="dosen" class="form-control">
                                                        @foreach ($dosens as $dosen)
                                                        <option value="{{$dosen->nama}}">{{$dosen->nama}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                {{-- <div class="form-group">
                                                    <label for="">Jumlah Jam Perkuliahan</label>
                                                    <input type="number" class="form-control" name="jumlah_jam" min="1" max="9" value="" required><!-- $alumni dari route dan alumniController -->
                                                </div> --}}
                                                <div class="form-group">
                                                    <label for="">Pertemuan Jam Ke</label><br>
                                                    {{-- <input type="text" class="form-control" name="jam_pertemuan" placeholder="Jam Pertemuan" value="" required><!-- $alumni dari route dan alumniController --> --}}
                                                    <input type="checkbox" name="jam_pertemuan[]" value="1">
                                                    <label for="p1"> 1 </label><br>
                                                    <input type="checkbox" name="jam_pertemuan[]" value="2">
                                                    <label for="p2"> 2</label><br>
                                                    <input type="checkbox" name="jam_pertemuan[]" value="3">
                                                    <label for="p3"> 3</label><br>
                                                    <input type="checkbox" name="jam_pertemuan[]" value="4">
                                                    <label for="p4"> 4</label><br>
                                                    <input type="checkbox" name="jam_pertemuan[]" value="5">
                                                    <label for="p5"> 5</label><br>
                                                    <input type="checkbox" name="jam_pertemuan[]" value="6">
                                                    <label for="p6"> 6</label><br>
                                                    <input type="checkbox" name="jam_pertemuan[]" value="7">
                                                    <label for="p7"> 7</label><br>
                                                    <input type="checkbox" name="jam_pertemuan[]" value="8">
                                                    <label for="p8"> 8</label><br>
                                                    <input type="checkbox" name="jam_pertemuan[]" value="9">
                                                    <label for="p9"> 9</label><br>
                                                    <input type="checkbox" name="jam_pertemuan[]" value="10">
                                                    <label for="p10"> 10</label><br>
                                                    <input type="checkbox" name="jam_pertemuan[]" value="11">
                                                    <label for="p11"> 11</label><br>
                                                    <input type="checkbox" name="jam_pertemuan[]" value="12">
                                                    <label for="p12"> 12</label><br>
                                                    <input type="checkbox" name="jam_pertemuan[]" value="13">
                                                    <label for="p13"> 13</label><br>

                                                </div>
                                                <div class="form-group row col-md-4">
                                                    <label for="">Tanggal</label>
                                                    <input type="date" class="form-control" name="tanggal" placeholder="Tanggal Perkuliahan" value="" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Judul Materi yang diajarkan</label>
                                                    <input type="text" class="form-control" name="materi" placeholder="Judul Materi" value="" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <input onclick="return confirm('Apakah data sudah benar ?')" type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </form>
                            </div>
                    </div>
        </div>
    </div>


    <script type="text/javascript">
      function showTime() {
        var date = new Date(),
            utc = new Date(
              date.getFullYear(),
              date.getMonth(),
              date.getDate(),
              date.getHours(),
              date.getMinutes(),
              date.getSeconds()
            );

        document.getElementById('time').innerHTML = utc.toLocaleTimeString();
      }

      setInterval(showTime, 1000);
    </script>

    <script type="text/javascript">
        jQuery(document).ready(function ()
        {
                jQuery('select[name="kelas"]').on('change',function(){
                   var kelas = jQuery(this).val();
                   console.log(kelas)
                   var prodi = jQuery('select[name="prodi"]').val();
                   console.log(prodi)

                //    alert(countryID);
                   if(prodi)
                   {
                      jQuery.ajax({
                         url : 'https://'+document.location.hostname+'/getmk/'+prodi+'/kelas/'+kelas,
                         type : "GET",
                         dataType : "json",
                         cache: false,
                         success:function(data)
                         {
                            // alert(countryID);
                            console.log(data);
                            jQuery('select[name="nama_mk"]').empty();
                            jQuery.each(data, function(key,value){
                               $('select[name="nama_mk"]').append('<option value="'+ value +'">'+ value +'</option>');
                            });
                         }
                      });
                   }
                   else
                   {
                      $('select[name="nama_mk"]').empty();
                   }
                });
        });
</script>

<script type="text/javascript">
    jQuery(document).ready(function ()
    {
        jQuery('input[name="nim"]').on('change',function(){
                   var nim = jQuery(this).val();
                   console.log(nim)

                //    alert(countryID);
                   if(nim)
                   {
                      jQuery.ajax({
                         url : 'https://'+document.location.hostname+'/getmhs/'+nim,
                         type : "GET",
                         dataType : "json",
                         cache: false,
                         success:function(data)
                         {
                            // alert(countryID);
                            console.log(data);
                            jQuery('select[name="nama"]').empty();
                            jQuery.each(data, function(key,value){
                               $('select[name="nama"]').append('<option value="'+ value +'">'+ value +'</option>');
                            });
                         }
                      });
                   }
                   else
                   {
                      $('select[name="nama"]').empty();
                   }
                });
    });
    </script>
@endsection
