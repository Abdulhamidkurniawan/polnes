@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Tambah Alumni</div>
                        @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="card-body">
                                    <form class="" action="{{ route('alumni.store') }}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                    <label for="">NIM</label>
                                                    <input type="text" class="form-control" name="nim" placeholder="NIM Mahasiswa" value="" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Nama</label>
                                                    <input type="text" class="form-control" name="nama" placeholder="Nama Mahasiswa" value="" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Jenis Kelamin</label>
                                                    <select name="jenis_kelamin" class="form-control">
                                                            <option value="Laki-laki">Laki-laki</option>
                                                            <option value="Perempuan">Perempuan</option>
                                                    </select> <!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Hp/Telp</label>
                                                    <input type="text" class="form-control" name="hp" placeholder="Telp Mahasiswa" value="" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Jenjang</label>
                                                    <input type="text" class="form-control" name="jenjang" placeholder="D-III/D-IV" value="" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Tahun Angkatan</label>
                                                    <input type="text" class="form-control" name="tahun_angkatan" placeholder="cth: 2019" value="" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Judul</label>
                                                    <input type="text" class="form-control" name="judul" placeholder="Judul Tugas Akhir/Skripsi" value="" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Pembimbing 1</label>
                                                    <input type="text" class="form-control" name="pembimbing_1" placeholder="Nama Pembimbing 1" value="" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Pembimbing 2</label>
                                                    <input type="text" class="form-control" name="pembimbing_2" placeholder="Nama Pembimbing 2" value="" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </form>
                            </div>
                    </div> 
        </div>
    </div>   
@endsection