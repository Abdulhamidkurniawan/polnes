@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit Surat</div>
                            <div class="card-body">
                                    <form class="" action="{{ route('alumni.update', $alumni)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch alumniController bisa dibaca -->
                                            <div class="form-group">
                                                    <label for="">NIM</label>
                                                    <input type="text" class="form-control" name="nim" placeholder="NIM Mahasiswa" value="{{$alumni->nim}}" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Nama</label>
                                                    <input type="text" class="form-control" name="nama" placeholder="Nama Mahasiswa" value="{{$alumni->nama}}" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Jenis Kelamin</label>
                                                    <select name="jenis_kelamin" class="form-control">
                                                            <option value="Laki-laki">Laki-laki</option>
                                                            <option value="Perempuan">Perempuan</option>
                                                    </select> <!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Hp/Telp</label>
                                                    <input type="text" class="form-control" name="hp" placeholder="Telp Mahasiswa" value="{{$alumni->hp}}" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Jenjang</label>
                                                    <input type="text" class="form-control" name="jenjang" placeholder="D-III/D-IV" value="{{$alumni->jenjang}}" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Tahun Angkatan</label>
                                                    <input type="text" class="form-control" name="tahun_angkatan" placeholder="cth: 2019" value="{{$alumni->tahun_angkatan}}" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Judul</label>
                                                    <input type="text" class="form-control" name="judul" placeholder="Judul Tugas Akhir/Skripsi" value="{{$alumni->judul}}" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Pembimbing 1</label>
                                                    <input type="text" class="form-control" name="pembimbing_1" placeholder="Nama Pembimbing 1" value="{{$alumni->pembimbing_1}}" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Pembimbing-2</label>
                                                    <input type="text" class="form-control" name="pembimbing_2" placeholder="Nama Pembimbing 2" value="{{$alumni->pembimbing_2}}" required><!-- $alumni dari route dan alumniController -->
                                                </div>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </form>
                            </div>
                    </div> 
        </div>
    </div>   
@endsection