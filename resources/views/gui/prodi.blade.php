<!DOCTYPE html>
<html lang="en">
<style type="text/css">
    #hero {
      background: url("{{asset('assets/img/bengkel2.jpg')}}") top center;
    }
    #cta {
      background: linear-gradient(rgba(2, 2, 2, 0.5), rgba(0, 0, 0, 0.5)), url("{{asset('assets/img/cta-bg.jpg')}}") fixed center center;
    }
    </style>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Jurusan Teknik Elektro</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{asset('assets/img/favicon.png')}}" rel="icon">

  <link href="{{asset('assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/venobox/venobox.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/aos/aos.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{asset('assets/css/styleday.css')}}" rel="stylesheet">
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top ">
    <div class="container d-flex">
      <div class="contact-info mr-auto">
        <i class="icofont-envelope"></i> <a href="mailto: polnes@polnes.ac.id">polnes@polnes.ac.id</a>
        <i class="icofont-phone"></i> (0541) 260588
      </div>
      <div class="social-links">
        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        <a href="#" class="skype"><i class="icofont-skype"></i></a>
        <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a>
      </div>
    </div>
  </div>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top ">
  <div class="container d-flex align-items-center">

      <!-- <h1 class="logo mr-auto"><a href="index.html"><img src="assets/img/logo_jte.jpg" class="img-fluid"> Teknik Elektro</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
      <a href="{{ route('gui.default') }}" class="logo mr-auto"><img src="assets/img/logo_jte.jpg" alt=""
              class="img-fluid"></a>

      <nav class="nav-menu d-none d-lg-block">
          <ul>
              @foreach ($menus as $menu)
                  @if ($menu->count > 0)
                      <li class="drop-down"><a href="{{ $menu->link }}">{{ $menu->judul }}</a>
                          <ul>
                              @foreach ($sub_menus as $sub_menu)
                                  @if ($sub_menu->sub_menu == $menu->judul)
                                    @if ($sub_menu->count > 0)
                                      <li class="drop-down"><a href="{{ $sub_menu->link }}">{{ $sub_menu->judul }}</a>
                                          <ul>
                                            @foreach ($deep_menus as $deep_menu)
                                              @if ($deep_menu->sub_menu == $sub_menu->judul)
                                                  <li><a href="{{ $deep_menu->link }}">{{ $deep_menu->judul }}</a>
                                              @endif
                                            @endforeach
                                          </ul>
                                      </li>
                                    @else
                                      <li><a href="{{ $sub_menu->link }}">{{ $sub_menu->judul }}</a>
                                    @endif
                                  @endif
                              @endforeach
                          </ul>
                      </li>
                @else
                    <li><a href="{{ $menu->link }}">{{ $menu->judul }}</a></li>
                @endif
              @endforeach
              {{-- <li class="active"><a href="{{ route('gui.default') }}">Beranda</a></li>
              <li class="drop-down"><a href="#about">Profil</a>
                  <ul>
                      <li><a href="#">Visi Misi</a></li>
                      <li><a href="#">Struktur Organisasi</a></li>
                      @foreach ($pengajars as $pengajar)
                          <li><a href="{{ route('gui.pengajar', $pengajar) }}">Dosen</a></li>
                      @endforeach
                  </ul>
              </li> --}}

              {{-- <li class="drop-down"><a href="">Akademik</a>
                  <ul>
                      <li><a href="#">Kurikulum</a></li>
                      <li><a href="#">Capaian Pembelajaran Lulusan</a></li>
                      <li><a href="#">Daftar Matakuliah</a></li>
                      <li><a href="#">Rencana Pembelajaran Semester</a></li>
                      <li><a href="#">Tugas Akhir</a></li>
                      <li><a href="#">Panduan Pengembangan</a></li>
                  </ul>
              </li> --}}
              {{-- <li><a href="#services">Layanan</a></li>
              <li><a href="#portfolio">Galeri</a></li>
              <li><a href="#team">Tim</a></li>
              <li><a href="{{ route('gui.loker') }}">Lowongan Kerja</a></li>
              <li><a href="#contact">Kontak</a></li> --}}
              <!-- <li><a href="#pricing">Pricing</a></li> -->
              <!-- <li><a href="#tes">tes</a></li> -->
              {{-- <li class="drop-down"><a href="">Akademik</a>
      <ul>
        <li><a href="#">Kurikulum</a></li>
        <li class="drop-down"><a href="#">Deep Drop Down</a>
          <ul>
            <li><a href="#">Deep Drop Down 1</a></li>
            <li><a href="#">Deep Drop Down 2</a></li>
            <li><a href="#">Deep Drop Down 3</a></li>
            <li><a href="#">Deep Drop Down 4</a></li>
            <li><a href="#">Deep Drop Down 5</a></li>
          </ul>
        </li>
        <li><a href="#">Capaian Pembelajaran Lulusan</a></li>
        <li><a href="#">Drop Down 3</a></li>
        <li><a href="#">Daftar Matakuliah</a></li>
        <li><a href="#">Rencana Pembelajaran Semester</a></li>
        <li><a href="#">Tugas Akhir</a></li>
        <li><a href="#">Panduan Pengembangan</a></li>
      </ul>
    </li> --}}
          </ul>
      </nav><!-- .nav-menu -->
    </div>
</header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">
    <div class="container position-relative" data-aos="fade-up" data-aos-delay="500">
      @if ($post->category_id == '7')
      <h1>Lowongan Kerja</h1>
      <h2>{{$post->title}}</h2>
      @else
      <h1>{{$post->title}}</h1>
      <h2>Jurusan Teknik Elektro</h2>
      @endif

      <a href="#about" class="btn-get-started scrollto">Selengkapnya</a>
    </div>
  </section>
  <!-- End Hero -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row">
          <!-- <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left">
            <img src="{{asset('assets/img/bengkel1.jpg')}}" class="img-fluid" alt="">
          </div> -->
          <div class="col-lg-12 pt-4 pt-lg-0 order-2 order-lg-1 content" data-aos="fade-right">
            <h3>{{$post->title}}</h3>
            <!-- <p class="font-italic">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
              magna aliqua.
            </p>
            <ul>
              <li><i class="icofont-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
              <li><i class="icofont-check-circled"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
              <li><i class="icofont-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
            </ul> -->
            <p>
            {!!$post->content!!}
            </p>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->
  </main><!-- End #main -->

  <footer id="footer">

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>JTE</span></strong>. All Rights Reserved
      </div>
      <!-- <div class="credits">
         Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div> -->
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
  <script src="{{asset('assets/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{asset('assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{asset('assets/vendor/venobox/venobox.min.js')}}"></script>
  <script src="{{asset('assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('assets/vendor/aos/aos.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{asset('assets/js/mainday.js')}}"></script>

</body>

</html>
