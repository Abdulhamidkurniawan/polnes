@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-body">
                    <form method="POST" action="{{ route('mail.store') }}">
                        @csrf
                        {{-- {{ route('register') }} --}}
                                                <div class="form-group">
                                                    <label for="">Judul Tugas Akhir/Skripsi</label>
                                                <input type="text" class="form-control" name="judul" placeholder="Tuliskan judul anda disini ..." value="{{ old('judul') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Tujuan</label>
                                                <input type="text" class="form-control" name="tujuan" placeholder="Tujuan Surat" value="{{ old('tujuan') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Alamat Perusahaan</label>
                                                <input type="text" class="form-control" name="alamat" placeholder="Alamat Tujuan/Perusahaan" value="{{ old('alamat') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">NIM</label>
                                                <input type="text" class="form-control" name="nim" placeholder="NIM Mahasiswa" value="{{ old('nim') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Nama</label>
                                                <input type="text" class="form-control" name="nama" placeholder="Nama Mahasiswa" value="{{ old('nama') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">E-Mail</label>
                                                    <input type="text" class="form-control" name="email" placeholder="E-mail Mahasiswa" value="{{ old('email') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Hp/Telp</label>
                                                <input type="text" class="form-control" name="hp" placeholder="Telp Mahasiswa" value="{{ old('hp') }}" required><!-- $mail dari route dan mailController -->
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Program Studi</label>
                                                    <select name="prodi" class="form-control">
                                                        <option value="Teknik Listrik - DIII">Teknik Listrik - DIII</option>
                                                        <option value="Teknik Listrik - DIV">Teknik Listrik - DIV</option>
                                                     </select>                                                
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Jenis Surat</label>
                                                    <select name="jenis" class="form-control">
                                                        <option value="Kerja Praktek - DIII">Kerja Praktek - DIII</option>
                                                        <option value="Kerja Praktek - DIV">Kerja Praktek - DIV</option>
                                                        <option value="Tugas Akhir">Tugas Akhir</option>
                                                        <option value="Skripsi">Skripsi</option>
                                                       {{-- <option value="mahasiswa">mahasiswa</option> --}}
                                                     </select>  
                                                     <br>
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
