@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-12 offset-md-0">
        <div class="card">
                            {{-- {!! Form::open (['method'=>'GET','url'=>'carimail','role'=>'search'])!!}
                                <div class="card-header">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" name="nama" placeholder="Masukan kode atau nama mail" value=""><!-- $borang dari route dan borangController -->                                         
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-sm btn-info">Cari mail</button>
                                        </div>
                                    </div>
                                </div>
                            {!! Form::close()!!}
                            <br> --}}
                <table class="table table-hover" >
                    <thead>
                      <tr>
                        <th scope="col"><center>No</center></th>
                        <th scope="col"><center>Nama</center></th>
                        <th scope="col"><center>NIM</center></th>
                        <th scope="col"><center>Jenis Surat</center></th>
                        <th scope="col"><center>Tanggal</center></th>
                        <th scope="col"><center>Jenjang</center></th>
                        <th scope="col"><center>Pilihan</center></th>
                      </tr>
                    </thead>
                    <?php $c=0;?>            
            
                    @foreach ($mails as $mail)

                    <tbody>
                        <tr>
                            <?php $c=$c+1;?>
                        <th scope="row"><center>{{$c}}</center></th>
                          <td>{{$mail->nama}}</td>
                          <td><center><a href="{{ route('mail.show', $mail) }}" >{{$mail->nim}}</a></center></td>
                          <td><center>{{$mail->jenis}}</center></td>
                          <td><center>{{substr($mail->created_at,0,-9)}}</center></td>
                          <td><center>{{$mail->prodi}}</center></td>
                          <td><form action="{{ route('mail.destroy', $mail) }}" method="post">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }} <!-- membuat delete mailController bisa dibaca -->
                           <center><a href="{{ route('mail.edit', $mail) }}" class="btn btn-sm btn-primary">Edit</a> 
                              <a href="{{ route('mail.cetak', $mail) }}" class="btn btn-sm btn-info">Cetak</a> 
                            @if (Auth::user()->jabatan == 'admin')
                              <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                            @endif
                    </form>                  
                  </center></td>
                        </tr>
                      </tbody>
                      @endforeach
                    </table>

            <br>
            <br>
            {!! $mails->render() !!}
        </div>
</div>
@endsection