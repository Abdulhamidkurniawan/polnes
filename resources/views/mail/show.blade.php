@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="row">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">{{$mail->title}} | <small>{{$mail->jenis}}</small></div>
                        <div class="card-body">
                            <p> {{$mail->content}} </p>
                        </div>
                </div>
            <br>
        </div>
        </div>
    </div>
@endsection
