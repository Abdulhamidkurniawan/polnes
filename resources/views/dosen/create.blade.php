@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Data Matakuliah') }}</div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <div class="card-body">
                    <form method="POST" action="{{ route('mk.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="kode_mk" class="col-md-4 col-form-label text-md-right">{{ __('Kode') }}</label>
                            <div class="col-md-6">
                                <input id="kode_mk" type="text" class="form-control" name="kode_mk" placeholder="Kode Matakuliah" value="{{ old('kode_mk') }}" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nama_mk" class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>
                            <div class="col-md-6">
                                <input id="nama_mk" type="text" class="form-control" name="nama_mk" placeholder="Nama Matakuliah" value="{{ old('nama_mk') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="prodi" class="col-md-4 col-form-label text-md-right">{{ __('Prodi') }}</label>
                                <div class="col-md-6">
                                    <select name="prodi" class="form-control">
                                       <option value="D3">D3 Teknik Listrik</option>
                                       <option value="S1">S1 Teknik Listrik</option>
                                    </select>
                                 </div>
                        </div>
                        <div class="form-group row">
                            <label for="semester" class="col-md-4 col-form-label text-md-right">{{ __('Semester') }}</label>
                            <div class="col-md-6">
                                <input id="semester" type="number" class="form-control" name="semester" placeholder="1 s.d. 8" min ="1" max ="8" value="{{ old('semester') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="sks" class="col-md-4 col-form-label text-md-right">{{ __('SKS') }}</label>
                            <div class="col-md-6">
                                <input id="sks" type="text" class="form-control" name="sks" placeholder="Jumlah SKS" value="{{ old('sks') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jumlah_jam" class="col-md-4 col-form-label text-md-right">{{ __('Jumlah Jam') }}</label>
                            <div class="col-md-6">
                                <input id="jumlah_jam" type="number" class="form-control" name="jumlah_jam" placeholder="Jam" min ="1" max ="9" value="{{ old('jumlah_jam') }}" required>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Tambah Data') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
