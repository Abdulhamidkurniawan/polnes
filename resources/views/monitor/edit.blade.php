@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit water</div>
                            <div class="card-body">
                            <form class="" action="{{ route('monitor.update', $monitor)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                                            <div class="form-group row">
                                                <label for="id_node" class="col-md-4 col-form-label text-md-right">{{ __('Id Node') }}</label>

                                                <div class="col-md-6">
                                                    <input id="id_node" type="text" class="form-control{{ $errors->has('id_node') ? ' is-invalid' : '' }}" name="id_node" value="{{$monitor->id_node}}" required autofocus>

                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="valve" class="col-md-4 col-form-label text-md-right">{{ __('Valve') }}</label>

                                                <div class="col-md-6">
                                                    <input id="valve" type="text" class="form-control{{ $errors->has('valve') ? ' is-invalid' : '' }}" name="valve" value="{{$monitor->valve}}" required>

                                                </div>
                                            </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-6 offset-md-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{ __('Simpan') }}
                                                    </button>
                                                </div>
                                            </div>
                                            </form>
                            </div>
                    </div>
        </div>
    </div>
@endsection
