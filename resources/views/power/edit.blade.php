@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit water</div>
                            <div class="card-body">
                            <form class="" action="{{ route('water.update', $water)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                                            <div class="form-group row">
                                                <label for="usage" class="col-md-4 col-form-label text-md-right">{{ __('Usage') }}</label>
                    
                                                <div class="col-md-6">
                                                    <input id="usage" type="text" class="form-control{{ $errors->has('usage') ? ' is-invalid' : '' }}" name="usage" value="{{$water->usage}}" required autofocus>
                    
                                                </div>
                                            </div>
                    
                                            <div class="form-group row">
                                                <label for="location" class="col-md-4 col-form-label text-md-right">{{ __('Location') }}</label>
                    
                                                <div class="col-md-6">
                                                    <input id="location" type="location" class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" name="location" value="{{$water->location}}" required>
                    
                                                </div>
                                            </div>
                    
                                            <div class="form-group row">
                                                <label for="sender" class="col-md-4 col-form-label text-md-right">{{ __('Sender') }}</label>
                    
                                                <div class="col-md-6">
                                                    <input id="sender" type="text" class="form-control{{ $errors->has('sender') ? ' is-invalid' : '' }}" name="sender" value="{{$water->sender}}" required>
                    
                                                </div>
                                            </div>
                    
                                            <div class="form-group row">
                                                <label for="response" class="col-md-4 col-form-label text-md-right">{{ __('Response') }}</label>
                    
                                                <div class="col-md-6">
                                                    <input id="response" type="text" class="form-control" name="response" value="{{$water->response}}" required>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-6 offset-md-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{ __('Simpan') }}
                                                    </button>
                                                </div>
                                            </div>
                                            </form>
                            </div>
                    </div> 
        </div>
    </div>   
@endsection