@extends('layouts.app') <!-- --> <!-- include-->

@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
                <div class="card">
                        <div class="card-header">Edit MK</div>
                            <div class="card-body">
                                    <form class="" action="{{ route('mk.update', $mk)}}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('PATCH') }} <!-- membuat patch PostController bisa dibaca -->
                                            <div class="form-group row">
                                                <label for="kode_mk" class="col-md-4 col-form-label text-md-right">{{ __('Kode') }}</label>
                                                <div class="col-md-6">
                                                    <input id="kode_mk" type="text" class="form-control" name="kode_mk" placeholder="Kode Matakuliah" value="{{$mk->kode_mk}}" required autofocus>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="nama_mk" class="col-md-4 col-form-label text-md-right">{{ __('Nama') }}</label>
                                                <div class="col-md-6">
                                                    <input id="nama_mk" type="text" class="form-control" name="nama_mk" placeholder="Nama Matakuliah" value="{{$mk->nama_mk}}" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="prodi" class="col-md-4 col-form-label text-md-right">{{ __('Prodi') }}</label>
                                                <div class="col-md-6">
                                                    <input id="prodi" type="text" class="form-control" name="prodi" placeholder="Program Studi" value="{{$mk->prodi}}" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="semester" class="col-md-4 col-form-label text-md-right">{{ __('Semester') }}</label>
                                                <div class="col-md-6">
                                                    <input id="semester" type="number" class="form-control" name="semester" placeholder="1 s.d. 8" min ="1" max ="8" value="{{$mk->semester}}" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="jumlah_jam" class="col-md-4 col-form-label text-md-right">{{ __('Jumlah Jam') }}</label>
                                                <div class="col-md-6">
                                                    <input id="jumlah_jam" type="number" class="form-control" name="jumlah_jam" placeholder="Jam" min ="1" max ="9" value="{{$mk->jumlah_jam}}" required>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col-md-6 offset-md-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{ __('Simpan') }}
                                                    </button>
                                                </div>
                                            </div>
                                            </form>
                            </div>
                    </div>
        </div>
    </div>
@endsection
